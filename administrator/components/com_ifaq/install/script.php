<?php
/**
 * @package    iFAQ
 * @subpackage Install
 * @author     Douglas Machado {@link http://idealextensions.com}
 * @author     Created on 21-Jan-2011
 * @license    GNU/GPL
 */

//-- No direct access
defined('_JEXEC') || die('=;)');

function com_install(){

	@ini_set('max_execution_time',0);
	$lang =& JFactory::getLanguage();
	$lang->load('com_ifaq',JPATH_SITE);
	$installClass = new com_ifaqInstallerScript();
	$installClass->install();
}

/**
 * Enter description here ...
 *
 */
class com_ifaqInstallerScript
{
    public function install($parent)
    {


    }//function

    public function uninstall($parent)
    {
        echo '<p>'.JText::_('com_ifaq_CUSTOM_UNINSTALL_SCRIPT').'</p>';
    }//function

    public function update($parent)
    {
        echo '<p>'.JText::_('com_ifaq_CUSTOM_UPDATE_SCRIPT').'</p>';
    }//function

    public function preflight($type, $parent)
    {
        echo '<p>'.JText::sprintf('com_ifaq_CUSTOM_PREFLIGHT', $type).'</p>';
    }//function

    public function postflight($type, $parent)
    {
    	$this->addUpdateSite();

     	$db = JFactory::getDbo();
        $db->setQuery(
				'UPDATE #__extensions ' .
				' SET enabled = 1' .
				" WHERE element='ifaq'"
			);
    	if (!$db->query()) {
			throw new Exception($db->getErrorMsg());
		}

		if($type == 'install'){
			$this->updateModules('mod_ifaq_search',	'ifaq-after-title',1);
		}

        /*
         * An example of setting a redirect to a new location after the install is completed
         * $parent->getParent()->set('redirect_url', 'http://www.example.com');
         */
    }//function


    function addUpdateSite(){
    	if(version_compare(JVERSION,'1.6.0','<')) return false;

    	$extension_name = 'com_ifaq';
    	$db = JFactory::getDbo();
    	$query="SELECT update_site_id FROM #__update_sites WHERE location LIKE '%{$extension_name}%' AND type LIKE 'extension'";
    	$db->setQuery($query);
    	$update_site_id = $db->loadResult();

    	$object = new stdClass();
    	$object->name='iFAQ';
    	$object->type='extension';
    	$domain = parse_url(JURI::root());
    	$domain = $domain['host'];
    	$domain = urlencode(base64_encode($domain));

    	$pid = 1;
    	$jversion = new JVersion();
    	$object->location='http://idealextensions.com/?extension='.$extension_name.'&pid='.$pid.'&output=updatexml&currentversion='.$this->new_version.'&amp;joomlaversion='. $jversion->getShortVersion().'&amp;dm='.$domain.'&file-extension.xml';

    	$object->enabled=1;
    	if(empty($update_site_id)){
    		$db->insertObject("#__update_sites",$object);
    		$update_site_id = $db->insertid();
    	}else{
    		$object->update_site_id = $update_site_id;
    		$db->updateObject("#__update_sites",$object,'update_site_id');
    	}
    	$query="SELECT extension_id FROM #__extensions WHERE `element` = '{$extension_name}' AND type LIKE 'component'";
    	$db->setQuery($query);
    	$extension_id = $db->loadResult();
    	if(empty($update_site_id) OR empty($extension_id))  return false;
    	$query='INSERT IGNORE INTO #__update_sites_extensions (update_site_id, extension_id) values ('.$update_site_id.','.$extension_id.')';
    	$db->setQuery($query);
    	$db->query();
    	return true;
    }

    public function updateModules($module, $position=null, $published=1){
    	$db =JFactory::getDBO();
    	$query = $db->getQuery(true);
    	$query->update($db->quoteName("#__modules"));
    	if($position){ $query->set($db->quoteName("position").' = '.$db->quote($position));}
    	$query->set($db->quoteName("published").' = '.$published);
    	$query->where($db->quoteName("module")." =" .$db->quote($module));
    	$db->setQuery($query);
    	$db->query();
    }
}//class

