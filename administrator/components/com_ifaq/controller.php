<?php
/**
 * @version SVN: $Id: header.php 18 2010-11-08 01:10:19Z elkuku $
 * @package    iFAQ
 * @subpackage Base
 * @author     Douglas Machado {@link http://idealextensions.com}
 * @author     Created on 21-Jan-2011
 * @license    GNU/GPL
 */

//-- No direct access
defined('_JEXEC') || die('=;)');

//-- Import Joomla controller library
jimport('joomla.application.component.controller');

/**
 * General Controller iFAQ component.
 *
 * @package iFAQ
 */
class iFAQController extends JControllerLegacy
{
    /**
     * Display task.
     *
     * @return void
     */
    public function display($cachable = false, $urlparams = false)
    {
        //-- Set default view if not set
        JRequest::setVar('view', JRequest::getCmd('view', 'cpanel'));

        //-- Call parent behavior
        parent::display($cachable);

        //-- Add submenu and icons
        require_once JPATH_COMPONENT.'/helpers/ifaq.php';

        iFAQHelper::addSubmenu('iFAQ');
    }//function
}//class
