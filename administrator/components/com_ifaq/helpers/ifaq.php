<?php
/**
 * @version SVN: $Id: header.php 18 2010-11-08 01:10:19Z elkuku $
 * @package    iFAQ
 * @subpackage Helpers
 * @author     Douglas Machado {@link http://idealextensions.com}
 * @author     Created on 21-Jan-2011
 * @license    GNU/GPL
 */

//-- No direct access
defined('_JEXEC') || die('=;)');

/**
 * iFAQ component helper.
 */
class iFAQHelper
{
    /**
     * Configure the Linkbar.
     */
    public static function addSubmenu($submenu)
    {
        $document = JFactory::getDocument();
        $document->addStyleDeclaration('.icon-48-com_ifaq '
        .'{background-image: url(../media/com_ifaq/images/com_ifaq-48x48.png);}');

    }//function
    /**
     * Check is a menu item exists
     * @param string $option
     */
    public function menuItems($option='com_ifaq') {
    	$db			= & JFactory::getDBO();
		$query = 	'SELECT * FROM #__menu WHERE link LIKE '.$db->Quote('%'.$option.'%');
		$db->setQuery($query);
		return $db->loadObjectList();
    }
}//class
