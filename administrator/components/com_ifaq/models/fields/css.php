<?php
/**
 * @package		iFAQ
 * @copyright	Copyright (C) 2006 - 2014 Ideal Custom software development. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('JPATH_PLATFORM') or die;
JFormHelper::loadFieldClass('textarea');

/**
 * Language
CSSCHANGER_COLOR="Color"
CSSCHANGER_BORDER="Border"
CSSCHANGER_OPACITY="Opacity"
CSSCHANGER_SIZE="Size"
CSSCHANGER_RADIUS="Radius"
CSSCHANGER_STYLE="Style"
CSSCHANGER_BACKGROUND="Background"
CSSCHANGER_FONT="Font"
CSSCHANGER_ALIGNMENT="Alignment"
CSSCHANGER_LEFT="Left"
CSSCHANGER_RIGHT="Right"
CSSCHANGER_CENTER="Center"
CSSCHANGER_ITALIC="Italic"
CSSCHANGER_BOLD="Bold"
CSSCHANGER_FONT_FAMILY="Font-family"
CSSCHANGER_UPPERCASE="UPPERCASE"
CSSCHANGER_LOWERCASE="lowercase"
CSSCHANGER_CAPITALIZE="Capitalize"
CSSCHANGER_INHERITED="Inherited"
 * */
/**
 * Supports an HTML select list of files
 *
 * @package	 Joomla.Platform
 * @subpackage  Form
 * @since	   11.1
*/
class JFormFieldCss extends JFormFieldTextarea
{
	/**
	 * The form field type.
	 *
	 * @var	string
	 * @since  11.1
	 */
	protected $type = 'CSS';

	/**
	 * Method to get the field input markup.
	 *
	 * @return	string	The field input markup.
	 * @since	1.6
	 */
	protected function getInput()
	{
		$doc	= JFactory::getDocument();
		JHtml::_('bootstrap.framework');
		JHtml::_('bootstrap.loadCss');
		$js		= '';
		$opt	= array();
		$html	= $this->getCSSChanger($opt);

		$footer = '';
		$params	= array('height' => '550px', 'width' => '95%', 'title' => JText::_('TITLE'));
		$selector	= $this->id.'-modal';

		$options = JHtml::getJSObject($opt);

		$js = "jQuery('#{$this->id}').cssCreator({$options});";

		$modalWindow = ($this->renderModal($selector, $params, $html, $js, $footer) );
		$button = '<a id="'.$selector.'-button" class="btn btn-default hasTooltip" href="#"><i class="icon-pencil glyphicon glyphicon-pencil"></i> '.JText::_('JTOOLBAR_EDIT').'</a>';
		$doc->addScriptDeclaration("
jQuery(document).ready(function($){
	$('#{$selector}-button').on('click',function(){jQuery('#{$selector}').modal('show');});
	$('#{$selector}').on('hidden',function(){ $('#{$this->id}').val($('".$opt['editElement']."').attr('style').replace(/\;/g,';".'\n'."')); });
});
");
		return $button.'<br />'.parent::getInput().$modalWindow;

	}

	public function getCSSChanger(&$opt){
		$doc	= JFactory::getDocument();
		$doc->addScript(JUri::root(true).'/components/com_ifaq/assets/js/jquery.csscreator.js');
		// Added Color picker
		$doc->addScript(JUri::base(true).'/components/com_ifaq/assets/jscolor/jscolor.js');

		// Add tab event
		$doc->addScriptDeclaration("\n jQuery(document).ready(function($){ $('#myTab a').on('click',function(e){e.preventDefault();$(this).tab('show');}) });\n");


		$menu	= array();
		$html	= array();
		$html[]	= '<div class="cssChanger">';
		$html[]	= '<div class="tab-content">';
		$opt['editElement'] 	= '#ifaq-collapsible';
		$opt['blockExample'] 	= '<div id="ifaq-collapsible"><a href="#">FAQ Title</a><span></span></div>';
		$code	= '<div id="cssChanger-code">'.$opt['blockExample'].'</div>';
		if(isset($this->element['border']) AND $this->element['border']){
			$html[] = $this->getBorder();
			$opt['loadBorder'] = true;
			$menu[]	= '<li class="active"><a href="#border">'.JText::_('CSSCHANGER_BORDER').'</a></li>'."\n";
		}else{
			$opt['loadBorder'] = false;
		}

		if(isset($this->element['background']) AND $this->element['background']){
			$html[] = $this->getBackground();
			$opt['loadBackground'] = true;
			$menu[]	= '<li><a href="#background"  data-toggle="tab">'.JText::_('CSSCHANGER_BACKGROUND').'</a></li>'."\n";
		}else{
			$opt['loadBackground'] = false;
		}
		if(isset($this->element['font']) AND $this->element['font']){
			$html[] = $this->getFont();
			$opt['loadFont'] = true;
			$opt['loadFontColor'] = ($this->element['font-color'] == true AND $this->element['font-color'] != 'inherited' ? true : false);
			$menu[]	= '<li><a href="#font"  data-toggle="tab">'.JText::_('CSSCHANGER_FONT').'</a></li>'."\n";
		}else{
			$opt['loadFont'] = false;
		}

		$menu = '<ul class="nav nav-tabs" id="myTab">'."\n".implode("\n\r", $menu).'</ul>'."\n\n";

		$html[]	= '</div>';
		$html[]	= '</div>';
		return $code.'<br />'.$menu.''.implode("\n\r", $html).'';
	}

	protected function getBorder() {
		$html = '
<div  class="tab-pane active" id="border">
	<label>'.JText::_('CSSCHANGER_COLOR').'</label>
	<div>
		<input id="bd-color" class="color inputbox span4" type="text" value="" />
	</div>
	<label for="bd-size">'.JText::_('CSSCHANGER_SIZE').':</label>
	<div class="input-append">
		<input class="size inputbox span12" id="bd-size" type="range" min="0" max="10" value="1" data-unit="px" >
		<span class="add-on">1px</span>
	</div>
	<label for="bd-opacity">'.JText::_('CSSCHANGER_OPACITY').':</label>
	<div class="input-append">
		<input class="opacity inputbox span12" id="bd-opacity" type="range" min="0" max="100" value="100"
			data-unit="%" />
		<span class="add-on">100%</span>
	</div>
	<label for="bd-opacity">'.JText::_('CSSCHANGER_RADIUS').':</label>
	<div class="input-append">
		<input class="radius inputbox span12" id="bd-radius" type="range" min="0" max="50" value="0"
			data-unit="px" />
		<span class="add-on">0px</span>
	</div>
	<label>'.JText::_('CSSCHANGER_STYLE').':</label>
	<div id="bd-style" class="btn-group" data-toggle="buttons-radio">
<button type="button" class="btn btn-primary active" data-border-style="solid" value="solid"><div style="width:20px;height:20px;border:1px solid #FFF"></div></button>
<button type="button" class="btn btn-primary" data-border-style="dashed"	 value="dashed"><div style="width:20px;height:20px;border:1px dashed #FFF"></div></button>
<button type="button" class="btn btn-primary" data-border-style="dotted"	 value="dotted"><div style="width:20px;height:20px;border:1px dotted #FFF"></div></button>
<button type="button" class="btn btn-primary" data-border-style="double"	 value="double"><div style="width:17px;height:16px;border:3px double #FFF"></div></button>
</div>
	<div class="clear"></div>
</div>';
		return $html;
	}

	protected function getBackground() {
		$html = '
<div  class="tab-pane" id="background">
	<label>'.JText::_('CSSCHANGER_COLOR').'</label>
	<div>
		<input id="bg-color" class="color inputbox span4" type="text" value="" />
	</div>
	<label for="bg-opacity">'.JText::_('CSSCHANGER_OPACITY').':</label>
	<div class="input-append">
		<input class="opacity inputbox span12" id="bg-opacity" type="range" min="0" max="100" value="100"
			data-unit="%" />
		<span class="add-on">100%</span>
	</div>

	<div class="clear"></div>
</div>';
		return $html;
	}
	protected function getFont() {
		$html = '
<div  class="tab-pane" id="font">';
	if(isset($this->element['font-color']) AND $this->element['font-color']){
		$html .= '<label>'.JText::_('CSSCHANGER_COLOR').'</label>';
		if($this->element['font-color'] == 'inherited'){
			$html .= '<div>'.JText::_('CSSCHANGER_INHERITED').'</div>';
		}else {
			$html .= '<div>
						<input id="font-color" class="color inputbox span4" type="text" value="" />
					</div>';
			$html .= '<label for="bg-opacity">'.JText::_('CSSCHANGER_OPACITY').':</label>
			<div class="input-append">
			<input class="opacity inputbox span12" id="bg-opacity" type="range" min="0" max="100" value="100"
					data-unit="%" />
					<span class="add-on">100%</span>
					</div>';
		}
	}

	$html .= '
	<label>'.JText::_('CSSCHANGER_ALIGNMENT').':</label>
			<div  id="font-align" class="btn-group" data-toggle="buttons-radio">
			<button type="button" value="left"		class="btn btn-default active"	title="'.JText::_('CSSCHANGER_LEFT').'"><i class="icon-align-left 	glyphicon glyphicon-align-left"></i></button>
			<button type="button" value="center"	class="btn btn-default"			title="'.JText::_('CSSCHANGER_CENTER').'"><i class="icon-align-center 		glyphicon glyphicon-align-center"></i></button>
			<button type="button" value="right"		class="btn btn-default"			title="'.JText::_('CSSCHANGER_RIGHT').'"><i class="icon-align-right 			glyphicon glyphicon-align-right"></i></button>
            </div>
	<label>'.JText::_('CSSCHANGER_STYLE').':</label>
			<div class="btn-group" data-toggle="buttons-checkbox">
				<button type="button" id="font-weight" value="bold"		class="btn btn-default" title="'.JText::_('CSSCHANGER_BOLD').'"><i class="icon-bold 	glyphicon glyphicon-align-bold"></i></button>
				<button type="button" id="font-style" value="italic"	class="btn btn-default" title="'.JText::_('CSSCHANGER_ITALIC').'"><i class="icon-italic 	glyphicon glyphicon-align-italic"></i></button>
            </div>
			<div  id="text-transform" class="btn-group" data-toggle="buttons-radio">
				<button type="button" value="inherit"		class="btn btn-default" title="'.JText::_('JDEFAULT').'"				>'.JText::_('CSSCHANGER_INHERITED').'</button>
				<button type="button" value="uppercase"		class="btn btn-default" title="'.JText::_('CSSCHANGER_UPPERCASE').'"	>'.JText::_('CSSCHANGER_UPPERCASE').'</button>
				<button type="button" value="lowercase"		class="btn btn-default" title="'.JText::_('CSSCHANGER_LOWERCASE').'"	>'.JText::_('CSSCHANGER_LOWERCASE').'</button>
				<button type="button" value="capitalize"	class="btn btn-default" title="'.JText::_('CSSCHANGER_CAPITALIZE').'"	>'.JText::_('CSSCHANGER_CAPITALIZE').'</button>

            </div>
			<div class="btn-group">
				<select class="" id="font-family">
			<option value="">'.JText::_('CSSCHANGER_FONT_FAMILY').'</option>
			<option value="inherited">'.JText::_('CSSCHANGER_INHERITED').'</option>
			<option value="arial,helvetica,sans-serif" style="font-family:arial,helvetica,sans-serif;">Arial</option>
<option value="comic sans ms,cursive" style="font-family:comic sans ms,cursive;">Comic Sans MS</option>
<option value="courier new,courier,monospace" style="font-family:courier new,courier,monospace;">Courier New</option>
<option value="georgia,serif" style="font-family:georgia,serif;">Georgia</option>
<option value="lucida sans unicode,lucida grande,sans-serif" style="font-family:lucida sans unicode,lucida grande,sans-serif;">Lucida Sans Unicode</option>
<option value="tahoma,geneva,sans-serif" style="font-family:tahoma,geneva,sans-serif;">Tahoma</option>
<option value="times new roman,times,serif" style="font-family:times new roman,times,serif;">Times New Roman</option>
<option value="trebuchet ms,helvetica,sans-serif" style="font-family:trebuchet ms,helvetica,sans-serif;">Trebuchet MS</option>
<option value="verdana,geneva,sans-serif" style="font-family:verdana,geneva,sans-serif;">Verdana</option>
				</select>
            </div>

	<div class="clear"></div>
</div>';
		return $html;
	}

	public function renderModal($selector = 'ifaq-modal', $params = array(), $body = '', $js = '', $footer = '')
	{
		// Ensure the behavior is loaded
		JHtml::_('bootstrap.modal', $selector, $params);

		$html = "<div class=\"modal hide fade\" id=\"" . $selector . "\">\n";
		$html .= "<div class=\"modal-header\">\n";
		$html .= "<button type=\"button\" class=\"close\" data-dismiss=\"modal\">×</button>\n";
		$html .= "<h3 id=\"".$selector."-title\">" . $params['title'] . "</h3>\n";
		$html .= "</div>\n";
		$html .= "<div id=\"" . $selector . "-container\">\n
				<div class=\"modal-body\" style=\"height:".$params['height'] .";width:".$params['width']."\">
				".$body."</div> \n";
		$html .= "</div>\n";
		$html .= "</div>\n";

		$html .= "<script>";
		$html .= "jQuery('#" . $selector . "').on('show', function () {\n";
		$html .= $js;

		$html .= "});\n";
		$html .= "</script>";
		$html .= $footer . "\n";
		return $html;
	}
}
