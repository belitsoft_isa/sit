<?php
/**
 * @copyright	Copyright (C) 2006 - 2014 Ideal Custom software development. All rights reserved.
 * @author     Douglas Machado {@link http://idealextensions.com}
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('JPATH_BASE') or die;

jimport('joomla.form.formfield');
jimport('joomla.html.html.category');

/**
 * Supports a modal contact picker.
 *
 * @package		iFAQ
* @since		1.6
 */
class JFormFieldMultiplecategories extends JFormField
{
	/**
	 * The form field type.
	 *
	 * @var		string
	 * @since	1.6
	 */
	protected $type = 'Multiplecategories';

	/**
	 * Method to get the field input markup.
	 *
	 * @return	string	The field input markup.
	 * @since	1.6
	 */
	protected function getInput()
	{
		$lang	= JFactory::getLanguage();
		$lang->load('com_ifaq',JPATH_SITE);
		$lang->load('com_ifaq',JPATH_ADMINISTRATOR);

		// Load the javascript and css
		JHtml::_('behavior.framework');

		$ctrl	= $this->name;
		$attribs	= ' ';

		if ($v = $this->element['size']) {
			$attribs	.= 'size="'.$v.'"';
		}
		if ($v = $this->element['class']) {
			$attribs	.= 'class="'.$v.'"';
		} else {
			$attribs	.= 'class="inputbox"';
		}
		if ($m = $this->element['multiple'])
		{
			$attribs	.= 'multiple="multiple"';
			//$ctrl		.= '[]';
		}

		$options	= JHtml::_('category.options',$this->element['extension']);

		if (!$this->element['hide_none']) {
			array_unshift($options, JHtml::_('select.option', '', JText::_('JOPTION_DO_NOT_USE')));
		}

		return JHtml::_(
			'select.genericlist',
			$options,
			$ctrl,
			array(
				//'id' => $control_name.$name,
				'list.attr' => $attribs,
				'list.select' => $this->value
			)
		);
	}
}
