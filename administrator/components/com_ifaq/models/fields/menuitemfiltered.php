<?php
/**
 * @package		iFAQ
 * @copyright	Copyright (C) 2006 - 2014 Ideal Custom software development. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('JPATH_BASE') or die;

jimport('joomla.html.html');
jimport('joomla.form.formfield');

// Load language file, needed for the module
$lang	= JFactory::getLanguage();
$lang->load('com_ifaq',JPATH_ADMINISTRATOR);

/**
 * Form Field class for the Joomla Framework.
 *
 * @package		iFAQ
* @since		1.6
 */
class JFormFieldMenuItemfiltered extends JFormField
{
	/**
	 * The form field type.
	 *
	 * @var		string
	 * @since	1.6
	 */
	protected $type = 'MenuItemfiltered';

	public function getInput()
	{
		$db	= JFactory::getDbo();
		$menuType	= '';
		$where	= array();
		if(isset($this->element['component'])){
			$where[] = "link LIKE ".$db->Quote('%'.$this->element['component'].'%');
		}

		// load the list of menu types
		// TODO: move query to model
		$query = 'SELECT menutype, title' .
				' FROM #__menu_types' .
				' ORDER BY title';
		$db->setQuery($query);
		$menuTypes = $db->loadObjectList();

		if (isset($this->element['state'])) {
			$where[] = ' published = '.(int) $this->element['state'];
		}

		// load the list of menu items
		// TODO: move query to model
		$query = 'SELECT id as value, title as text' .
				' FROM #__menu' .
				' WHERE ' . (count($where) ? implode( ' AND ', $where ) : '1') .
				' ORDER BY menutype, parent_id'
				;

		$db->setQuery($query);
		$options = $db->loadObjectList();

		return JHtml::_('select.genericlist', $options, $this->name,
			array(
				'list.attr' => 'class="inputbox"',
				'list.select' => $this->value
			)
		);
	}
}
