<?php
/**
 * @package    iFAQ
 * @subpackage Views
 * @author     Douglas Machado {@link http://idealextensions.com}
 * @author     Created on 21-Jan-2011
 * @license    GNU/GPL
 */

//-- No direct access
defined('_JEXEC') || die('=;)');

//-- Import Joomla view library
jimport('joomla.application.component.view');
require_once (JPATH_COMPONENT.'/helpers/ifaq.php');
/**
 * iFAQ List View.
 *
 * @package iFAQ
 */
class iFAQViewConfig extends JViewLegacy
{
   
    /**
     * iFAQ view display method.
     *
     * @param string $tpl The name of the template file to parse;
     *
     * @return void
     */
    public function display($tpl = null)
    {
		JHtml::_('behavior.framework');
		
		$lang	= JFactory::getLanguage();
		$lang->load('com_ifaq',JPATH_ADMINISTRATOR);
		
   		$component = JComponentHelper::getComponent('com_ifaq');
   		//echo '<pre>'; print_r($component); exit;
		$app		= JFactory::getApplication();
		$menus		= $app->getMenu('site');
		$items	= $menus->getItems('component_id', $component->id);
		if(count($items)){
			$lists		= array();
			$views		= array();
			//$views[] 	= JHTML::_('select.option', 'search',		JText::_('COM_IFAQ_SEARCH') );
			$views[] 	= JHTML::_('select.option', 'category',		JText::_('JCATEGORY') );
			$views[] 	= JHTML::_('select.option', 'categories',	JText::_('JCATEGORIES') );
			$lists['view']			= JHTML::_('select.genericlist',   $views, 'view', 'class="inputbox" size="1" ', 'value', 'text', 'search');
					
			$types	= array();
			$types[] 	= JHTML::_('select.option', 'any',		JText::_('COM_IFAQ_SEARCH_TYPE_OPT_ANY') );
			$types[] 	= JHTML::_('select.option', 'keywords',	JText::_('COM_IFAQ_SEARCH_TYPE_OPT_KEYWORDS') );
			$lists['search_type']	= JHTML::_('select.genericlist',   $types, 'search_type', 'class="inputbox" size="1" ', 'value', 'text', 'any');
			
			$types	= array();
			$types[] 	= JHTML::_('select.option', 'any',		JText::_('COM_IFAQ_SEARCH_PHRASE_OPT_ANY') );
			$types[] 	= JHTML::_('select.option', 'all',		JText::_('COM_IFAQ_SEARCH_PHRASE_OPT_ALL') );
			$lists['searchphrase']	= JHTML::_('select.genericlist',   $types, 'searchphrase', 'class="inputbox" size="1" ', 'value', 'text', 'all');
			
			
			$arr = array(
				JHTML::_('select.option',  '0', JText::_( 'JNO' ) ),
				JHTML::_('select.option',  '1', JText::_( 'JYES' ) )
			);
			$lists['showtitle']		= JHTML::_('select.genericlist',	$arr,	'showtitle', 'class="inputbox" size="1" ', 'value', 'text', 0);	
			
			$arr = array(
				JHTML::_('select.option',  '0', JText::_( 'JNO' ) ),
				JHTML::_('select.option',  '1', JText::_( 'JYES' ) )
			);
			$lists['showdesc']		= JHTML::_('select.genericlist',	$arr,	'showdesc', 'class="inputbox" size="1" ', 'value', 'text', 0);	
			
			$arr = array(
					JHTML::_('select.option',  '', JText::_( 'JOPTION_USE_DEFAULT' ) ),
					JHTML::_('select.option',  'custom', JText::_( 'COM_IFAQ_CONFIG_SLIDER_CUSTOM' ) ),
					JHTML::_('select.option',  'joomla', JText::_( 'COM_IFAQ_CONFIG_SLIDER_JOOMLA_CORE' ) )
			);
			$lists['sliders']		= JHTML::_('select.genericlist',	$arr,	'slider', 'class="inputbox" size="1" ', 'value', 'text', '');
			
			jimport('joomla.filesystem.folder');
			
			$options = array();
			$options[]	= JHTML::_('select.option',  '', JText::_( 'JOPTION_USE_DEFAULT' ));
			$folders = JFolder::folders(JPATH_ROOT.'/components/com_ifaq/templates/', '');
			foreach ($folders as $folder)
			{
				$options[] = JHtml::_('select.option', $folder, $folder);
			}
			
			$lists['templates']		= JHTML::_('select.genericlist',	$options,	'template', 'class="inputbox" size="1" ', 'value', 'text', '');
			
				
			$this->assignRef('lists',	$lists);
			
			parent::display($tpl);
		}else{
			echo JText::_('COM_IFAQ_ERROR_NO_MENUITEM_FOUND');
		}
    }//function

    
}//class
