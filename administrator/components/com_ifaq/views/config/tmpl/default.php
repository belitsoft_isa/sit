<?php
/**
 * @package    iFAQ
 * @subpackage Views
 * @author     Douglas Machado {@link http://idealextensions.com}
 * @author     Created on 21-Jan-2011
 * @license    GNU/GPL
 */

//-- No direct access
defined('_JEXEC') || die('=;)');
JHtml::_('bootstrap.framework');
JHtml::_('bootstrap.loadCss');

	$document = JFactory::getDocument();
		$document->setTitle(JText::_('Add FAQ'));

		JRequest::setVar('tmpl', 'component');


$js = "
function addiFAQ()
	{
		var tag = '';
		var faqView		= jQuery('#view').val();
		var search		= jQuery('#search').val();
		var showtitle	= jQuery('#showtitle' ).val();
		var title		= jQuery('#title').val();
		var showdesc	= jQuery('#showdesc' ).val();
		var description	= jQuery('#description').val();
		var category	= jQuery('#category').val();
		var categories	= jQuery('#categories').val();
		var search_type	= jQuery('#search_type').val();
		var searchphrase= jQuery('#searchphrase').val();
		var template	= jQuery('#template').val();
		var slider		= jQuery('#slider').val();
		if(faqView == 'category'){
			tag = ' view=|'+faqView+'| id=|'+category+'| ';
		}else if(faqView == 'categories'){
			tag = ' view=|'+faqView+'| id=|'+categories+'| ';
		}else{
			tag = ' view=|'+faqView+'| ';
		}
		if(search){
			tag += ' search=|'+search+'| ';
			if(search_type){ tag += ' search_type=|'+search_type+'| ';}
			if(searchphrase){ tag += ' searchphrase=|'+searchphrase+'| ';}
		}
		if(title && showtitle == 1){ tag += ' title=|'+title+'| ';}else{ tag += ' showtitle=|'+showtitle+'| ';}
		if(description && showdesc == 1){ tag += ' description=|'+description+'| ';}else{ tag += ' showdesc=|'+showdesc+'| ';}

		if(slider){ tag += ' slider=|'+slider+'| ';}
		if(template){ tag += ' template=|'+template+'| ';}

		tag = '{ifaq '+tag+' }';

		window.parent.jSelectiFAQ(tag);
		return false;
	}

jQuery(document).ready(function($){
	var catSlide	= $('#category-container');
	var secSlide	= $('#categories-container');

	secSlide.slideUp();

	$('#view').on('change', function(e){
		if($('#view').val() == 'category'){
			catSlide.slideDown();
			secSlide.slideUp();
		}else if($('#view').val() == 'categories'){
			catSlide.slideUp();
			secSlide.slideDown();
		}
	});

	var titleSlide	= $('#title-container');
	titleSlide.slideUp();
	$('#showtitle').on('change', function(e){
		if($('#showtitle').val() == '0'){
			titleSlide.slideUp();
		}else if($('#showtitle').val() == '1'){
			titleSlide.slideDown();
		}
	});

	var descSlide	= $('#description-container');
	descSlide.slideUp();
	$('#showdesc').on('change', function(e){
		if($('#showdesc').val() == '0'){
			descSlide.slideUp();
		}else if($('#showdesc').val() == '1'){
			descSlide.slideDown();
		}
	});

});";
$document->addScriptDeclaration($js);
$document->addStyleDeclaration('
fieldset div.notes{background-color:#FFFFE1;border:1px solid #666666;color:#666666;font-size:88%;height:auto;margin:0 0 10px 10px;padding:5px;width:158px;}
fieldset div.notes h4{background:url("components/com_ifaq/assets/images/info-16.png") left top no-repeat;border-color:#666666;border-style:solid;border-width:0 0 1px;color:#666666;font-size:110%;padding:3px 0 3px 27px;}
fieldset div.notes p.last {margin:0;}
fieldset div.notes p{color:#666666;margin:0 0 1.2em;}
fieldset div.notes ul{padding:0 0 0 15px;}
div textarea{width:50%;height:80px}
');
?>


		<?php

?>
	<fieldset  class="form-horizontal ">
	<legend><?php echo JText::_('COM_IFAQ_ADD_FAQ_LABEL');?></legend>

	<div class="row-fluid">
       <div class="span8">
	       <div class="control-group">
		    <label class="control-label"for="view"><?php echo JText::_( 'COM_IFAQ_ADD_FAQ_VIEW' ); ?></label>
		    <div class="controls">
		      <?php echo $this->lists['view']; ?>
		    </div>
		  </div>
		  <div class="control-group"  id="category-container">
				<label class="control-label" for="category""><?php echo JText::_( 'JCATEGORY' ); ?></label>
			<div class="controls">
					<select name="category"  id="category" class="inputbox" >
						<!-- option value=""><?php echo JText::_('JOPTION_SELECT_CATEGORY');?></option  -->
						<?php echo JHtml::_('select.options', JHtml::_('category.options', 'com_content'), 'value', 'text','');?>
					</select>
		    </div>
		  </div>

		  <div class="control-group" id="categories-container">
				<label class="control-label" for="categories""><?php echo JText::_( 'Categories' ); ?></label>
			<div class="controls">
					<select name="categories" id="categories" class="inputbox">
					<!-- option value=""><?php echo JText::_('JOPTION_SELECT_CATEGORY');?></option  -->
					<?php echo JHtml::_('select.options', JHtml::_('category.options', 'com_content'), 'value', 'text', '');?>
					</select>
		    </div>
		  </div>

		  <div class="control-group"  style="display:none">
				<label class="control-label"  for="search"><?php echo JText::_( 'COM_IFAQ_SEARCH_TERM_LABEL' ); ?></label>
			<div class="controls">
					<input type="text" id="search" name="search" />
		    </div>
		  </div>

		   <div class="control-group"  style="display:none">
				<label class="control-label" for="search_type"><?php echo JText::_( 'COM_IFAQ_SEARCH_TYPE_LABEL' ); ?></label>
			<div class="controls">
					<?php echo $this->lists['search_type']; ?>
		    </div>
		  </div>

		   <div class="control-group"  style="display:none">
				<label class="control-label" for="searchphrase"><?php echo JText::_( 'COM_IFAQ_SEARCH_PHRASE_LABEL' ); ?></label>
			<div class="controls">
					<?php echo $this->lists['searchphrase']; ?>
		    </div>
		  </div>


			<div class="control-group">
				<label class="control-label" for="showtitle"><?php echo JText::_( 'COM_IFAQ_SHOW_TITLE_LABEL' ); ?></label>
				<div class="controls">
						<?php echo $this->lists['showtitle']; ?>
			    </div>
			</div>


			<div class="control-group" id="title-container">
				<label  class="control-label" for="title"><?php echo JText::_( 'COM_IFAQ_OVERRIDE_TITLE_LABEL' ); ?></label>
				<div class="controls">
						<input type="text" id="title" name="title" />
			    </div>
			</div>


			<div class="control-group">
				<label  class="control-label" for="showdesc"><?php echo JText::_( 'COM_IFAQ_SHOW_DESC_LABEL' ); ?></label>
				<div class="controls">
						<?php echo $this->lists['showdesc']; ?>
			    </div>
			</div>


			<div class="control-group"   id="description-container">
				<label  class="control-label" for="description"><?php echo JText::_( 'COM_IFAQ_OVERRIDE_DESCRIPTION_LABEL' ); ?></label>
				<div class="controls">
						<textarea id="description" name="description"></textarea>
			    </div>
			</div>

			<div class="control-group" >
				<label  class="control-label" for="template"><?php echo JText::_( 'COM_IFAQ_CONFIG_TEMPLATE_LABEL' ); ?></label>
				<div class="controls">
						<?php echo $this->lists['templates']; ?>
			    </div>
			</div>



			<div class="submit">
				<div>
					<button class="btn btn-primary button" onclick="return 	addiFAQ();"><?php
						echo JText::_( 'COM_IFAQ_ADD_FAQ_BUTTON' ); ?></button>
				</div>
			</div>
		</div>

		<div class="notes span3">
	        <h4><?php echo JText::_('COM_IFAQ_ADD_FAQ_NOTES');?></h4>
	        <p class="last"><?php echo JText::_('COM_IFAQ_ADD_FAQ_DESC');?></p>
       </div>
      </div>
	</fieldset>
	<div class="clr"> </div>
