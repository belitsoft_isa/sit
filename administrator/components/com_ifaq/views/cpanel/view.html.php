<?php
/**
 * @version SVN: $Id: header.php 18 2010-11-08 01:10:19Z elkuku $
 * @package    iFAQ
 * @subpackage Views
 * @author     Douglas Machado {@link http://idealextensions.com}
 * @author     Created on 21-Jan-2011
 * @license    GNU/GPL
 */

//-- No direct access
defined('_JEXEC') || die('=;)');

//-- Import Joomla view library
jimport('joomla.application.component.view');
require_once (JPATH_COMPONENT.'/helpers/ifaq.php');
/**
 * iFAQ List View.
 *
 * @package iFAQ
 */
class iFAQViewCpanel extends JViewLegacy
{
    /**
     * items to be displayed
     */
    protected $items;

    /**
     * pagination for the items
     */
    protected $pagination;

    /**
     * iFAQ view display method.
     *
     * @param string $tpl The name of the template file to parse;
     *
     * @return void
     */
    public function display($tpl = null)
    {
       $this->setToolBar();
       
		$lang 		= JFactory::getLanguage();
		$lang->load('com_menus', JPATH_COMPONENT);
		$lang->load('com_ifaq.sys', JPATH_COMPONENT);
		$lang->load('com_ifaq', JPATH_COMPONENT);
		$lang->load('plg_content_ifaq', JPATH_ROOT.'/plugins/content/ifaq');
		$this->items		= $this->get('Items');
        //-- Display the template
        parent::display($tpl);

        //-- Set the document
        $this->setDocument();
    }//function

    /**
     * Setting the toolbar.
     *
     * @return void
     */
    protected function setToolBar()
    {
        JToolBarHelper::title(JText::_('iFAQ'), 'com_ifaq');
       
        JToolBarHelper::preferences('com_ifaq');
    }//function

    /**
     * Method to set up the document properties.
     *
     * @return void
     */
    protected function setDocument()
    {
        JFactory::getDocument()->setTitle(JText::_('iFAQ Administration').' - '.JFactory::getDocument()->getTitle());
    }//function
}//class
