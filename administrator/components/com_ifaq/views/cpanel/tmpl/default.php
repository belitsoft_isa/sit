<?php
/**
 * @package    iFAQ
 * @subpackage Views
 * @author     Douglas Machado {@link http://idealextensions.com}
 * @author     Created on 21-Jan-2011
 * @license    GNU/GPL
 */

//-- No direct access
defined('_JEXEC') || die('=;)');

// Include the component HTML helpers.
JHtml::addIncludePath(JPATH_COMPONENT.'/helpers/html');
JHtml::_('behavior.tooltip');

$user	= JFactory::getUser();
$userId	= $user->get('id');
?>
<div>
<form action="<?php echo JRoute::_('index.php'); ?>" 
	method="post" 
	name="adminForm" 
	id="contact-form" class="form-validate form-horizontal">
	<fieldset>
		<div>
			<h1><?php echo JText::_('COM_IFAQ'); ?></h1>
			<p><img src="components/com_ifaq/assets/images/icon-48.png" style="float:left;padding:5px;margin:5px" />
			<?php echo JText::_('COM_IFAQ_XML_DESCRIPTION'); ?>
			</p>
		</div>

		<div style="width:49.8%;float:left">
			<h2><?php echo JText::_('COM_IFAQ_CREATE_MENU_ITEM'); ?></h2>
			<p>
				<a href="index.php?option=com_menus&view=item&layout=edit" >
				<img src="components/com_ifaq/assets/images/icon-48-menumgr.png" style="float:left;padding:5px;margin:5px" /></a>
			<?php echo JText::_('COM_IFAQ_TUTORIAL_MENU_ITEM_DESCRIPTION'); ?>
			</p>
		</div>
		<div style="width:49.8%;float:left">
			<h2><?php echo JText::_('PLG_CONTENT_IFAQ'); ?></h2>
			<p>
				<img src="components/com_ifaq/assets/images/icon-48-embed.png" style="float:left;padding:5px;margin:5px" />
				<?php echo JText::_('PLG_CONTENT_IFAQ_XML_DESCRIPTION'); ?>
			</p>
		</div>
	</fieldset>

</form>
</div>