<?php
/**
 * @version SVN: $Id: header.php 18 2010-11-08 01:10:19Z elkuku $
 * @package    iFAQ
 * @subpackage Controllers
 * @author     Douglas Machado {@link http://idealextensions.com}
 * @author     Created on 21-Jan-2011
 * @license    GNU/GPL
 */

//-- No direct access
defined('_JEXEC') || die('=;)');

//-- Import Joomla controller library
jimport('joomla.application.component.controller');

/**
 * iFAQ List controller.
 *
 * @package iFAQ
 */
class iFAQControllerCpanel extends JController
{
	
}//class
