<?php
/**
 * @package    iFAQ
 * @subpackage Base
 * @author     Douglas Machado {@link http://idealextensions.com}
 * @author     Created on 21-Jan-2011
 * @license    GNU/GPL
 */

//-- No direct access
defined('_JEXEC') || die('=;)');

//-- Import joomla controller library
jimport('joomla.application.component.controller');
$jversion = new JVersion();
if( version_compare( $jversion->getShortVersion(), '2.5.5', 'lt' ) AND !class_exists('JControllerLegacy') ) {
	require_once (JPATH_COMPONENT.'/legacy/controller.php');
	require_once (JPATH_COMPONENT.'/legacy/view.php');
}

//-- Get an instance of the controller prefixed by iFAQ
$controller = JControllerLegacy::getInstance('iFAQ');

//-- Perform the Request task
$controller->execute(JRequest::getCmd('task'));

//-- Redirect if set by the controller
$controller->redirect();
