CREATE TABLE IF NOT EXISTS `#__aimysitemap` (
    `id`         int(11) unsigned NOT NULL AUTO_INCREMENT,
    `url`        varchar(255)     NOT NULL UNIQUE,
    `title`      varchar(255)     NOT NULL DEFAULT '',
    `priority`   float            NOT NULL DEFAULT 0.5,
    `mtime`      bigint           NOT NULL DEFAULT 0,
    `lang`       char(7)          NOT NULL DEFAULT '*',
    `state`      int(1)           NOT NULL DEFAULT 0,
    `lock`       bool             NOT NULL DEFAULT false,
    `changefreq` char(7)          NOT NULL DEFAULT 'monthly',
    PRIMARY KEY( `id` )
) DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;

CREATE TABLE IF NOT EXISTS `#__aimysitemap_crawl` (
    `url`        varchar(255)     NOT NULL UNIQUE
                                  COMMENT 'Crawled URL',
    `crawled`    bool             DEFAULT FALSE
                                  COMMENT 'Crawled already?',
    `index`      bool             DEFAULT FALSE
                                  COMMENT 'Index URL?',
    `title`      varchar(255)     NOT NULL DEFAULT '',
    `mtime`      bigint           NOT NULL DEFAULT 0,
    `lang`       char(7)          NOT NULL DEFAULT '*',
    PRIMARY KEY( `url` )
) DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `#__aimysitemap_kvstore` (
    `k`          varchar(255)     NOT NULL UNIQUE
                                  COMMENT 'key',
    `v`          text             NOT NULL DEFAULT ''
                                  COMMENT 'value',
    PRIMARY KEY( `k` )
) DEFAULT CHARSET=utf8;


-- vim: sts=4 sw=4 ts=4 ai et ft=mysql:
