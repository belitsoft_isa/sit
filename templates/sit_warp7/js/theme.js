/* Copyright (C) YOOtheme GmbH, http://www.gnu.org/licenses/gpl.html GNU/GPL */


window.onload = function(e){
    //main menu
    jQuery('.uk-navbar-nav').find('li.uk-open').removeClass('uk-navbar-nav');
}

jQuery(function($) {

    var config = $('html').data('config') || {};

    // Social buttons
    $('article[data-permalink]').socialButtons(config);

});

jQuery(document).ready(function() {

    //footerToBottom();

    jQuery('.sidebar-tabs .nav > li.parent').click(
        function(e) {
            console.log(jQuery(this));
            if(jQuery(this).find('ul.small').css('display')=='none'){
                jQuery(this).find('.small').slideDown(150);
                jQuery('.sidebar-tabs ul.nav ul.small li').click(
                    function(e) {
                        var a_href=jQuery(this).find('a').attr('href');
                        window.open(a_href,'_self');

                    });
                return false;
            }else{
                jQuery('.nav-child').slideUp("slow");
                jQuery('.sidebar-tabs ul.nav ul.small li').click(
                    function(e) {
                        var a_href=jQuery(this).find('a').attr('href');
                        window.location.href=a_href;

                    });
            }

        });

    jQuery(document).click( function(event){
        jQuery('.nav-child').slideUp("slow");
        event.stopPropagation();
    });

    jQuery('.accordion-title').on('click', function() {
        jQuery(this).toggleClass('accordion-open');
    });





//left nav menu
    jQuery('.a-sidebar-accordion').find('.uk-custom-open').addClass('uk-open');
    jQuery('.a-sidebar-accordion').find('.uk-active').parent('.uk-nav-sub').parent('div').removeClass('uk-hidden').removeAttr('style').css({"overflow": "hidden", "position": "relative"});



    jQuery('.red-menu-item').parent().css({"display":"inline-block","width":"45%","padding":"0","list-style":"none"});



    separateMenuLinks();

    function separateMenuLinks(){

        jQuery('.space-menu-item').each(function( index ) {
            var elemParent=jQuery( this ).parent('li').parent('ul').parent('div').not('.uk-hidden'),
                newElemDiv=jQuery(elemParent).next();
            if(newElemDiv.length){
                jQuery(newElemDiv).find('ul').prepend(jQuery( this ).parent('li'));

            }

        });

    }

});
jQuery( window ).resize(function(){
    //footerToBottom();
});

function footerToBottom() {
    var footerWrapHeight;
    footerWrapHeight =jQuery(".footer-wrap").outerHeight();
    jQuery('.push').css("height", footerWrapHeight+'px');
    jQuery(".top-wrap").css("margin-bottom", -footerWrapHeight);
}
