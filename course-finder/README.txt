# [SIT PROGRAM FINDER] (http://singaporetech.edu.sg/programme-finder)

Powerful program finder

* Website: [singaporetech.edu.sg/](http://singaporetech.edu.sg/)
* Author : [Voilàah](http://www.voilaah.com)

## Getting Started

go to the url : http://local/j_sit/beta/course-finder/demo/index.html
make sure that the file "diploma.csv" is updated in the "assets" folder with your latest data following the expected format (1). 
Then click on "click to fetch", wait, then copy and paste the JSON result.
To minimise and compress your json result, go there : http://jscompress.com/

(1) expected format : 
Poly Name;Diploma Name;University Name;Programme Name;URL
