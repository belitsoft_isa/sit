
/* elational Data Model  */

/*

FetchInstitution = 5 items
An institution has many diplomas
A diploma is available trough many degree name awarded by universities

*/

/*

  Institutions (a collection of Institution models)
  --Diplomas (a collection of Diploma models)
    --Degrees (a collection of Degree models)
      --University

*/


jQuery(function(){


/********************************************
* Models and Collections
********************************************/

/* Institution Models */
/*--------------------*/
  CourseFinderApp.Model.Institution = Backbone.Model.extend({
    initialize: function( model, options ) {
        if (CourseFinderApp.Model.debug) {
          console.log('|__ new institution ' + this.get('name') + '__|');
        }
        var diplomas = new CourseFinderApp.Collection.DiplomaList();
        this.set( {diplomas: diplomas } );
    },
    getDiplomaByName: function(diplomaName) {
      return this.get('diplomas').find(function(_diploma) {
          return diplomaName == _diploma.get('name');
      });
    },
    containsDiploma: function( diploma ) {
      var self = this;
      var isDupe = self.get('diplomas').any(function(_diploma) {
          return diploma.get('label') == _diploma.get('name');
      });
      return isDupe;
    }
  });

  CourseFinderApp.Collection.InstitutionList = Backbone.Collection.extend({
      model: CourseFinderApp.Model.Institution,
      comparator: function(item) {
        return item.get('name');
      },
      initialize: function(model, options) {
        this.on("add", this.onAddInstitution);
      },

      onAddInstitution : function(institution) {
          if (CourseFinderApp.Model.debug) {
            console.log("|__ adding " + institution.get("name"));
          }
        },

      contains: function( institution ) {
          var isDupe = this.any(function(_institution) {
              return institution.get('name') === _institution.get('name');
          });
          return isDupe;
      }
  });


/* Diploma Models */
/*----------------*/
  CourseFinderApp.Model.Diploma = Backbone.Model.extend({
    defaults: {
      name: 'diploma name',
      courses: {}
    },
    initialize: function(  ) {
      // console.log('|____ new ' + this.get('name'));
      this.set( {id:generateGUID()});
      this.set( {courses: new CourseFinderApp.Collection.CourseList() });
    }
  });

  CourseFinderApp.Collection.DiplomaList = Backbone.Collection.extend({
      model: CourseFinderApp.Model.Diploma,
      comparator: function(item) {
        return item.get('name');
      },
      initialize: function(model, options) {
        this.on("add", this.onAddDiploma, this);
        this.on("remove", this.onRemoveDiploma, this);
      },
      onAddDiploma: function(diploma) {
        if (CourseFinderApp.Model.debug) {
          console.log("|______ adding diploma " + diploma.get("name") );
        }
      },
      onRemoveDiploma: function(diploma) {
        // if (CourseFinderApp.Model.debug) {
          console.log("|______ removing diploma " + diploma.get("name") );
        // }
      }
    });



/* Courses Models */
/*----------------*/
  CourseFinderApp.Model.Course = Backbone.Model.extend({
    defaults: {
      university: 'university',
      degree : 'degree',
      url: "#"
    }
  });
  CourseFinderApp.Collection.CourseList  = Backbone.Collection.extend({
    model: CourseFinderApp.Model.Course,

    initialize: function(model, options) {
        this.on("add", this.onAddCourse, this);
    },
    onAddCourse: function(course) {
      if (CourseFinderApp.Model.debug) {
        console.log("|______ adding course " + course.get('degree')  + ' @ ' + course.get("university") );
      }
    },
    contains: function( course ) {
      var isDupe = this.any(function(_course) {
          return course.get('label') === _course.get('degree');
      });
      return isDupe;
    }
  });



/********************************************
* Views
********************************************/


  /* SelectSingleView represents a single diploma model trough a option tag */
  CourseFinderApp.View.SelectOptionView = Backbone.View.extend({
    tagName: "option",

    initialize: function(options){
      _.bindAll(this, 'render');
    },
    render: function(){
        // we can also use a template instead of direct html injecting
        jQuery(this.el).attr('value', this.model.get('id')).html(this.model.get('name'));
        return this;
    }

  });
  /* SelectView is an abstract class (to be extend then) represents a select list containing a list of model */
  CourseFinderApp.View.SelectView = Backbone.View.extend({
    //el: jQuery('#diplomas-editor-container'),
    //template: _.template(jQuery("#diplomas_select_template").html()),

    events: {
        "change": "changeSelected"
    },
    toString: function() {
      return "CourseFinderApp.View.SelectView";
    },
    initialize: function(){
        _.bindAll(this, 'addOne', 'addAll');
        this.collection.bind('reset', this.addAll, this);
        // this.on('listChanged', this.addAll);
    },
    addOne: function(single){
        var selectOptionView = new CourseFinderApp.View.SelectOptionView({ model: single });
        this.selectOptionViews.push(selectOptionView);
        jQuery(this.el).append(selectOptionView.render().el);
    },
    addAll: function(ev){
      if (CourseFinderApp.View.debug) {
        console.log( this.toString() + " addAll() " + ev);
      }
      _.each(this.selectOptionViews, function(selectOptionView) { selectOptionView.remove(); });
      this.selectOptionViews = [];
      this.collection.each(this.addOne);
      if (this.selectedId) {
          jQuery(this.el).val(this.selectedId);
      }
    },
    changeSelected: function(e){
        var value = jQuery(this.el).val();
        if (CourseFinderApp.View.debug) {
          console.log('value selected:' + value);
        }
        // var id = jQuery(e.currentTarget).data("id");
        var id = jQuery(this.el).data("id");
        if (!id) {
          var field = jQuery(e.currentTarget);
          id = jQuery("option:selected", field).val();
        }
        var item = this.collection.get(id);
        // this.trigger('selectionChanged:' + value, e);
        this.setSelectedId( item );
    },
    populateFrom: function(_collection) {
        // this.collection.url = url;
        // this.collection.fetch();
        this.collection = _collection;
        // this.trigger('listChanged');
        this.addAll();
        this.setDisabled(false);
    },
    setDisabled: function(disabled) {
        jQuery(this.el).attr('disabled', disabled);
    }
  });

  /* Our 2 specialisations of the SelectView abstract class */
  CourseFinderApp.View.InstitutionsView = CourseFinderApp.View.SelectView.extend({
    toString: function() {
      return "CourseFinderApp.View.InstitutionsView";
    },
    initialize: function() {
      // Backbone.View.prototype.initialize.apply(this, arguments); // call super();
      this.constructor.__super__.initialize.apply(this, arguments); // call super();
      this.render(); // because we want to render this view at the begining
    },
    setSelectedId: function(institution) {
        this.diplomasView.selectedId = null;
        // this.diplomasView.collection.reset();
        this.diplomasView.setInstitution(institution);

        // this.coursesView.collection.reset();
        // this.coursesView.setDisabled(true);
    },
    render: function() {
      this.addAll();
    }
  });

  CourseFinderApp.View.DiplomasView = CourseFinderApp.View.SelectView.extend({
    toString: function() {
      return "CourseFinderApp.View.DiplomasView";
    },
    setSelectedId: function(diplomaId) {
        this.coursesView.selectedId = null;
        this.coursesView.setDiplomaId(diplomaId);
    },
    setInstitution: function(institution) {
      // this.populateFrom("countries/" + institutionId + "/cities");
      // console.log( institution.diplomas );
      //this.collection.reset();
      this.populateFrom( institution.get('diplomas') );
    }
  });


/* Courses View */

  CourseFinderApp.View.CourseView = CourseFinderApp.View.SelectOptionView.extend({
    tagName: 'li',
    // template: _.template(jQuery("#course-item-template").html()),


    render: function() {
        // this.template= _.template(jQuery("#course-item-template").html());
        this.template= _.template( tpl.get("course-item-template") );
        var markup = this.template(this.model.toJSON());
        this.$el.html(markup);
        return this;
    }

  });

  CourseFinderApp.View.CoursesView = CourseFinderApp.View.SelectView.extend({
    toString: function() {
      return "CourseFinderApp.View.CoursesView";
    },
    setSelectedId: function(id) {
      // Do nothing
    },
    setDiplomaId: function(diploma) {
        //this.populateFrom("cities/" + cityId + "/suburbs");
        this.populateFrom( diploma.get('courses') );
    },
    addOne: function(single){ // extend the addOne method to change the tagName
        var selectOptionView = new CourseFinderApp.View.CourseView({ model: single });
        this.selectOptionViews.push(selectOptionView);
        this.$el.append( selectOptionView.render().el );
        // jQuery(this.el).append(selectOptionView.render().el);
        // Backbone.View.prototype.initialize.apply(this, arguments); // call super();
    }
  });

});