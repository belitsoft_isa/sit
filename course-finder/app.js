// Disable the $ global alias completely
jQuery.noConflict();

window.CourseFinderApp = {},
  CourseFinderApp.Model = {},
  CourseFinderApp.Collection = {},
  CourseFinderApp.View = {},
  CourseFinderApp.Model.debug = false,
  CourseFinderApp.View.debug = false,
  CourseFinderApp.debug = false,
  appview = {},
  CourseFinderFetcherApp = {},
  path = location.protocol+'//'+window.location.hostname+'/course-finder'//(window.location.host.indexOf('local')>-1)?'./':'./course-finder'
  ;

var generateGUID = function(){
    var guid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
        var r = Math.random()*16|0, v = c == 'x' ? r : (r&0x3|0x8);
         return v.toString(16);
    });
    return guid;
};

// make it safe to use console.log always
(function(a){function b(){}for(var c="assert,count,debug,dir,dirxml,error,exception,group,groupCollapsed,groupEnd,info,log,markTimeline,profile,profileEnd,time,timeEnd,trace,warn".split(","),d;!!(d=c.pop());){a[d]=a[d]||b;}})
(function(){try{console.log();return window.console;}catch(a){return (window.console={});}}());

//
/* synchronous html template loader */
var tpl = {
    // Hash of preloaded templates for the app
    templates: {},
    // Recursively pre-load all the templates for the app.
    // This implementation should be changed in a production environment:
    // All the template files should be concatenated in a single file.
    loadTemplates: function(names, callback) {
        var that = this;
        var loadTemplate = function(index) {
            var name = names[index];

            jQuery.get(path + '/tpl/' + name + '.html', function(data) {
                that.templates[name] = data;
                index++;
                if (index < names.length) {
                    loadTemplate(index);
                } else {
                    callback();
                }
            });
        };
        loadTemplate(0);
    },
    // Get template by name from hash of preloaded templates
    get: function(name) {
        return this.templates[name];
    }
};

jQuery(document).ready(function(){

  // fetch the data model
  // university has many degrees which has many institutions which has many diploma
  // Note: a diploma can belong to many institutions
  CourseFinderFetcherApp = new Backbone.Marionette.Application({});
  CourseFinderFetcherApp.debug = false;

  CourseFinderFetcherApp.addInitializer(function(options){
    universityList = new FetchUniversityCollection();
    universityList.fetch({
      success: function () {
        // load the html template before starting the main application view
        tpl.loadTemplates(['institutions_select_template', 'diplomas_select_template', 'course-item-template'], function() {
          appview = new CourseFinderApp.View.AppView( {universityList:universityList} );
        });

      }
    });

  });
  CourseFinderFetcherApp.start();

  // Fetching data is finsihed there
  // now we have to transform this data modle
  // into a new data model easier to handle for our frontend application

  CourseFinderApp.View.AppView = Backbone.View.extend( {
    el: jQuery('body'),
    events: {
      "click #dump": 'dumpModel'
    },
    initialize: function(options) {
      var self = this;
      var diplomasCount = 0,
          coursesCount = 0;
      self.institutionsList = new CourseFinderApp.Collection.InstitutionList();

      this.universityList = options.universityList;
      // build the new model
      this.universityList.forEach( function(university){
        if (CourseFinderApp.Model.debug) {
          console.log('|.. Mapping University ' + university.get('label'));
        }
        var degrees = university.get('degrees');
        degrees.forEach(
          function(degree) {
            if (CourseFinderApp.Model.debug) {
              console.log('|.... Mapping degree ' + degree.get('label'));
            }
            var institutions = degree.get('institutions');
            institutions.forEach(
              function( _institution ) {
                if (CourseFinderApp.Model.debug) {
                    console.log('|...... Mapping _institution ' + _institution.get('label'));
                }
                // get the diplomas from our new model or create new one
                // to ensure the uniqueness of our institution (should be 5 in total)
                var theInstitution = self.getInstitution( _institution );
                // handle the fetched diplomas collection
                var fetchedDiplomas = _institution.get('diplomas');
                self.addDiplomas( { degree: degree, university: university, institution: theInstitution, fetchedDiplomas: fetchedDiplomas });
                // for eah diploma inserted
                // try to add the related course (degree and university) if not already exist
                // theInstitution.get('diplomas').each(function(diploma) {
                //   diploma.addCourse( degree, university);
                // });
              });
          });
      });

      this.dumpModel();

      //render institutionList View
      // this.institutionListView = new CourseFinderApp.View.InstitutionListView( { collection: this.institutionsList} );
      // this.institutionListView.render();
      // this.diplomaListView = new CourseFinderApp.View.DiplomaListView();

      var institutionsView = new CourseFinderApp.View.InstitutionsView({el: jQuery("#institutions-selector"), collection: self.institutionsList});
      var diplomasView = new CourseFinderApp.View.DiplomasView({el: jQuery("#diplomas-selector"), collection: new CourseFinderApp.Collection.DiplomaList()});
      var coursesView = new CourseFinderApp.View.CoursesView({el: jQuery("#courses-selector"), collection: new CourseFinderApp.Collection.CourseList()});

      institutionsView.diplomasView = diplomasView;
      institutionsView.coursesView = coursesView;
      diplomasView.coursesView = coursesView;

      institutionsView.setDisabled( false );

    },
    dumpModel: function() {
      var self = this;
      var diplomasCount = 0,
          coursesCount = 0;
        self.institutionsList.each(function(institution) {
          diplomasCount = diplomasCount + institution.get('diplomas').size();
          institution.get('diplomas').each(function(diploma) {
            coursesCount = coursesCount + diploma.get('courses').size();
          });
        });


        console.log( "|____ Institutions : " + self.institutionsList.size() );
        self.institutionsList.each( function(institution) {
          console.log('|______ ' + institution.get('name'));
        })

    },
    getInstitution: function( _institution ) {
      var institution = this.institutionsList.findWhere( { id: _institution.get('id')} );
      if (!institution) {
      // if  (!self.institutionsList.contains( {name: institution.get('label') } )) {
          institution = new CourseFinderApp.Model.Institution(
            { id: _institution.get('id'), name: _institution.get('label') });
          this.institutionsList.add( institution );
      } else {
        if (CourseFinderApp.Model.debug) {
          console.log('|__ getting institution ' + institution.get('name'));
        }
      }

      return institution;
    },
    addDiplomas: function( data ) {
      var self = this;
      var institution = data.institution;
      var fetchedDiplomas = data.fetchedDiplomas;
      var degree = data.degree;
      var university = data.university;
      var diploma;
      fetchedDiplomas.forEach( function(fetchedDiploma){
        // var myModel = self.diplomas.findWhere({ name: diploma.get('label') });
        // if (!myModel) {
        fetchedDiploma.set( 'label', fetchedDiploma.get('label').trim() ); // trim the diploma label
        if (!institution.containsDiploma( fetchedDiploma )) {
          diploma = new CourseFinderApp.Model.Diploma( {name: fetchedDiploma.get('label')} );
          institution.get('diplomas').add( diploma );
        } else {
          diploma = institution.getDiplomaByName( fetchedDiploma.get('label') );
          if (CourseFinderApp.Model.debug) {
            console.log('|______ getting diploma ' + diploma.get('name'));
          }
        }
        self.addCourse( {degree:degree, university:university, diploma: diploma } );
      });
    },
    addCourse: function( data ) {
      var diploma = data.diploma;
      var degree = data.degree;
      var university = data.university;
      if (CourseFinderApp.Model.debug) {
        console.log('|______ adding course to diploma ' + diploma.get('name'));
      }
      var course;
      if ( (!diploma.get('courses').contains( degree )) ) { //&& (!diploma.get('courses').contains( university ))) {
        course = new CourseFinderApp.Model.Course( { degree: degree.get('label'), url: degree.get('url'), university: university.get('label')});
        diploma.get('courses').add( course );
      }
    }
});

});