
/* elational Data Model  */

/*

Institution = 5 items
An institution has many diplomas
A diploma is available trough many degree name awarded by universities

*/


// $(document).ready(function(){
(function ($) {

/********************************************
* Models and Collections
********************************************/

/* Data models / collections */
/* --------------------------*/
University = Backbone.Model.extend({
    defaults: {
        label: 'university'
    },

    initialize: function(){
      var degrees = new DegreeCollection(this.get("degrees"));
      this.set("degrees", degrees);
    }

  });
UniversityCollection = Backbone.Collection.extend({
	model: University,
	url: './mock-courses.json',
});
Degree = Backbone.Model.extend({
	defaults: {
		label: "degree name",
		requirements: "default requirements"
	}
});
DegreeCollection = Backbone.Collection.extend({
	model: Degree
});



Institution = Backbone.Model.extend({
    defaults: {
        label: 'new institution'
    }
  });

DiplomaDescription = Backbone.Model.extend({
    defaults: {
        label: 'new diploma'
    }
  });

Course = Backbone.Model.extend({
    defaults: {
        label: 'new course',
        university: 'university'
    }
  });

/* Collection models */
/* ------------------*/
InstitutionCollection = Backbone.Collection.extend({
    model: Institution,
    url: "courses.json",
    initialize: function (models, options) {
		this.bind("add", options.view.addFriendLi);//Listen for new additions to the collection and call a view function if so
    },
    render: function () {
        alert("test" + this.collection.toJSON());
    }
  });

/* Views */
/* ------------------*/
InstitutionView = Backbone.View.extend({
	tagName: "option"
});

InstitutionCollectionView = Backbone.View.extend({
	el: "#institutions",
	initialize: function () {
        this.collection = new InstitutionCollection();
        this.collection.bind("reset", this.render, this);
        this.collection.bind("change", this.render, this);
        this.collection.fetch();
    },
    render: function () {
        alert("test" + this.collection.toJSON());
    }
});


AppView = Backbone.View.extend({
    el: $("body"),
    initialize: function () {
      this.institutionList = new InstitutionCollection( null, { view: this });
      //Create a friends collection when the view is initialized.
      //Pass it a reference to this view to create a connection between the two
    },
    events: {
      "click #add-friend":  "showPrompt",
    },
    showPrompt: function () {
      var friend_name = prompt("Who is your friend?");
      var friend_model = new Friend({ name: friend_name });
      //Add a new friend model to our friend collection
      this.friends.add( friend_model );
    },
    addFriendLi: function (model) {
      //The parameter passed is a reference to the model that was added
      $("#friends-list").append("<li>" + model.get('name') + "</li>");
      //Use .get to receive attributes of the model
    }
  });

  // var appview = new AppView();
  CourseFinderApp = new Backbone.Marionette.Application({});


})(jQuery);

$(document).ready(function(){
  CourseFinderApp.addInitializer(function(options){
    universityCollection = new UniversityCollection();
    universityCollection.fetch();
  });
   CourseFinderApp.start();
});

/*

var Panel = Backbone.View.extend({ initialize: function(options){
console.log('Panel initialized');
this.foo = 'bar'; }
});
var PanelAdvanced = Panel.extend({ initialize: function(options){
Panel.prototype.initialize.call(this, [options]); console.log('PanelAdvanced initialized'); console.log(this.foo); // Log: bar
} });


*/