$(function(){
  /********************************************
  * Models and Collections
  ********************************************/
  /* This set of models and collections are there  */
  /* to perform the data json fetching             */
  /* then the model is transform in a second model */
  /* ----------------------------------------------*/
  /*

  Universities (a collection of University models)
  --Degress (a collection of Degree models)
    --Institutions (a collection of Institution models)
      --Diplomas (a collection of Diploma models)

      turn into

  Institutions (a collection of Institution models)
  --Diplomas (a collection of Diploma models)
    --Degrees (a collection of Degree models)
      --University

  */
  FetchUniversity = Backbone.Model.extend({
      initialize: function(){
        if (CourseFinderFetcherApp.debug) {console.log("|"+ this.get('label'));}
        var degrees = new FetchDegreeCollection( this.get("degrees"), { university: this } );
        this.set("degrees", degrees);
      }

    });
  FetchUniversityCollection = Backbone.Collection.extend({
    model: FetchUniversity,
    url: path + '/courses.min.json'
  });

  FetchDegree = Backbone.Model.extend({
    initialize: function(model, options) {
      if (CourseFinderFetcherApp.debug) {console.log("|__" + this.get('label'));}
      this.set({ universityAwarded : options.university });
      var institutions = new FetchInstitutionCollection( this.get("institutions"), { degree: this} );
      this.set( {institutions : institutions} );
    }
  });
  FetchDegreeCollection = Backbone.Collection.extend({
    model: FetchDegree,
    initialize: function(models, options) {

    }
  });


  FetchInstitution = Backbone.Model.extend({
    initialize: function(model, options) {
      if (CourseFinderFetcherApp.debug) {console.log("|____" + this.get('label'));}
      this.set({ degree : options.degree });
      var diplomas = new FetchDiplomaCollection( this.get("diplomas"), { institution: this} );
      this.set( {diplomas : diplomas });
    }
  });
  FetchInstitutionCollection = Backbone.Collection.extend({
    model: FetchInstitution,
    initialize: function(models, options) {

    }
  });

  FetchDiploma = Backbone.Model.extend({
    initialize: function(model, options) {
      if (CourseFinderFetcherApp.debug) {
        console.log("|______" + this.get('label'));}
      this.set({ institution : options.institution });
    }
  });
  FetchDiplomaCollection = Backbone.Collection.extend({
    model: FetchDiploma,
    initialize: function(models, options) {

    }
  });
});