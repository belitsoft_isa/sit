<?php
/**
 * @package		mod_ifaq
 * @author		Douglas Machado {@link http://idealextensions.com}
 * @author		Created on 24-Feb-2011
 * @copyright	Copyright (C) 2006 - 2015 IdealExtensions.com. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 */
// no direct access
defined('_JEXEC') or die;

class modiFAQHelper
{
	static function loadiFAQ(&$params)
	{

		$config	= JFactory::getConfig();
		$lang 	= JFactory::getLanguage();
		$lang->load('com_ifaq');

		$view		= $params->get('view','search');
		$category	= $params->get('view-category-id');
		$categories	= $params->get('view-categories-id');
		if($category AND $view == 'category'){
			JRequest::setVar('ifaq_id', $category);
		}elseif($categories AND $view == 'categories'){
			JRequest::setVar('ifaq_id', $categories);
		}
		JRequest::setVar('ifaqModule', 1);
		//if($params->get('displayDesc') == 'override') $params->set('displayDesc', 1);
		JRequest::setVar('ifaq_displayTitle',	$params->get('displayDesc', 0) );
		JRequest::setVar('ifaq_displayDesc',	$params->get('displayDesc', 0) );
		JRequest::setVar('ifaq_title',			$params->get('displayDesc-override-title', null) );
		JRequest::setVar('ifaq_description',	$params->get('displayDesc-override-description', null) );
		JRequest::setVar('q',					$params->get('search', null) );
		JRequest::setVar('ifaq_searchphrase',	$params->get('searchphrase', null) );
		JRequest::setVar('ifaq_search_type',	$params->get('search_type', null) );

		$model = JPATH_BASE.'/components/com_ifaq/models/'.$view.'.php';
		if(file_exists($model)){
			require_once($model);
			$modelClass	= 'IfaqModel'.ucfirst($view);
			$model		= new $modelClass();
		}elseif($config->getValue('config.error_reporting') == 6143 OR $conf->getValue('config.debug')){
			return JText::_('Model does not exists');
		}

	//Get view
		$viewPath = JPATH_BASE.'/components/com_ifaq/views/'.$view.'/view.html.php';
		if(file_exists($viewPath)){
			require_once(JPATH_BASE.'/components/com_ifaq/view.php');
			require_once(JPATH_BASE.'/components/com_ifaq/includes.php');
			require_once(JPATH_BASE.'/components/com_ifaq/models/'.$view.'.php');
			require_once(JPATH_BASE.'/components/com_ifaq/models/articles.php');
			require_once($viewPath);
			$viewClass	= 'IfaqView'.ucfirst($view);
			$view		= new $viewClass();

			return $view->display();

		}elseif($config->getValue('config.error_reporting') == 6143 OR $conf->getValue('config.debug')){
			return JText::sprintf('View %s does not exists', $view);
		}
	}
}
