<?php
/**
 * @package	mod_ifaq_search
 * @copyright	Copyright (C) 2006 - 2015 IdealExtensions.com. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 */
// no direct access
defined('_JEXEC') or die;

// Include the syndicate functions only once
require_once dirname(__FILE__).'/helper.php';

$lang = JFactory::getLanguage();
$lang->load('mod_ifaq',dirname(__FILE__));
$lang->load('com_ifaq',JPATH_SITE.'/components/com_ifaq');
$lang->load('com_ifaq');

$moduleclass_sfx = htmlspecialchars($params->get('moduleclass_sfx'));
$faq = modiFAQHelper::loadiFAQ($params);

require JModuleHelper::getLayoutPath('mod_ifaq', $params->get('layout', 'default'));