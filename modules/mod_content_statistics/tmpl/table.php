<?php // no direct access
defined('_JEXEC') or die('Restricted access'); ?>
<?php
if($params->get( 'Itemid' )) $itemid = "&Itemid=".$params->get( 'Itemid' );
else $itemid = "";
$document =& JFactory::getDocument();
$document->addStyleSheet(JURI::base(true).'/modules/mod_content_statistics/tmpl/statistics.css');

if(!empty($items)){
$max = $items[0]->howmuch ;

?>
<table width="100%" cellpadding="0" cellspacing="0">
<?
$k = 1 ;
foreach($items as $item){
	
	$width = round(( $item->howmuch / $max ) * 100 ) ;
	$link = JRoute::_( $item->item_link . $itemid);
?>
    <tr class="stat_item_tr stat_item_tr_<? echo $k; ?>">
        <td class="stat_item_name" valign="top">
       		<span class="item_name"><a href="<? echo $link; ?>"><? echo $item->item_name; ?></a></span>
        </td>
        <td valign="top">
        	<span class="item_count" style="width:<? echo $width; ?>%; background-color:#<? echo $params->get('linecolor','000000'); ?>;"><? echo number_format($item->howmuch,0); ?></span>
        </td>
    </tr>
<? $k = 1 - $k; } ?>
</table>
<? } ?>