<?php 

/*------------------------------------------------------------------------
# mod_content_statistics - Content Statistics Ranking Module
# ------------------------------------------------------------------------
# author				Germinal Camps
# copyright 			Copyright (C) 2011 JoomlaContentStatistics.com. All Rights Reserved.
# @license				http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Websites: 			http://www.joomlacontentstatistics.com
# Technical Support:	Forum - http://www.joomlacontentstatistics.com/forum
-------------------------------------------------------------------------*/

// no direct access
defined('_JEXEC') or die('Restricted access'); 


$document =& JFactory::getDocument();
$document->addStyleSheet(JURI::base(true).'/modules/mod_content_statistics/tmpl/statistics.css');

$document->addScript('https://www.google.com/jsapi');

$rand = rand();

$js = "";

$js .= "google.load('visualization', '1', {packages:['corechart']});
      google.setOnLoadCallback(drawChart".$rand.");
      function drawChart".$rand."() {
		  var data".$rand." = new google.visualization.DataTable();
		  data".$rand.".addColumn('string', 'Chart');
		  data".$rand.".addColumn('number', 'Slices');
		  data".$rand.".addRows(".count($items).");";

if(!empty($items)){
$max = $items[0]->howmuch ;
$i = 0 ;
foreach($items as $item){
	
	$width = round(( $item->howmuch / $max ) * 100 ) ;
	$link = JRoute::_( $item->item_link . $itemid);
	
	$js .= "data".$rand.".setValue(".$i.", 1, ".$item->howmuch.");\n" ;
	$js .= "data".$rand.".setValue(".$i.", 0, \"".addslashes($item->item_name)."\");\n" ;
	
	$i++;
 }

} 

if($params->get('linecolor')){
	$colorstring = explode(",", $params->get('linecolor')) ;
	for($i = 0, $n = count($colorstring); $i < $n; $i++){
		
		$colorstring[$i] = "'#" . $colorstring[$i] ."'";
		
	}
	
	$colorstring = implode(",", $colorstring) ;
}

if($colorstring) $colorstring = "colors: [".$colorstring."]," ;

if($params->get('backgroundcolor')){
	 $backgroundcolor = "#" . $params->get('backgroundcolor')  ;
}
else{
	$backgroundcolor =  'white' ;
}

switch($params->get('legendposition')){
	
	case "bottom": case "top":
	$chartarea = "chartArea: {width: ".($params->get('width')).", height: ".($params->get('height')-50)."}," ;
	break;	
	
	default:
	$chartarea = "chartArea: {width: ".($params->get('width')).", height: ".($params->get('height'))."}," ;
	break;	
}

if($params->get('chartsize','automatic') == "automatic") $chartarea = "" ;

$js .= "var chart".$rand." = new google.visualization.PieChart(document.getElementById('chart_div".$rand."'));
chart".$rand.".draw(data".$rand.", {
	width: ".$params->get('width').", 
	height: ".$params->get('height').",
	".$colorstring."
	".$chartarea."
	legend: '".$params->get('legendposition')."',
	fontSize: 11,
	pieSliceText: '".$params->get('slicetext', 'percentage')."',
	backgroundColor: '".$backgroundcolor."',
	is3D: ".$params->get('is3d','false')."
	});
}" ;


$document->addScriptDeclaration($js);

echo $params->get('introtext');

if($params->get('debug')) echo "<pre>".$js."</pre>" ;
echo "<div id='chart_div".$rand."'></div>" ;
echo $params->get('introtext2');	

?>