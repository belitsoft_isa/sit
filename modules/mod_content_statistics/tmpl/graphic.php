<?php // no direct access
defined('_JEXEC') or die('Restricted access'); ?>
<?php
if($params->get( 'Itemid' )) $itemid = "&Itemid=".$params->get( 'Itemid' );
else $itemid = "";
$document =& JFactory::getDocument();
$document->addStyleSheet(JURI::base(true).'/modules/mod_content_statistics/tmpl/statistics.css');

$max = $items[0]->howmuch ;

//label style
//$values['chxs'] = '1,'.$params->get('labelcolor','676767').',11.5,0,1,'.$params->get('labelcolor','676767');

$values['chxt'] = 'x,y';
$values['chs'] = $params->get('width').'x'.$params->get('height');

//chart type
$values['cht'] = 'bhs';

$values['chdlp'] = 'b';

$values['chbh'] = 'a';


//legend
$legend = $params->get('legend') ;
if($legend){
	
	$search = array('artist_name','album_name','song_name');
	$replace = array($names->artist_name, $names->album_name, $names->song_name);
	$legend = str_replace($search, $replace, $legend) ;
	
	//$legend = str_replace('artistname', $names->artist_name, $legend) ;
	
	$values['chdl'] = $legend ;
	
	$values['chdlp'] = $params->get('legendposition','b');
}

$values['chls'] = $params->get( 'linewidth', '2');
//$values['chm'] = 'B,C5D4B5BB,0,0,0' ;

//colors

$values['chco'] = $params->get('linecolor'); // line color

if($params->get('backgroundcolor')){
	$backcolors[] = 'bg,s,'.$params->get('backgroundcolor') ; //chart background color
}
if($params->get('chartareacolor')){
	$backcolors[] = 'c,s,'.$params->get('chartareacolor') ; //chart area color
}

if(count($backcolors)){
	$values['chf'] = implode("|", $backcolors) ;
}

//values printed
//$values['chbh'] = 'a';
//$values['chm'] = 'N*f0*,000000,0,-1,11' ;
//$values['chm'] = 'B,'.$params->get('fillcolor','C5D4B5').$params->get('filltransparency').',0,0,0' ;

$days = $params->get( 'num_units_time', 7 ); 
$current = $days ;
$max = 0; 

$num_labels = $params->get( 'num_labels', 1 ); 
$i = 0 ;


foreach($items as $item){
	$thedata[] = $item->howmuch ;
	if($item->howmuch > $max) $max = $item->howmuch ;
	
	$thelabels[] = $item->item_name ;
	
	$current-- ;
	$i++ ;
}

//the DATA
$values['chd'] = 't:' . implode(",", $thedata);

//the X axis labels
$values['chxl'] =  '1:|' . implode("|", array_reverse($thelabels));


if($params->get('fillcolor')) $values['chm'] = 'B,'.$params->get('fillcolor').$params->get('filltransparency').',0,0,0'  ;

$extra_space = 0 ;
if($params->get('show_values',1)){
//numbers and backcolor	
$cadena = 'N*f0*,000000,0,-1,11' ;
if($params->get('fillcolor')) $values['chm'] .= "|".$cadena ;
else $values['chm'] = $cadena ;

$extra_space = 20 ;
}


$values['chxr'] = '0,0,'. ($max)   ;
$values['chds'] = '0,' . ($max) ;

//GRID
//$horizontal_division = 100 / (count($items) - 1) ;

$horizontal_division = "-1" ;

$values['chg'] = $horizontal_division . ',0,0,0';
//<x_axis_step_size>,<y_axis_step_size>,<dash_length>,<space_length>,<x_offset>,<y_offset>

//constructing the URL
foreach($values as $key => $value){
	$string[] = $key."=".urlencode($value) ;
	$string_noencoded[] = $key."=".$value ;
}

$vars = implode("&", $string);

$img_url = "http://chart.apis.google.com/chart?" . $vars;

//echo "http://chart.apis.google.com/chart?" . implode("&", $string_noencoded);

echo JHTML::image($img_url, JText::_('Chart'));
		  
		  
?>