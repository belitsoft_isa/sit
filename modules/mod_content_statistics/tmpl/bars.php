<?php 

/*------------------------------------------------------------------------
# mod_content_statistics - Content Statistics Rankings Module
# ------------------------------------------------------------------------
# author				Germinal Camps
# copyright 			Copyright (C) 2011 JoomlaContentStatistics.com. All Rights Reserved.
# @license				http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Websites: 			http://www.joomlacontentstatistics.com
# Technical Support:	Forum - http://www.joomlacontentstatistics.com/forum
-------------------------------------------------------------------------*/

// no direct access
defined('_JEXEC') or die('Restricted access'); ?>
<?php
if($params->get( 'Itemid' )) $itemid = "&Itemid=".$params->get( 'Itemid' );
else $itemid = "";
$document =& JFactory::getDocument();
$document->addStyleSheet(JURI::base(true).'/modules/mod_content_statistics/tmpl/statistics.css');

if(!empty($items)){
$max = $items[0]->howmuch ;
$total_bars = $params->get( 'total_bars', 25 );

$color1 = $params->get('linecolor');
$color2 = $params->get('linecolor2');
$color2orig = $color2;

$color = $color1 ;

$color1 = modContentStatisticsHelper::HexToRGB($color1);
$color2 = modContentStatisticsHelper::HexToRGB($color2);

//print_r($color1);
//print_r($color2);

if($color2orig != ""){

	$dif[] = abs($color1[0] - $color2[0]) ;
	$dif[] = abs($color1[1] - $color2[1]) ;
	$dif[] = abs($color1[2] - $color2[2]) ;
	
	//print_r($dif);
	
	$dif_step[] = $dif[0] / $total_bars ;
	$dif_step[] = $dif[1] / $total_bars ;
	$dif_step[] = $dif[2] / $total_bars ;
	
	//print_r($dif_step);
	
	$sign1 = -1 ;
	$sign2 = -1 ;
	$sign3 = -1 ;
	
	if(($color1[0] - $color2[0]) < 0) $sign1 = 1 ;
	if(($color1[1] - $color2[1]) < 0) $sign2 = 1 ;
	if(($color1[2] - $color2[2]) < 0) $sign3 = 1 ;
	
	for($i = 0; $i < $total_bars; $i++){
		$r = floor($color1[0] + $dif_step[0]*$i*$sign1) ;
		$g = floor($color1[1] + $dif_step[1]*$i*$sign2) ;
		$b = floor($color1[2] + $dif_step[2]*$i*$sign3) ;
		
		$colsrgb[$i] = array($r,$g,$b) ;
		$cols[$i] = modContentStatisticsHelper::RGBToHex(array($r,$g,$b)) ;
	}
	
	//print_r($colsrgb);
	//print_r($cols);

}

echo $params->get('introtext');

foreach($items as $item){
	
	$width = round(( $item->howmuch / $max ) * 100 ) ;
	$link = JRoute::_( $item->item_link . $itemid);
	
	$num_bars = round(( $item->howmuch / $max ) * $total_bars) ;
	
	
?>

<div class="stat_item">
<span class="item_name"><a href="<? echo $link; ?>" title="<? echo htmlentities($item->item_name); ?>"><? echo modContentStatisticsHelper::crop_string($item->item_name, $params->get('cropstring', 0)); ?></a> (<? echo $params->get( 'symbol_before','' ).number_format($item->howmuch,$params->get( 'decimal',0 )).$params->get( 'symbol_after','' ); ?>)</span>
<div class="bar_space">
<? for($i = 0; $i < $num_bars; $i++){
if($color2orig) $color = $cols[$i] ;
?>
<span class="item_count_bars" style="background-color:#<? echo $color; ?>;"></span>
<? } ?>
<? for($i = 0; $i < $total_bars - $num_bars; $i++){ ?>
<span class="item_count_bars empty_bar"></span>
<? } ?>
</div>
</div>

<? }

} ?>
<? echo $params->get('introtext2'); ?>