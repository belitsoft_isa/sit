<?php // no direct access
defined('_JEXEC') or die('Restricted access'); ?>
<?php
if($params->get( 'Itemid' )) $itemid = "&Itemid=".$params->get( 'Itemid' );
else $itemid = "";
$document =& JFactory::getDocument();
$document->addStyleSheet(JURI::base(true).'/modules/mod_content_statistics/tmpl/statistics.css');

if(!empty($items)){
$max = $items[0]->howmuch ;

foreach($items as $item){
	
	$width = round(( $item->howmuch / $max ) * 100 ) ;
	$link = JRoute::_( $item->item_link . $itemid);
?>

<div class="stat_item">
<span class="item_name"><a href="<? echo $link; ?>"><? echo $item->item_name; ?></a></span>
<span class="item_count" style="width:<? echo $width; ?>%; background-color:#<? echo $params->get('linecolor','000000'); ?>;"><? echo number_format($item->howmuch,0); ?></span>
</div>

<? }

} ?>