<?php

/*------------------------------------------------------------------------
# mod_content_statistics - Content Statistics Rankings Module
# ------------------------------------------------------------------------
# author				Germinal Camps
# copyright 			Copyright (C) 2011 JoomlaContentStatistics.com. All Rights Reserved.
# @license				http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Websites: 			http://www.joomlacontentstatistics.com
# Technical Support:	Forum - http://www.joomlacontentstatistics.com/forum
-------------------------------------------------------------------------*/

// no direct access
defined('_JEXEC') or die('Restricted access');

class modContentStatisticsHelper
{

	function getItems(&$params){
		
		$mainframe =& JFactory::getApplication();
		$_db	= & JFactory::getDBO();
		$user	= & JFactory::getUser();
		
		$component = $params->get( 'component' );
	
		$criteria = $params->get( 'criteria' );
		$selector = $params->get( 'selector' );
		$specific_id = $params->get( 'specific_id' );
		
		$num = $params->get( 'num_items' );
		
		$limit = ' LIMIT ' . $num ;
		
		$time = $params->get( 'time' ) ; // en hores
		$timeabs = $params->get( 'timeabs' ) ;
		
		switch($params->get( 'mode', 'relative' )){
			case 'absolute':
				switch($timeabs){
					case 'TODAY':
					$more_where = ' AND st.date_event BETWEEN "'.date('Y-m-d').' 00:00:00" AND NOW() ' ;
					break;
					case 'YESTERDAY':
					$more_where = ' AND st.date_event BETWEEN SUBDATE(CURDATE(),1) AND CURDATE() ' ;
					break;
					case 'WEEK':
					$weekstarts = $params->get( 'weekstarts' ) ;
					$today = (int)date('w') ;
					$diff = ($today - $weekstarts)%7 ;
					$more_where = ' AND st.date_event BETWEEN SUBDATE(CURDATE(),'.$diff.') AND NOW() ' ;
					break;
					case 'LASTWEEK':
					$weekstarts = $params->get( 'weekstarts' ) ;
					$today = (int)date('w') ;
					$diff = ($today - $weekstarts)%7 ;
					$more_where = ' AND st.date_event BETWEEN SUBDATE(CURDATE(),'.($diff + 7).') AND SUBDATE(CURDATE(),'.$diff.') ' ;
					break;
					case 'MONTH':
					$more_where = ' AND st.date_event BETWEEN "'.date('Y-m').'-01 00:00:00" AND NOW() ' ;
					break;
					case 'LASTMONTH':
					if((int)date('m') == 1){ $year = (int)date('Y') - 1 ; $month = 12 ;}
					else { $year = date('Y') ; $month = date('m') - 1 ; }
					$more_where = ' AND st.date_event BETWEEN "'.$year.'-'.$month.'-01 00:00:00" AND "'.date('Y-m').'-01 00:00:00" ' ;
					break;
					case 'YEAR':
					$more_where = ' AND st.date_event BETWEEN "'.date('Y').'-01-01 00:00:00" AND NOW() ' ;
					break;
					case 'LASTYEAR':
					$more_where = ' AND st.date_event BETWEEN "'.((int)date('Y') - 1).'-01-01 00:00:00" AND "'.date('Y').'-01-01 00:00:00" ' ;
					break;
				}
			break;
			case 'relative':default:
				if($time){
					$more_where = ' AND st.date_event > ( NOW() - INTERVAL '.$time.' ) ' ;
				}
			break;
		}
		
		
		$viewer = $params->get( 'filter_user' );
		$specific_user = $params->get( 'filter_specific_user' );
		if($viewer){
			if($user->id) $more_where .= ' AND st.user_id = ' . $user->id ;
			else $more_where .= ' AND 0 ' ; // return NOTHING
		}
		elseif($specific_user){
			$more_where .= ' AND st.user_id = ' . $specific_user ;
		}
		
		$where = ' AND st.component = "'.$component.'" ' . $more_where ;
		
		//we call the PLUGIN specific function to get the QUERY
		$dispatcher   =& JDispatcher::getInstance();
		$plugin_ok = JPluginHelper::importPlugin('contentstats', $component);
		
		// getQueryRanking($criteria, $selector, $specific_id, $more_where){
		//$results = $dispatcher->trigger('getQueryRanking', array($criteria, $selector, $specific_id, $where, $params));
		$results = $dispatcher->trigger('getQueryRanking_'.$component, array($criteria, $selector, $specific_id, $where, $params));
		//print_r($results);
		foreach($results as $result){
			if($result->component == $component){
				$query = $result->query ;
			}
		}
		
		$query = 	$query .
					$limit
					; 
		
		//echo $query;die;
		
		$_db->setQuery( $query );
		$items = $_db->loadObjectList();
			
		//print_r($items);die;		
		
		return $items;

	}
	
	function crop_string($string, $size){
		$len = mb_strlen($string);
		if($len > $size && $size > 0){
			$return = mb_substr($string, 0, $size - 3) . "...";
		}
		else $return = $string;
		return $return;
	}
	
	function GiveDec($hex)
	{
	   if($hex == "A")
		  $value = 10;
	   else
	   if($hex == "B")
		  $value = 11;
	   else
	   if($hex == "C")
		  $value = 12;
	   else
	   if($hex == "D")
		  $value = 13;
	   else
	   if($hex == "E")
		  $value = 14;
	   else
	   if($hex == "F")
		  $value = 15;
		else $value = $hex ;
	   
	   return $value;
	}
	function GiveHex($dec)
	{
	   if($dec == 10)
		  $value = "A";
	   else
	   if($dec == 11)
		  $value = "B";
	   else
	   if($dec == 12)
		  $value = "C";
	   else
	   if($dec == 13)
		  $value = "D";
	   else
	   if($dec == 14)
		  $value = "E";
	   else
	   if($dec == 15)
		  $value = "F";
	   else
		  $value = "" + $dec;
	   return $value;
	}
	function HexToRGB($input)
	{
	   $input = strtoupper($input);
	  
	   $a = modContentStatisticsHelper::GiveDec(substr($input,0, 1));
	   $b = modContentStatisticsHelper::GiveDec(substr($input,1, 1));
	   $c = modContentStatisticsHelper::GiveDec(substr($input,2, 1));
	   $d = modContentStatisticsHelper::GiveDec(substr($input,3, 1));
	   $e = modContentStatisticsHelper::GiveDec(substr($input,4, 1));
	   $f = modContentStatisticsHelper::GiveDec(substr($input,5, 1));
	
	   $x = ($a * 16) + $b;
	   $y = ($c * 16) + $d;
	   $z = ($e * 16) + $f;
	
	   $return = array($x, $y, $z);
	   return $return ;
	}
	function RGBToHex($input)
	{
	   $Red = $input[0] ;
	   $Green = $input[1] ;
	   $Blue = $input[2] ;
	
	   $a = modContentStatisticsHelper::GiveHex(floor($Red / 16));
	   $b = modContentStatisticsHelper::GiveHex($Red % 16);
	   $c = modContentStatisticsHelper::GiveHex(floor($Green / 16));
	   $d = modContentStatisticsHelper::GiveHex($Green % 16);
	   $e = modContentStatisticsHelper::GiveHex(floor($Blue / 16));
	   $f = modContentStatisticsHelper::GiveHex($Blue % 16);
	
	   $z = $a . $b . $c . $d . $e . $f;
	
	   return $z;
	}

}