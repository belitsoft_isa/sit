<?php
/**
 * RokMiniEvents Module
 * @copyright Copyright (C) 2009 RocketTheme. All rights reserved.
 * @license http://www.gnu.org/copyleft/gpl.html GNU/GPL, see RT-LICENSE.php
 * @author RocketTheme, LLC
 *
 */

// Check to ensure this file is included in Joomla!
die();
?>

1. Copyright and disclaimer
----------------


2. Changelog
------------
This is a non-exhaustive changelog for RokNavMenu, inclusive of any alpha, beta, release candidate and final versions.

Legend:

* -> Security Fix
# -> Bug Fix
+ -> Addition
^ -> Change
- -> Removed
! -> Note

------- 1.5 Release [31-Mar-2011] -------
# Removed reference param for cache call
+ Added RSEvents as source
+ Added JEvents as source
+ Added EventList as source
# Fixed component language load
# Fixed showing/hiding past events
# Fixed errors when no events returned

------- 1.4 Release [23-Feb-2011] -------
# Fix for trimming not happening on non space whitespace characters
# Fixed jomsocial date skew when jomsocial timezone offset set

------- 1.3 Release [14-Feb-2011] -------
# Fixed logged in jomsocial events not showing
# Made sure all events being truncated

------- 1.2 Release [04-Feb-2011] -------
# Fixed timezone offset issue again

------- 1.1 Release [02-Feb-2011] -------
# Fix for loading user with 0 id
+ Added description trimming
+ Added force timezone option
# Fixed timezone offset to get server offset by default

------- 1.0 Release [31-Jan-2011] -------
! Initial Release
