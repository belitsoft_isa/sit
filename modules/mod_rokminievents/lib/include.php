<?php
/**
 * @version   1.5 March 31, 2011
 * @author    RocketTheme http://www.rockettheme.com
 * @copyright Copyright (C) 2007 - 2011 RocketTheme, LLC
 * @license   http://www.gnu.org/licenses/gpl-2.0.html GNU/GPLv2 only
 */

if (!defined('RTCOMMON'))
{
    define('RTCOMMON', 'RTCOMMON');
    require_once(dirname(__FILE__) . '/RTCommon/ClassLoader.php');
}
