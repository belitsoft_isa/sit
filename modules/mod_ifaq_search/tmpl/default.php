<?php
/**
 * @package		iFAQ
 * @copyright	Copyright (C) 2006 - 2015 IdealExtensions.com. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 */
// no direct access
defined('_JEXEC') or die;
?>
<form action="<?php echo JRoute::_('index.php?option=com_ifaq&amp;view=search'.($mitemid ? '&amp;Itemid='.$mitemid : ''));?>" method="post">
	<div class="search<?php echo $moduleclass_sfx ?>">
		<?php
			if ($button) :
				if ($imagebutton) :
					$button = '<input type="image" value="'.$button_text.'" class="button'.$moduleclass_sfx.'" src="'.$img.'" onclick="this.form.searchword.focus();"/>';
				else :
					$button = '<input type="submit" value="'.$button_text.'" class="button'.$moduleclass_sfx.'" onclick="this.form.searchword.focus();"/>';
				endif;
				$style = '';
			else:
				$style = ' style="width:95%" ';
			endif;

			$output = '<label for="mod-ifaq-search-searchword">'.$label.'</label>'
					.'<input name="q" id="mod-ifaq-search-searchword" maxlength="'.$maxlength.'"
							class="inputbox'.$moduleclass_sfx.'" '.$style.'
							type="search" size="'.$width.'" value="'.$text.'"
 							placeholder="'.htmlspecialchars($params->get('text', JText::_('MOD_IFAQ_SEARCH_SEARCHBOX_TEXT'))).'" />';

			switch ($button_pos) :
				case 'top' :
					$button = $button.'<br />';
					$output = $button.$output;
					break;

				case 'bottom' :
					$button = '<br />'.$button;
					$output = $output.$button;
					break;

				case 'right' :
					$output = $output.$button;
					break;

				case 'left' :
				default :
					$output = $button.$output;
					break;
			endswitch;

			echo $output;
		?>
	<input type="hidden" name="option" value="com_ifaq" />
	<input type="hidden" name="Itemid" value="<?php echo $mitemid; ?>" />
	<input type="hidden" name="moduleId"   value="<?php echo $module->id; ?>" />
	</div>
</form>
