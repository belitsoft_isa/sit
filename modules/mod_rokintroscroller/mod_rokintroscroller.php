<?php
/**
* @version   1.4 August 9, 2010
* @author    RocketTheme http://www.rockettheme.com
* @copyright Copyright (C) 2007 - 2010 RocketTheme, LLC
* @license   http://www.gnu.org/licenses/gpl-2.0.html GNU/GPLv2 only
*/


// no direct access
defined('_JEXEC') or die('Restricted access');

// Include the syndicate functions only once
require_once (dirname(__FILE__).DS.'helper.php');
modRokIntroScrollerHelper::loadScripts($module, $params);
// Cache this basd on access level
$conf =& JFactory::getConfig();
if ($conf->getValue('config.caching') && $params->get("module_cache", 0)) { 
	$user =& JFactory::getUser();
	$aid  = (int) $user->get('aid', 0);
	switch ($aid) {
	    case 0:
	        $level = "public";
	        break;
	    case 1:
	        $level = "registered";
	        break;
	    case 2:
	        $level = "special";
	        break;
	}
	// Cache this based on access level
	$cache =& JFactory::getCache('mod_rokintroscroller-' . $level);
	$list = $cache->call(array('modRokIntroScrollerHelper', 'getList'), $params);
}
else {
    $list = modRokIntroScrollerHelper::getList($params);
}

$counter = 0;
require(JModuleHelper::getLayoutPath('mod_rokintroscroller'));