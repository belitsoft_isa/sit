<?php // no direct access
/**
* @version   1.4 August 9, 2010
* @author    RocketTheme http://www.rockettheme.com
* @copyright Copyright (C) 2007 - 2010 RocketTheme, LLC
* @license   http://www.gnu.org/licenses/gpl-2.0.html GNU/GPLv2 only
*/
defined('_JEXEC') or die('Restricted access'); 
?>
<div class="scroller-bottom">
	<div class="scroller-bottom1">
		<div class="scroller-bottom2">
			<div class="scroller-top">
				<div class="scroller-top1">
					<div class="scroller-top2">
						<!-- Content START -->
							<div id="rokintroscroller" class="<?php echo $params->get('moduleclass_sfx'); ?>">
								


        <?php foreach ($list as $item) :  ?>
        <div class="scroller-item">
			<?php if ($params->get('show_title') == 1) :?>
				<?php if ($params->get('link_title') == 1) :?>
				<h2><a href="<?php echo $item->link; ?>"><?php echo $item->title; ?></a></h2>
				<?php else: ?>
				<h2><?php echo $item->title; ?></h2>
				<?php endif; ?>
			<?php endif; ?>
	
            <?php echo $item->introtext; ?> 

            <?php if ($params->get('show_readmore') == 1) :?>
            <a href="<?php echo $item->link; ?>" class="readon3"><?php echo $params->get('readmore'); ?></a> 
            <?php endif; ?>
       
        </div>
    <?php endforeach; ?>
							<!-- Content END -->		


							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

