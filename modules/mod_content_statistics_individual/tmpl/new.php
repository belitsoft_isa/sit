<?php 

/*------------------------------------------------------------------------
# mod_content_statistics_individual - Content Statistics Evolution Module
# ------------------------------------------------------------------------
# author				Germinal Camps
# copyright 			Copyright (C) 2011 JoomlaContentStatistics.com. All Rights Reserved.
# @license				http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Websites: 			http://www.joomlacontentstatistics.com
# Technical Support:	Forum - http://www.joomlacontentstatistics.com/forum
-------------------------------------------------------------------------*/

// no direct access
defined('_JEXEC') or die('Restricted access'); 


$document =& JFactory::getDocument();
$document->addStyleSheet(JURI::base(true).'/modules/mod_content_statistics_individual/tmpl/statistics.css');

$document->addScript('https://www.google.com/jsapi');

$rand = rand();

$js = "";

if($params->get('backgroundcolor')){
	$backcolors[] = 'bg,s,'.$params->get('backgroundcolor') ; //chart background color
}
if($params->get('chartareacolor')){
	$backcolors[] = 'c,s,'.$params->get('chartareacolor') ; //chart area color
}


$days = $params->get( 'num_units_time', 7 ); 
$current = $days ;
$max = 0; 

$num_labels = $params->get( 'num_labels', 1 ); 
$i = 0 ;


$date_format = $params->get('other_label_format') ? $params->get('other_label_format') : $params->get('label_format', '%a') ;

$average = 0;

$js .= "google.load('visualization', '1', {packages:['corechart']});
      google.setOnLoadCallback(drawChart".$rand.");
      function drawChart".$rand."() {
		  var data".$rand." = new google.visualization.DataTable();
		  data".$rand.".addColumn('string', 'Time');
		  data".$rand.".addColumn('number', '".$params->get('dataname')."');
		  data".$rand.".addRows(".count($items).");";


foreach($items as $item){
	
	if($item->howmuch > $max) $max = $item->howmuch ;
	
	$label = JHTML::_('date', $item->date_event, JText::_($date_format)) ;
	
	if(!$item->howmuch) $item->howmuch = 0 ;
	
	$average += $item->howmuch ;
	
	$js .= "data".$rand.".setValue(".$i.", 1, ".$item->howmuch.");\n" ;
	$js .= "data".$rand.".setValue(".$i.", 0, \"".$label."\");\n" ;
	
	$current-- ;
	$i++ ;
}

$average = $average / count($items) ;

if($params->get( 'average')){
	//$values['chd'] .= '|' . $average;	
}


if($params->get('markers')){
	 $markersize = $params->get('markersize')  ;
}
else{
	$markersize =  0 ;
}

if($params->get('backgroundcolor')){
	 $backgroundcolor = "#" . $params->get('backgroundcolor')  ;
}
else{
	$backgroundcolor =  'white' ;
}


switch($params->get('chart','lc')){
	case 'bvg':
		$chartype = "ColumnChart" ;
	
	break;
	case 'smooth':
		$chartype = "LineChart" ;
		//if($params->get('fillcolor')) $chartype = "AreaChart" ;
		$curveType = "curveType: 'function'," ;
	break;
	case 'area':
		$chartype = "AreaChart" ;
	break;
	case 'stepped':
		$chartype = "SteppedAreaChart" ;
	break;
	case 'steppedunconnected':
		$chartype = "SteppedAreaChart" ;
		$steps = "connectSteps: false," ;
	break;
	case 'lc':default:
		$chartype = "LineChart" ;
		if($params->get('fillcolor')) $chartype = "AreaChart" ;
	break;
}

$js .= "var chart".$rand." = new google.visualization.".$chartype."(document.getElementById('chart_div".$rand."'));
chart".$rand.".draw(data".$rand.", {
	width: ".$params->get('width').", 
	height: ".$params->get('height').",
	colors: ['#".$params->get('linecolor')."'],
	lineWidth: ".$params->get( 'linewidth', '2').",
	".$curveType."
	".$steps."
	chartArea: {left: 30, top: 10, width: ".($params->get('width')-50).", height: ".($params->get('height')-40)."},
	legend: 'none',
	fontSize: 11,
	hAxis: {showTextEvery: ".$params->get('num_labels').",
			textStyle: {color: '#".$params->get('labelcolor','999999')."'},
			},
	vAxis: {textStyle: {color: '#".$params->get('labelcolor','999999')."'},
			},
	pointSize: ".$markersize.",
	backgroundColor: '".$backgroundcolor."'
	});
}" ;

$document->addScriptDeclaration($js);

echo $params->get('introtext');

if($params->get('debug')) echo "<pre>".$js."</pre>" ;
echo "<div id='chart_div".$rand."'></div>" ;
echo $params->get('introtext2');		  
		  
?>