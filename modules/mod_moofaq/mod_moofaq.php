<?php
/**
 * @version $Id$
 * @package    MooFAQ
 * @author     Douglas Machado {@link http://ideal.fok.com.br}
 * @author     Created on 25-Feb-2010
 */

//-- No direct access
defined('_JEXEC') or die('=;)');

$config		=& JFactory::getConfig();


$view		= $params->get('view','search');
$category	= $params->get('view-category-id');
$categories	= $params->get('view-categories-id');
if($category AND $view == 'category'){
	JRequest::setVar('moofaq_id', $category);
}elseif($categories AND $view == 'categories'){
	JRequest::setVar('moofaq_id', $categories);
}
JRequest::setVar('moofaqModule', 1);
//if($params->get('displayDesc') == 'override') $params->set('displayDesc', 1);
JRequest::setVar('moofaq_displayTitle',	$params->get('displayDesc', 0) );
JRequest::setVar('moofaq_displayDesc',	$params->get('displayDesc', 0) );
JRequest::setVar('moofaq_title',		$params->get('displayDesc-override-title', null) );
JRequest::setVar('moofaq_description',	$params->get('displayDesc-override-description', null) );
JRequest::setVar('q',					$params->get('search', null) );
JRequest::setVar('moofaq_searchphrase',	$params->get('searchphrase', null) );
JRequest::setVar('moofaq_search_type',	$params->get('search_type', null) );



$viewPath = JPATH_BASE.DS.'components'.DS.'com_moofaq'.DS.'views'.DS.$view.DS.'view.html.php';
	if(file_exists($viewPath)){
		require_once(JPATH_BASE.DS.'components'.DS.'com_moofaq'.DS.'models'.DS.$view.'.php');
		require_once(JPATH_BASE.DS.'components'.DS.'com_moofaq'.DS.'view.php');
		require_once(JPATH_BASE.DS.'components'.DS.'com_moofaq'.DS.'includes.php');
		require_once($viewPath);
		$viewClass	= 'MoofaqView'.ucfirst($view);
		$view		= new $viewClass();
		
		$html = $view->display();
		
	}elseif($config->getValue('config.error_reporting') == 6143 OR $conf->getValue('config.debug')){
		echo JText::_('ERROR: MooFAQ is either not installed or this module is not compatible with the version installed');
	}