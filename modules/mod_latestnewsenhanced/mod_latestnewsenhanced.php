<?php
/**
 * @copyright	Copyright (C) 2011 Simplify Your Web, Inc. All rights reserved.
 * @license		GNU General Public License version 3 or later; see LICENSE.txt
 */

// no direct access
defined('_JEXEC') or die('Restricted access');

// Include the syndicate functions only once
require_once (dirname(__FILE__).'/helper.php');

$list = modLatestNewsEnhancedHelper::getList($module->id, $params);

if (empty($list)) {
	return;
}

require(JModuleHelper::getLayoutPath('mod_latestnewsenhanced'));
?>
