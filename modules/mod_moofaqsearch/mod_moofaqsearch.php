<?php
/**
 * @version $Id$
 * @package    MooFAQ
 * @author     Douglas Machado {@link http://ideal.fok.com.br}
 * @author     Created on 25-Feb-2010
 */

//-- No direct access
defined('_JEXEC') or die('=;)');



$config		=& JFactory::getConfig();
$viewPath = JPATH_BASE.DS.'components'.DS.'com_moofaq'.DS.'views'.DS.'search'.DS.'view.html.php';
if(file_exists($viewPath)){
	require_once( dirname(__FILE__).DS.'helper.php' );

	$button				= $params->get('button', '');
	$imagebutton		= $params->get('button-show-imagebutton', '');
	$button_pos			= $params->get('button-show-pos', 'left');
	$button_text		= $params->get('button-show-text', JText::_('Search'));
	$width				= intval($params->get('width', 20));
	$maxlength			= $width > 20 ? $width : 20;
	$text				= $params->get('text', JText::_('search...'));
	$set_Itemid			= intval($params->get('set_itemid', 0));
	$moduleclass_sfx	= $params->get('moduleclass_sfx', '');
	$moofaqItemid		= intval($params->get('menuitem', null));
	$exclude_categories	= $params->get('exclude-categories', null);
	$exclude_sections	= $params->get('exclude-sections', null);
			
	
	if(is_array($exclude_categories)){
		$exclude_categories	= implode( ',', $exclude_categories);
	}
	if(is_array($exclude_sections)){
		$exclude_sections	= implode( ',', $exclude_sections);
	}
	
	$menus	= &JApplication::getMenu('site', array());
	$items	= $menus->getItems('id',$moofaqItemid);
	if(count($items)>0 AND isset($items[0]->link)){
		$link 	= $items[0]->link.'&Itemid='.$items[0]->id;
	}else{
		$link = 'index.php?option=com_moofaq&view=search&Itemid='.$moofaqItemid;
	}
	
	 
	if ($imagebutton) {
	    $img = modMooFAQSearchHelper::getSearchImage( $button_text );
	}
	require(JModuleHelper::getLayoutPath('mod_moofaqsearch'));
	
}elseif($config->getValue('config.error_reporting') == 6143 OR $conf->getValue('config.debug')){
	echo JText::_('ERROR: MooFAQ is either not installed or this Search module is not compatible with the version installed');
}