<?php // no direct access
defined('_JEXEC') or die('Restricted access'); 


?>

<form action="<?php echo JRoute::_($link); ?>" method="get">
	<div class="search<?php echo $params->get('moduleclass_sfx') ?>">
		<?php
		    $output = '<input name="q" id="moofaq_search" class="inputbox" maxlength="'.$maxlength.'" alt="'.$button_text.'" class="inputbox'.$moduleclass_sfx.'" type="text" size="'.$width.'" value="'.(JRequest::getVar('q',$text)).'"  onblur="if(this.value==\'\') this.value=\''.$text.'\';" onfocus="if(this.value==\''.$text.'\') this.value=\'\';" />';

			if ($button == 'show') :
			    if ($imagebutton) :
			        $button = ' <input type="image" value="'.$button_text.'" class="button moofaqbutton" src="'.$img.'" onclick="this.form.searchword.focus();"/>';
			    else :
			        $button = ' <input type="submit" value="'.$button_text.'" class="button moofaqbutton" onclick="this.form.searchword.focus();"/>';
			    endif;
			else:
				$button	= '';
			endif;

			switch ($button_pos) :
			    case 'top' :
				    $button = $button.'<br />';
				    $output = $button.$output;
				    break;

			    case 'bottom' :
				    $button = '<br />'.$button;
				    $output = $output.$button;
				    break;

			    case 'right' :
				    $output = $output.$button;
				    break;

			    case 'left' :
			    default :
				    $output = $button.$output;
				    break;
			endswitch;

			echo $output;
		?>
	</div>
	<?php /* if($moofaqItemid > 0): ?>
		<input type="hidden" name="Itemid"   value="<?php echo $moofaqItemid; ?>" />
	<?php endif; ?>
	
	<?php if($exclude_categories): ?>
		<input type="hidden" name="moofaq_exclude_categories"   value="<?php echo $exclude_categories; ?>" />
	<?php endif; ?>
	
	<?php if($exclude_sections): ?>
		<input type="hidden" name="moofaq_exclude_categories"   value="<?php echo $exclude_sections; ?>" />
	<?php endif; */ ?>
	<?php 
		/*
	<input type="hidden" name="view"   value="search" />
	<input type="hidden" name="option" value="com_moofaq" />*/
	?>
	<input type="hidden" name="moduleId"   value="<?php echo $module->id; ?>" />

</form>