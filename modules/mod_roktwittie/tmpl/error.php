<?php
/**
 * RokTwittie Module
 *
 * @package RocketTheme
 * @subpackage roktwittie.tmpl
 * @version   2.5 June 13, 2011
 * @author    RocketTheme http://www.rockettheme.com
 * @copyright Copyright (C) 2007 - 2011 RocketTheme, LLC
 * @license   http://www.gnu.org/licenses/gpl-2.0.html GNU/GPLv2 only
 *
 */

defined('_JEXEC') or die('Restricted access');

$error = (isset($status) && is_string($status)) ? $status : (isset($friends) ? $friends : '');
$error = (is_string($error)) ? json_decode($error) : '';
if (isset($error->error)) $error = $error->error;
?>

<div id="roktwittie" class="roktwittie<?php echo $params->get('moduleclass_sfx'); ?>">
	<div class="error">
		<?php echo $error; ?>
	</div>
</div>