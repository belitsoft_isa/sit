<?php
/**
 * RokNewsFlash Module
 *
 * @package RocketTheme
 * @subpackage roknewsflash.tmpl
 * @version   1.5 June 1, 2011
 * @author    RocketTheme http://www.rockettheme.com
 * @copyright Copyright (C) 2007 - 2011 RocketTheme, LLC
 * @license   http://www.gnu.org/licenses/gpl-2.0.html GNU/GPLv2 only
 *
 */
// no direct access
defined('_JEXEC') or die;
$doc = JFactory::getDocument();
$doc->addStyleSheet(JURI::Root(true).'/modules/mod_roknewsflash/tmpl/css/main.css');
$doc->addStyleSheet(JURI::Root(true).'/modules/mod_roknewsflash/tmpl/css/font-awesome.min.css');

?>


<?php /*

<div id="newsflash" class="roknewsflash <?php echo $params->get('moduleclass_sfx'); ?>">

    <span class="flashing"><?php echo $params->get('pretext'); ?></span>
    <div id="nt-example1-container" class="<?php echo $params->get('moduleclass_sfx'); ?>">
        <i class="fa fa-arrow-up" id="nt-example1-prev"></i>
        <ul id="nt-example1">
<?php foreach ($list as $item) :  ?>
		<li>
		    <a href="<?php echo $item->link; ?>">
		    <?php
		    if ($params->get('usetitle')==1) {
		        echo ($item->title);
		    } else {
		        echo ($item->introtext . '...');
		    }
		    ?>
  		    </a>
		</li>
<?php endforeach; ?>
    </ul>
        <i class="fa fa-arrow-down" id="nt-example1-next"></i>
        </div>
</div>
<?php */
?>

<div id="newsflash" class="roknewsflash <?php echo $params->get('moduleclass_sfx'); ?>">

    <span class="flashing"><?php echo $params->get('pretext'); ?></span>
    <div id="nt-example1-container" class="<?php echo $params->get('moduleclass_sfx'); ?>">
        <i class="fa fa-arrow-up" id="nt-example1-prev"></i>
        <ul id="nt-example1">
            <?php foreach ($list as $item) :  ?>
                <li>
                    <a href="<?php echo $item->link; ?>">
                        <?php
                        if ($params->get('usetitle')==1) {
                            echo ($item->title);
                        } else {
                            echo ($item->introtext . '...');
                        }
                        ?>
                    </a>
                </li>
            <?php endforeach; ?>
        </ul>
        <i class="fa fa-arrow-down" id="nt-example1-next"></i>
    </div>
</div>



<script src="<?php echo JURI::Root();?>/modules/mod_roknewsflash/tmpl/js/jquery.newsTicker.js"></script>
<script>

    jQuery('#nt-example1').newsTicker({
        row_height: "<?php echo $params->get('row_height'); ?>",
        max_rows: "<?php echo $params->get('max_rows'); ?>",
        duration: "<?php echo $params->get('duration'); ?>",
        prevButton: jQuery('#nt-example1-prev'),
        nextButton: jQuery('#nt-example1-next')
    });



</script>