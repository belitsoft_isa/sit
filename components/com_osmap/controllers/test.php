<?php
/**
 * @package   OSMap
 * @copyright 2007-2014 XMap - Joomla! Vargas. All rights reserved.
 * @copyright 2015 Open Source Training, LLC. All rights reserved..
 * @author    Guillermo Vargas <guille@vargas.co.cr>
 * @author    Alledia <support@alledia.com>
 * @license   GNU General Public License version 2 or later; see LICENSE.txt
 *
 * This file is part of OSMap.
 *
 * OSMap is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * any later version.
 *
 * OSMap is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OSMap. If not, see <http://www.gnu.org/licenses/>.
 */
// No direct access
defined('_JEXEC') or die('Restricted access');

jimport('joomla.application.component.controller');

/**
 * OSMap Ajax Controller
 *
 * @package      OSMap
 * @subpackage   com_osmap
 * @since        2.0
 */
class OSMapControllerTest extends JControllerLegacy
{

   public function replaceend(){
       $db = JFactory::getDbo();

       $query = $db->getQuery(true);
       $query->select('*')
           ->from('#__content')
           ->where('introtext LIKE  "%<p>{article_slider_end}</p>%"');

       $result = $db->setQuery($query)->loadObjectList();
       foreach($result AS $res){
           $object = new stdClass();

// Must be a valid primary key value.
           $object->id = $res->id;
           $object->introtext = str_replace('<p>{article_slider_end}</p>','</div></div>', $res->introtext);


// Update their details in the users table using id as the primary key.
           $result = JFactory::getDbo()->updateObject('#__content', $object, 'id');
       }
       die;
       return $result;
   }

    public function replacestart(){

        $db = JFactory::getDbo();

        $query = $db->getQuery(true);
        $query->select('*')
            ->from('#__content_copy')
            ->where('introtext LIKE  "%{article_slider_begin%"');

        $result = $db->setQuery($query)->loadObjectList();
        foreach($result AS $res){
            $object = new stdClass();
            $rand=rand();
// Must be a valid primary key value.
            $object->id = $res->id;
            $object->introtext = str_replace('{article_slider_begin title="','<div class="accordion-section-wrap"><input type="checkbox" id="someid-'.$rand.'" /><label for="someid-'.$rand.'">', $res->introtext);
            $object->introtext = str_replace('" expanded="0"}</p>','</label><div class="accordion-content">', $object->introtext);


// Update their details in the users table using id as the primary key.
            $result = JFactory::getDbo()->updateObject('#__content_copy', $object, 'id');
        }
        die;
    }

    public function replaceendportfolio(){
        $db = JFactory::getDbo();

        $query = $db->getQuery(true);
        $query->select('*')
            ->from('#__advportfolio_projects')
            ->where('description LIKE  "%<p>{article_slider_end}</p>%"');

        $result = $db->setQuery($query)->loadObjectList();

        foreach($result AS $res){
            $object = new stdClass();

// Must be a valid primary key value.
            $object->id = $res->id;
            $object->description = str_replace('<p>{article_slider_end}</p>','</div></div>', $res->description);


// Update their details in the users table using id as the primary key.
            $result = JFactory::getDbo()->updateObject('#__advportfolio_projects', $object, 'id');
        }
        die;
        return $result;
    }

    public function replacestartportfolio(){

        $db = JFactory::getDbo();

        $query = $db->getQuery(true);
        $query->select('*')
            ->from('#__advportfolio_projects')
            ->where('description LIKE  "%{article_slider_begin%"');

        $result = $db->setQuery($query)->loadObjectList();
        foreach($result AS $res){
            $object = new stdClass();

// Must be a valid primary key value.
            $object->id = $res->id;
            $object->description = str_replace('<p>{article_slider_begin title="','<div class="accordion-section-wrap"><input type="checkbox"/ id="someid-1"><label for="someid-1">', $res->description);
            $object->description = str_replace('" expanded="0"}</p>','</label><div class="accordion-content">', $object->description);


// Update their details in the users table using id as the primary key.
            $result = JFactory::getDbo()->updateObject('#__advportfolio_projects', $object, 'id');
        }
        die;
    }

    public function replaceinput(){

        $db = JFactory::getDbo();

        $query = $db->getQuery(true);
        $query->select('*')
            ->from('#__advportfolio_projects')
            ->where("description LIKE  '%someid-1%'");

        $result = $db->setQuery($query)->loadObjectList();

        $delimiter='<input type="checkbox"';
        $i = 0;
        foreach($result AS $res){
            $sentArray = explode( $delimiter, $res->description);
            $bigPiecesArray[0] = '';
            $object = new stdClass();
            for ($k = 0; $k < count($sentArray); $k++) {

                $rand=rand();
                $object->description =str_replace('"someid-1"','"someid-'.$rand.'"',  $sentArray[$k]);
                $bigPiecesArray[$i] .= $object->description.$delimiter;

            }


// Must be a valid primary key value.
         $object->id = $res->id;
          $object->description = $bigPiecesArray[0];
            $db = JFactory::getDbo();

            $approveQuery = "UPDATE `#__advportfolio_projects` SET `description` = ".$db->quote($object->description)." WHERE `id` = " . (int)$res->id;
            $db->setQuery($approveQuery)->query();

// Update their details in the users table using id as the primary key.
           //$result = JFactory::getDbo()->updateObject('#__advportfolio_projects', $object, 'id');

        }

        die;
    }

    public function replacecontinput(){

        $db = JFactory::getDbo();

        $query = $db->getQuery(true);
        $query->select('*')
            ->from('#__content')
            ->where("introtext LIKE  ".$db->quote('%id="tab1" type="radio" name="tabs1" checked%'));
        $result = $db->setQuery($query)->loadObjectList();
        $delimiter='id="tab1" type="radio" name="tabs1" checked';
        $i = 0;
        foreach($result AS $res){
           $sentArray = explode( $delimiter, $res->introtext);
            $bigPiecesArray[0] = '';
            $object = new stdClass();
            for ($k = 0; $k < count($sentArray); $k++) {

                $rand=rand(1,10);
                $sentArray[$k]=str_replace('id="tab1" type="radio" name="tabs1" checked /><label for="tab1">', 'id="tab'.$rand.'" type="radio" name="tabs'.$rand.'" checked /><label for="tab'.$rand.'">', $sentArray[$k]);
                $object->introtext =str_replace('<section id="content1">','<section id="content'.$rand.'">',  $sentArray[$k]);
                $bigPiecesArray[$i] .= $object->introtext.$delimiter;

            }

            $object->id = $res->id;
            $object->introtext = $bigPiecesArray[0];


// Update their details in the users table using id as the primary key.
           $result = JFactory::getDbo()->updateObject('#__content', $object, 'id');
            echo $result;
        }
        die;
    }



    public function replaceinput2(){

        $db = JFactory::getDbo();

        $query = $db->getQuery(true);
        $query->select('*')
            ->from('#__content')
            ->where("introtext LIKE  ".$db->quote('%" type="radio" name="%'));

        $result = $db->setQuery($query)->loadObjectList();

        $delimiter='<input id="tab';
        $i = 0;
        foreach($result AS $res){
            $sentArray = explode( $delimiter, $res->introtext);
            $bigPiecesArray[0] = '';
            $object = new stdClass();
            for ($k = 0; $k < count($sentArray); $k++) {

                $rand=rand(2,15);

                $object->introtext =str_replace('id="tab1" type="radio" name="tabs1" checked /><label for="tab1">', 'id="tab'.$rand.'" type="radio" name="tabs'.$rand.'" checked /><label for="tab'.$rand.'">', $sentArray[$k]);
                $object->introtext =str_replace('1"', ''.$rand.'"', $object->introtext);
                $object->introtext =str_replace('name="tabs1" checked', 'name="tabs'.$rand.'" checked', $object->introtext);
                $object->introtext =str_replace('<label for="tab1">', '<label for="tab'.$rand.'">', $object->introtext);
                $object->introtext =str_replace('<section id="content1">','<section id="content'.$rand.'">',  $object->introtext);

                $bigPiecesArray[$i] .= $object->introtext.$delimiter;

            }


// Must be a valid primary key value.
            $object->id = $res->id;
            $object->introtext = $bigPiecesArray[0];
            $db = JFactory::getDbo();

            $approveQuery = "UPDATE `#__content` SET `introtext` = ".$db->quote($object->introtext)." WHERE `id` = " . (int)$res->id;
           echo  $db->setQuery($approveQuery)->query();

// Update their details in the users table using id as the primary key.
            //$result = JFactory::getDbo()->updateObject('#__advportfolio_projects', $object, 'id');

        }

        die;
    }


}
