<?php
/*
 * Copyright (c) 2014-2015 Aimy Extensions, Lingua-Systems Software GmbH
 *
 * http://www.aimy-extensions.com/
 *
 * License: GNU GPLv2, see LICENSE.txt within distribution and/or
 *          http://www.aimy-extensions.com/software-license.html
 */
 defined( '_JEXEC' ) or die(); $i = 0; ?>

<div itemscope="itemscope" itemtype="http://schema.org/Article">



<?php if ( ! empty( $this->heading ) ) : ?>

<h1 itemprop="name"><?php echo $this->heading; ?></h1>

<?php endif; ?>


<div class="aimysitemap<?php
 echo is_null( $this->pc_sfx ) ? '' : htmlspecialchars( $this->pc_sfx ); ?>" itemprop="articleBody">
<?php if ( $this->variant == 'index' ) : ?>

<div id="aimysitemap-index">

    <?php foreach ( array_keys( $this->data ) as $i => $key ) : ?>

    <?php if ( $i % 3 == 0 ) echo '<div class="row-fluid">'; ?>

    <div class="span4">

    <h2><?php echo $key; ?></h2>

    <ul>
        <?php foreach ( $this->data[ $key ] as $item ) : ?>
        <li><a href="<?php
 echo $item->href; ?>" title="<?php
 echo $item->href; ?>"><?php
 echo $item->title; ?></a></li>
        <?php endforeach; ?>
    </ul>

    </div><!-- /.span4 -->

    <?php if ( $i % 3 == 2 ) echo '</div><!-- /.row-fluid -->'; ?>

    <?php endforeach; ?>

    <?php if ( $i == 0 or $i % 3 != 2 ) echo '</div><!-- /.row-fluid -->'; ?>

</div><!-- /#aimysitemap-index -->

<?php  else : ?>

<div id="aimysitemap-list">

    <ul>
    <?php foreach ( array_values( $this->data ) as $item ) : ?>
        <li><a href="<?php
 echo $item->href; ?>" title="<?php
 echo $item->href; ?>"><?php
 echo $item->title; ?></a>
    <?php endforeach; ?>
    </ul>

</div><!-- /#aimysitemap-list -->

<?php endif; ?>

</div>
</div>

<?php
 
