/**
 * Collapsible, jQuery Plugin
 *
 * Collapsible management.
 * https://github.com/carhartl/jquery-cookie
 *
 * Copyright (c) 2010 John Snyder (snyderplace.com)
 * @license http://www.snyderplace.com/collapsible/license.txt New BSD
 * @version 1.2.1
 */
(function($) {
    $.fn.collapsible = function (cmd, arg) {

        //firewalling
        if (!this || this.length < 1) {
            return this;
        }

        //address command requests
        if (typeof cmd == 'string') {
            return $.fn.collapsible.dispatcher[cmd](this, arg);
        }

        //return the command dispatcher
        return $.fn.collapsible.dispatcher['_create'](this, cmd);
    };

    //create the command dispatcher
    $.fn.collapsible.dispatcher = {

        //initialized with options
        _create : function(obj, arg) {
            createCollapsible(obj, arg);
        },

        //toggle the element's display
        toggle: function(obj) {
            toggle(obj, loadOpts(obj));
            return obj;
        },

        //show the element
        open: function(obj) {
            open(obj, loadOpts(obj));
            return obj;
        },

        //hide the element
        close: function(obj) {
            close(obj, loadOpts(obj));
            return obj;
        },

        //check if the element is closed
        collapsed: function(obj) {
            return collapsed(obj, loadOpts(obj));
        },

        //open all closed containers
        openAll: function(obj) {
            return openAll(obj, loadOpts(obj));
        },

        //close all opened containers
        closeAll: function(obj) {
            return closeAll(obj, loadOpts(obj));
        }
    };

    //create the initial collapsible
    function createCollapsible(obj, options)
    {

        //build main options before element iteration
        var opts = $.extend({}, $.fn.collapsible.defaults, options);

        //store any opened default values to set cookie later
        var opened = [];

        //iterate each matched object, bind, and open/close
        obj.each(function() {

            var $this = $(this);
            saveOpts($this, opts);

            //bind it to the event
            if (opts.bind == 'mouseenter') {

                $this.bind('mouseenter', function(e) {
                    e.preventDefault();
                    toggle($this, opts);
                });
            }

            //bind it to the event
            if (opts.bind == 'mouseover') {

                $this.bind('mouseover',function(e) {
                    e.preventDefault();
                    toggle($this, opts);
                });
            }

            //bind it to the event
            if (opts.bind == 'click') {

                $this.bind('click', function(e) {
                    e.preventDefault();
                    toggle($this, opts);
                });

            }

            //bind it to the event
            if (opts.bind == 'dblclick') {

                $this.bind('dblclick', function(e) {

                    e.preventDefault();
                    toggle($this, opts);
                });

            }

            //initialize the collapsibles
            //get the id for this element
            var id = $this.attr('id');

          //is this collapsible in the default open array?
            var dOpenIndex = inDefaultOpen(id, opts);

            //close it if not defaulted to open
            if (dOpenIndex === false) {

                $this.addClass(opts.cssClose);
                opts.loadClose($this, opts);

            } else { //its a default open, open it

                $this.addClass(opts.cssOpen);
                opts.loadOpen($this, opts);
                opened.push(id);
            }
        });



        return obj;
    }

    //load opts from object
    function loadOpts($this) {
        return $this.data('collapsible-opts');
    }

    //save opts into object
    function saveOpts($this, opts) {
        return $this.data('collapsible-opts', opts);
    }

    //returns true if object is opened
    function collapsed($this, opts) {
        return $this.hasClass(opts.cssClose);
    }

    //hides a collapsible
    function close(el, opts) {

        //give the proper class to the linked element
        el.addClass(opts.cssClose).removeClass(opts.cssOpen);
        el.parent().removeClass(opts.cssPreviousOpen).addClass(opts.cssPreviousClose);
        //close the element
        opts.animateClose(el, opts);


    }

    //opens a collapsible
    function open($this, opts, openAll) {
    	if(opts.accordion && !openAll){
    		var id = '.';
    		if(opts.id){
    			id = '#'+opts.id+' .';
    		}
    		closeAll($(id+opts.cssOpen), opts);
    	}

        //give the proper class to the linked element
        $this.removeClass(opts.cssClose).addClass(opts.cssOpen);
        $this.parent().removeClass(opts.cssPreviousClose).addClass(opts.cssPreviousOpen);
        //open the element
        opts.animateOpen($this, opts);


    }

    function scrollToSlider($this, opts){
    	if(opts.autoscroll){
    		if(opts.updateHash){
    			if(history.pushState) {
        		    history.pushState(null, null, $this.attr('id'));
        		}
        		else {
        		    location.hash = $this.attr('id');
        		}
    		}
    		
    		setTimeout(function (){
    			$('html,body').animate({
        			scrollTop: $this.offset().top-opts.offset
    	        },opts.speed);
    		},opts.speed+50);
    		
		}
    }

	    
    // toggle a collapsible on an event
    function toggle($this, opts) {

        if (collapsed($this, opts)) {
        	scrollToSlider($this, opts);
            //open a closed element
            open($this, opts);

        } else {

            //close an open element
            close($this, opts);
        }

        return false;
    }

    //open all closed containers
    function openAll(el, opts) {

        // loop through all container elements
        $.each(el, function(elem, value) {

            if (collapsed($(value), opts)) {
                //open a closed element
                open($(value), opts, true);
            }
        });
    }

    //close all open containers
    function closeAll(el, opts) {
        $.each(el, function(elem, value) {
            if (!collapsed($(value), opts)) {
                //close an opened element
                close($(value), opts);
            }
        });
    }


    //check if a collapsible is in the list of collapsibles to be opened by default
    function inDefaultOpen(id, opts)
    {
        //get the array of open collapsibles
        var defaultOpen = getDefaultOpen(opts);

        //is it in the default open array
        var index = $.inArray(id, defaultOpen);
        if (index == -1) { //nope, quit here
            return false;
        }

        return index;
    }

    //get the default open collapsibles and return array
    function getDefaultOpen(opts)
    {
        //initialize an empty array
        var defaultOpen = [];

        //if there is a list, lets split it into an array
        if (opts.defaultOpen != '') {
            defaultOpen = opts.defaultOpen.split(',');
        }

        return defaultOpen;
    }

    // settings
    $.fn.collapsible.defaults = {
        cssClose: 'collapse-close', //class you want to assign to a closed collapsible header
        cssOpen: 'collapse-open', //class you want to assign an opened collapsible header
        cssPreviousClose: 'ifaq-container-close', //class you want to assign to a closed collapsible header
        cssPreviousOpen: 'ifaq-container-open', //class you want to assign an opened collapsible header
        accordion:	true,
        autoscroll:	false,
        offset:		0,
        defaultOpen: '', //comma separated list of header ids that you want opened by default
        speed: 'slow', //speed of the slide effect
        bind: 'click', //event to bind to, supports click, dblclick, mouseover and mouseenter
        id: '',
        animateOpen: function (elem, opts) { //replace the standard slideUp with custom function
            elem.next().stop(true, true).slideDown(opts.speed);
        },
        animateClose: function (elem, opts) { //replace the standard slideDown with custom function
            elem.next().stop(true, true).slideUp(opts.speed);
        },
        loadOpen: function (elem, opts) { //replace the default open state with custom function
            elem.next().show();
        },
        loadClose: function (elem, opts) { //replace the default close state with custom function
            elem.next().hide();
        }

    };
})(jQuery);