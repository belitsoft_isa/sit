/**
 * Requires bootstrap
 */

;(function ( $, window, document, undefined ) {
    var pluginName = 'cssCreator',
        defaults = {
            loadBorder: 		false,
            loadBorderRadius:	true,
            loadBackground:		false,
            loadFont:			false,
            loadFontColor:		false,

            editElement:		'#ifaq-collapsible',
            blockExample:		'<div id="ifaq-collapsible"><a href="#">FAQ Title</a><span></span></div>',
            defaultCss:			''
        };

    // The actual plugin constructor
    function cssCreator( element, options ) {
    	var self = this;
    	self.element = element;

        // jQuery has an extend method that merges the
        // contents of two or more objects, storing the
        // result in the first object. The first object
        // is generally empty because we don't want to alter
        // the default options for future instances of the plugin
    	self.options = $.extend( {}, defaults, options) ;

    	self._defaults = defaults;
    	self._name = pluginName;

    	self.init();
    	self.populateControllers();

    }

    cssCreator.prototype = {
		init: function () {
			var self = this;
	        // Place initialization logic here
	        // You already have access to the DOM element and
	        // the options via the instance, e.g. this.element
	        // and this.options

	    	$('.cssChanger input[type="range"]').on('change input',function(){
	    		var el = $(this);
	    		el.next('.add-on').text(el.val()+el.attr('data-unit'));
    		});


	    	$('#cssChanger-code').html(this.options.blockExample);
	    	$(self.options.editElement).attr('style', $(self.element).val());


	    	//disable chzn select list
	    	$('#font_family_chzn').css('display', 'none');
	    	$('#font-family').css('display', '');



	    	$('.cssChanger input,.cssChanger button,.cssChanger select').on('change click mouseup input',function(){
	    		// BORDER
	    		var bdSize		= parseInt($('#bd-size').val());
	    		if(self.options.loadBorder && bdSize > 0){
	    			var bdOpacity 	= $('#bd-opacity').val() ;
	    			var bdColor		= $('#bd-color').val() ;
	    			var bdRadius		= $('#bd-radius').val() ;
	    			setTimeout(function(){$(self.options.editElement).css('border-style',$('#bd-style .active').val());},100);
	    			$(self.options.editElement).css('border-width',bdSize);
	    			if(bdOpacity < 100){
	    				var hex = self.hex2rgb(bdColor,bdOpacity);
	    				bdColor = hex.rgba;
	    			}else{
	    				bdColor = '#'+bdColor;
	    			}
	    			if(self.options.loadBorderRadius && bdRadius){
	    				$(self.options.editElement).css('border-radius',bdRadius+'px');
	    			}
	    			$(self.options.editElement).css('border-color',bdColor);

		    	}
	    		if(self.options.loadBackground){
	    			var bgOpacity 	= $('#bg-opacity').val() ;
	    			var bgColor		= $('#bg-color').val() ;
	    			if(bgOpacity < 100){
	    				var hex = self.hex2rgb(bgColor,bgOpacity);
	    				bgColor = hex.rgba;
	    			}else{
	    				bgColor = '#'+bgColor;
	    			}
	    			$(self.options.editElement).css('background-color',bgColor);

		    	}

	    		if(self.options.loadFont){
	    			var fontOpacity 	= $('#font-opacity').val() ;
	    			var fontColor		= $('#font-color').val() ;


	    			setTimeout(function(){
	    				var fontStyle		= $('#font-style.active').val();
	    				if(fontStyle){$(self.options.editElement).css('font-style',fontStyle);}
	    			},100);
	    			setTimeout(function(){
	    				var fontWeight		= $('#font-weight.active').val();
	    				if(fontWeight){$(self.options.editElement).css('font-weight',fontWeight);}
	    			},100);
	    			setTimeout(function(){
	    				var fontAlign		= $('#font-align .active').val();
	    				if(fontAlign){$(self.options.editElement).css('text-align',fontAlign);}
	    			},100);

	    			setTimeout(function(){
	    				var fontFamily		= $('#font-family').val();
	    				$(self.options.editElement).css('font-family',fontFamily);
	    			},100);


	    			if(self.options.loadFontColor && fontColor){
	    				if(fontOpacity && fontOpacity < 100){
		    				var hex = self.hex2rgb(fontColor,fontOpacity);
		    				fontColor = hex.rgba;
		    			}else{
		    				fontColor = '#'+fontColor;
		    			}
		    			$(self.options.editElement).css('color',fontColor);
	    			}
	    			setTimeout(function(){$(self.options.editElement).css('text-transform',$('#text-transform .active').val());},100);
	    			//$(self.options.editElement).css('border-width',bdSize);

		    	}
    		});


		},
		populateControllers: function(){
			var self		= this;
			var bdColor		= $(self.options.editElement).css('border-top-color');
			var bdStyle		= $(self.options.editElement).css('border-top-style');
			var bdWidth		= $(self.options.editElement).css('border-top-width');
			var bdOpacity	= 100;
			var bgColor		= $(self.options.editElement).css('background-color');
			var shadow		= $(self.options.editElement).css('shadow');

			if(self.options.loadBorder && parseInt(bdWidth) > 0){
				if(bdColor.search("#") == -1){ // color = rgba
					bdColor 	= self.rgba2hex(bdColor);
					bdOpacity	= bdColor.opacity;
					bdColor		= bdColor.hex;
				}
				$('#bd-opacity').val(bdOpacity);
	    		$('#bd-color').val(bdColor);
	    		$('#bd-style').val(bdStyle);
	    		$('#bd-size').val(bdWidth);

	    		new jscolor.color(document.getElementById('bd-color'));
			}
			if(self.options.loadBackground){
				if(bgColor.search("#") == -1){ // color = rgba
					bgColor 	= self.rgba2hex(bgColor);
					bgOpacity	= bgColor.opacity;
					bgColor		= bgColor.hex;
				}
				$('#bg-opacity').val(bgOpacity);
	    		$('#bg-color').val(bgColor);

	    		new jscolor.color(document.getElementById('bg-color'));
			}

			if(self.options.loadFont){
				var fontColor		= $(self.options.editElement).css('color');
				if(self.options.loadFontColor && fontColor){
					if(fontColor.search("#") == -1){ // color = rgba
						fontColor 	= self.rgba2hex(fontColor);
						fontOpacity	= fontColor.opacity;
						fontColor	= fontColor.hex;
					}
					new jscolor.color(document.getElementById('font-color'));
					$('#font-opacity').val(fontOpacity);
					$('#font-color').val(fontColor);

				}
				$('#font-align').val($(self.options.editElement).css('text-align'));
				$('#font-family').val($(self.options.editElement).css('font-family'));

			}

			$('.cssChanger input').trigger("change");

		},
		hex2rgb: function(hex,opacity) {
		    var result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
		    if(!opacity){opacity=100;}
		    return result ? {
		        r: parseInt(result[1], 16),
		        g: parseInt(result[2], 16),
		        b: parseInt(result[3], 16),
		        rgb: parseInt(result[1], 16) + ", " + parseInt(result[2], 16) + ", " + parseInt(result[3], 16),
		        rgba: 'rgba('+parseInt(result[1], 16) + ", " + parseInt(result[2], 16) + ", " + parseInt(result[3], 16)+','+(opacity/100)+')',
		    } : null;
		},
		rgba2hex: function(rgba)
		{
			rgba = rgba.replace(/[^\d,]/g, '').split(',');
			function hex(x) {
				return ("0" + parseInt(x).toString(16)).slice(-2);
			}
			var opacity = '';
			if(rgba[3]){
				opacity = parseInt(rgba[3]);
			}
			return rgba ? {
				hex: hex(rgba[0]) + hex(rgba[1]) + hex(rgba[2]),
		        opacity: opacity
		    } : null;
		}
    };

    // A really lightweight plugin wrapper around the constructor,
    // preventing against multiple instantiations
    $.fn[pluginName] = function ( options ) {
        return this.each(function () {
            if (!$.data(this, 'plugin_' + pluginName)) {
                $.data(this, 'plugin_' + pluginName,
                new cssCreator( this, options ));
            }
        });
    }

})( jQuery, window, document );