<?php
/**
 * @package		iFAQ
 * @copyright	Copyright (C) 2006 - 2014 Ideal Custom software development. All rights reserved.
 * @license		GNU/GPL, see LICENSE.php
 */

defined('_JEXEC') or die('Restricted access');

jimport( 'joomla.application.component.view');
if( !class_exists('JViewLegacy') ) {
	require_once (JPATH_ROOT.'/administrator/components/com_ifaq/legacy/view.php');
}
class IfaqView extends JViewLegacy
{
	function __construct($config = array())
	{
		parent::__construct($config);

		//Add the helper path to the JHTML library
		JHTML::addIncludePath(JPATH_COMPONENT.'/helpers');
	}
}
