<?php
/**
 * @package		iFAQ
 * @copyright	Copyright (C) 2006 - 2015 Ideal Custom software development. All rights reserved.
 * @license		GNU/GPL, see LICENSE.php
 */


// no direct access
defined('_JEXEC') or die('Restricted access');


class iFAQ
{

	protected static $loaded = array();
	protected static $opened = array();
	protected static $count = 0;
	protected static $params = array();

	function __construct($params) {
		self::$params = $params;
	}
	/**
	 * Creates a panes and loads the javascript behavior for it.
	 *
	 * @param	string	The pane identifier.
	 * @param	array	An array of options.
	 * @return	string
	 * @since	1.6
	 */
	public static function start($group = 'ifaq', $params = array())
	{
		self::_loadBehavior($group,$params);

		array_push(self::$opened,false);

		return '
<div class="ifaq-container" id="'.$params['id'].'">'."\n";
	}

	/**
	 * Close the current pane.
	 *
	 * @return	string
	 * @since	1.6
	 */
	public static function end()
	{
		return "\n</div>\n";
	}

	/**
	 * Begins the display of a new panel.
	 *
	 * @param	string	Text to display.
	 * @param	string	Identifier of the panel.
	 * @return	string
	 * @since	1.6
	 */
	public static function panel($text, $id, $content, $link='javascript:void(0);')
	{
		++self::$count;
		$class	= '';
		if(JFactory::getApplication()->input->getBool('ifaqPlugin', 0)){
			$class	= 'ifaq-plugin-panel';
		}
		if(self::$count % 2 == 0){
			$class .= ' ifaq-even-line ';
		}else{
			$class .= ' ifaq-odd-line ';
		}
		$class .= 'collapse-close ifaq-number-'.self::$count;
		return "\n\t\t".'<div><div class="ifaq-collapsible '.$class.'" id="'.$id.'">'."\n\t"
								.'<a href="'.$link.'"><h3>'.$text.'</h3></a>'."\n\t"
							."\n\t\t<span></span></div>"
								."\n\t\t".'<div class="ifaq-panel">
	<div class="ifaq-content">'."\n".$content."\n\t\t".'</div>'."\n\t".'</div>'.'</div>'
								;
	}

	/**
	 * Load the JavaScript behavior.
	 *
	 * @param	string	The pane identifier.
	 * @param	array	Array of options.
	 * @return	void
	 * @since	1.6
	 */
	protected static function _loadBehavior($group, $params = array()){

		if(JRequest::getVar('print') == 1){
			return '';
		}

			$js = "";
			if(!isset(self::$loaded[$params['id']])){
				self::$loaded[$params['id']] = true;
				
				JHtml::_('jquery.framework');
				$comParams = JComponentHelper::getParams('com_ifaq');
				if($comParams->get('load_bootstrap')){
					// Bootstrap has to load last in order to avoid incompatibility with jQuery UI
					JHtml::_('bootstrap.framework');
					JHtml::_('bootstrap.loadCss');
				}
				$document	= JFactory::getDocument();
				$document->addScript(JURI::base(true).'/components/com_ifaq/assets/js/jquery.collapsible.js');
				$openSliders= (JRequest::getInt('open',$params['autoopen']));
				if($openSliders){
					$js .= "
	$('.ifaq-number-$openSliders').collapsible('toggle');
";
				}
				
				if($params['autoscroll']){
					$js .= "
	$(location.hash).collapsible('toggle');
";
				}
				
				$js	= "
jQuery(document).ready(function($) {
	//collapsible management
	$('#{$params['id']} .ifaq-collapsible').collapsible({
		defaultOpen: '',
		accordion	: ".($params['autohide'] ? "true" :"false").",
		offset		: ".($params['offset'] ? $params['offset'] : 0).",
		speed		: ".($params['duration']).",
		updateHash	: ".(!empty($params['update-hash']) ? 'true' : 'false').",
		autoscroll	: ".($params['autoscroll'] ? "true" :"false").",
		controls	: ".( ($params['slider_controls'] AND !JFactory::getApplication()->input->getBool('ifaqModule',0)) ? 'true' : 'false').",
		id 			: '".$params['id']."'
	});
	//assign open/close all to functions
	function openAll() {
	$('#{$params['id']} .ifaq-collapsible').collapsible('openAll');
	}
	function closeAll() {
		$('#{$params['id']} .ifaq-collapsible').collapsible('closeAll');
	}
	//listen for close/open all
	$('.ifaqControl-{$params['id']} .collapse-all').click(function(event) {
		event.preventDefault();
		closeAll();
	});
	$('.ifaqControl-{$params['id']} .expand-all').click(function(event) {
		event.preventDefault();
		openAll();
	});
	{$js}
});
";
				//$js = "/* <![CDATA[ */ \n".$js ." \n /* ]]> */";
				$document->addScriptDeclaration( $js );
			}
	}

	function getControls(){
		return '<div class="ifaqControls no-print ifaqControl-'.self::$params['id'].'">
				<a title="'.JText::_('COM_IFAQ_EXPAND_ALL_DESC').'"
						class="btn btn-default expand-all"
						href="'.JURI::current().'#expand"
						id="expand-all"><span class="glyphicon glyphicon-chevron-down icon-chevron-down"> </span> '
							.JText::_('COM_IFAQ_EXPAND_ALL_LABEL').'</a>
				<a title="'.JText::_('COM_IFAQ_COLLAPSE_ALL_DESC').'"
						href="'.JURI::current().'#collapse"
						class="btn btn-default collapse-all"
						id="collapse-all"><span class="glyphicon glyphicon-chevron-up icon-chevron-up"> </span> '
							.JText::_('COM_IFAQ_COLLAPSE_ALL_LABEL').'</a>
			</div><br />';
	}

	function getCurrentURL(){
		$cururl = JRequest::getURI();
		if(($pos = strpos($cururl, "index.php"))!== false){
			$cururl = substr($cururl,$pos);
		}
		$cururl =  JRoute::_($cururl, true, 0);
		return $cururl;
	}
}
?>
