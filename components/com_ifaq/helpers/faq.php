<?php
/**
 * @package		iFAQ
 * @copyright	Copyright (C) 2006 - 2014 Ideal Custom software development. All rights reserved.
 * @license		GNU/GPL, see LICENSE.php
 */

require_once (JPATH_ROOT.'/components/com_ifaq/helpers/slider.php');

// no direct access
defined('_JEXEC') or die('Restricted access');

/**
 * iFAQ Component HTML Helper
 */
class ifaqHTMLHelper extends JObject
{
	var $id		= 0;

	var $pane	= null;

	var $access	= null;

	/**
	 * Category Object
	 * @var     Object
	 */
	var $category = null;

	/**
	 * Categories Array of Objects
	 * @var     Object
	 */
	var $categories = null;

	/**
	 * Parent Object
	 * @var     Object
	 */
	var $parent = null;

	/**
	 * FAQ type
	 * @var		String  Posssible values: category, categories, search
	 */
	var $view	= null;

	/**
	 * Parameters Object
	 * @var     Object
	 */
	var $params = null;

	var $paneParams = null;

	public $jVersion	= null;

	/**
	 * Creates a new instance of iFAQ
	 */
	function __construct(&$params, $view)
	{

		// Initialize some variables
		$this->params	= &$params;
		$this->user		= JFactory::getUser();
		$this->app		= JFactory::getApplication();
		$this->input 	= $this->app->input;
		$this->view		= $view;
		$this->jVersion	= new JVersion();

		//$this->params->set('template','dark_blue_gradient');

		if($this->input->getBool('ifaqPlugin', false)){
			if($this->input->getCmd('ifaq_plg_template')){
				$this->params->set('template',$this->input->getCmd('ifaq_plg_template','basic'));
			}
		}

		$this->getPane();
		$this->sliderObj= new iFAQ($this->paneParams);
		$this->addStyleSheets();


	}//function

	function addStyleSheets(){
		//Include the CSS
 		if(JFactory::getLanguage()->isRTL()){

		}
		JFactory::getDocument()->addStyleSheet(JURI::root().'components/com_ifaq/templates/'.$this->params->get('template','basic').'/ifaq.css');

	}

	/**
	 * Creates a new instance of the pane if it does not exists
	 */
	function getpane()
	{
		if(!$this->paneParams){
			$paneParams	= array();
			$paneParams['startOffset']			= $this->params->get('startOffset',null);
			$paneParams['startTransition']		= $this->params->get('startTransition',null);
			$paneParams['useCookie']			= $this->params->get('useCookie',true);
			$paneParams['opacityTransition']	= $this->params->get('opacityTransition',null);

			$paneParams['allowAllClose']		= true;

			$paneParams['autoscroll']			= $this->params->get('effect-autoscroll',true);
			$paneParams['offset']				= $this->params->get('offset',0);
			$paneParams['autohide']				= $this->params->get('effect-autohide',false);
			$paneParams['slider_controls']		= $this->params->get('collapse-expand-all-controls',true);
			$paneParams['duration']				= $this->params->get('effect-duration',300);
			$paneParams['autoopen']				= $this->params->get('autoopen',0);
			$paneParams['id']					= 'ifaq-'.mt_rand(0,999999).md5(microtime());

			$this->paneParams	= &$paneParams;
		}
		/*
		 * @todo: remove var pane
		 */
		$this->pane='';
		return $this->pane;
	}//function

	function getTitle()
	{

		$html	= self::loadModulePosition('ifaq-before-title');

		if (JRequest::getVar('ifaq_displayTitle',1) == 1
			AND ( (isset($this->parent->title) AND $this->parent->title != 'ROOT') OR isset($this->category->title))
			AND (
				($this->params->get('show_base_title',1) AND  $this->view == 'categories')
				OR ($this->params->get('show_category_title',1) AND  $this->view == 'category')
			) // @todo implement this in the view xml file
		){
			
			
			$html	.= '<h2 class="ifaq-faqtitle contentheading'.$this->params->get('pageclass_sfx').'">';
				if($this->view == 'categories'){
					$html	.= $this->parent->title;
				}else{
					$html	.= $this->category->title;
				}
			$html	.= '</h2>';
		}
		$html	.= self::loadModulePosition('ifaq-after-title');
		return $html;
	}//function

	function getPageHading()
	{
		$html	= '';
		if ($this->params->get('show_page_heading', ($this->view == 'search'))
			AND !JRequest::getVar('ifaqModule') // Do not load in modules
			AND	!JRequest::getVar('ifaqPlugin') // Do not load in plugins
		){
			// added the title class in order to try to be compatible with RocketTheme templates
			$html	.= '<h1 class="title componentheading'.$this->params->get('pageclass_sfx').'">';
			// Add Span in order to add compatibility with some JoomlArt Templates
			$html	.= '<span>';
				$html	.= $this->params->get('page_heading',$this->params->get('page_title'));
			$html	.= '</span>';
			$html	.= '</h1>';
		}
		return $html;
	}//function

	function getDescription($data=null)
	{
		$html	= '';
		$info	= $data;

		if(!JRequest::getVar('ifaq_displayDesc',1)){
			// Only allow to use JRequest::getVar('description') once, this way we wont have problems with the Categories view
			JRequest::setVar('ifaq_description',null);
			return '';
		}
		if(!$data AND $this->view == 'categories'){
			$info	= new stdClass();
			$info->description	= $this->parent->description;
		}elseif(!$data AND $this->view == 'category'){
			$info	= &$this->category;
			/*$info	= new stdClass();
			$info	= $this->category->description;
			echo ifaqHTMLHelper::print_r($this->category); exit; */
		}elseif(!$data){
			$info	= new stdClass();
			$info->description	= JRequest::getVar('ifaq_description', null);
		}

		if ($this->params->get('show_base_description')
				|| $this->params->get('show_description')
				|| ( JRequest::getVar('ifaq_description') )  ){
			
			$html	.= '<div  class="category-desc" id="ifaq-cat-desc'.(isset($info->id) ? '-'.$info->id : '').'">';

				if(	$this->params->get('show_base_description')
					AND $this->params->get('categories_description')
					AND !defined('IFAQ_MAIN_CATEGORY')
					AND $this->view == 'categories'
					)
				{
					define('IFAQ_MAIN_CATEGORY',1);
					$html	.= JHtml::_('content.prepare',$this->params->get('categories_description'));
				}elseif (($this->params->get('show_description') && $info->description)){
					$html	.= JRequest::getVar('ifaq_description',$info->description, null, 'string', JREQUEST_ALLOWRAW);
					$html	.= '<br style="clear:both" />';
					// Only allow to use JRequest::getVar('description') once, this way we wont have problems with the Categories view
					JRequest::setVar('ifaq_description',null);
				}
			$html	.= '</div>';
		}
		return $html;
	}//function

	function getFaqPDF()
	{
		//disabled
		return '';

		$html	= '';
		if ($this->params->get('show_faq_pdf', 999) <= $this->user->get('aid', 0)){
			$html	.= '<div class="ifaq-faqpdf">';
				$html	.= JHTMLiFAQ::_('icon.faqpdf',  $this->view, $this->id, $this->params);
			$html	.= '</div>';
		}
		return $html;
	}//function

	function getFaqPrint()
	{
		$html	= '';
		if ($this->params->get('show_faq_print', 0) AND $this->view != 'search'){
			$routeMethod	= 'get'.ucfirst($this->view).'Route';
				$html	.= '
					<div id="pop-print" class="btn btn-default hidden-print">';
					$url	= IfaqHelperRoute::$routeMethod($this->id, JRequest::getVar('catid'));
					$html	.= JHTML::_('ifaq.print_popup',  null,  $this->params, '',$url);
				$html	.= '</div>';
		}
		return $html;
	}//function

	function getAnchors()
	{
		$html	= 	'';

		if ($this->params->get('show_anchor', 0) AND isset($this->categories)){
			$html	.=  '<ul class="ifaq-anchors no-print">';
			foreach($this->categories[$this->parent->id] as $id => $category){
				if ( (isset($category->numitems) AND ($category->numitems) > 0) OR $this->params->get('show_empty_categories_cat') ){
						$html	.=  '<li><a href="'.JRequest::getURI().'#ifaqCat-'.$category->id.'" >'.$category->title.'</a></li>';
					}
				}
				$html	.=  '</ul>';
		}
		return $html;
	}//function

	function render()
	{
		$html	= 	'
		<div class="item-page ifaq'.trim($this->params->get('pageclass_sfx',''))
			.(JRequest::getVar('tmpl') == 'component' ? ' ifaq-modal' : '')
			.' ifaq-tpl-'.$this->params->get('template','math_blue')
			.'">';
		$html	.=	$this->getPageHading();
		$html	.=	$this->getTitle();

		if( version_compare( $this->jVersion->getShortVersion(), '2.5.8', 'lt' )) {
			$access	= $this->user->authorisedLevels();
		}else{
			$access	= $this->user->getAuthorisedViewLevels();
		}

		// Check access level
		if(array_key_exists($this->params->get('access_level',0),$access)
			AND !$access[$this->params->get('access_level',0)]
		){
			$html	.= JText::_('COM_IFAQ_ACCESS_DENIED');
			return $html;
		}

		$html	.=	$this->getFaqPDF();
		$html	.=	$this->getFaqPrint();
		$html	.=	$this->getDescription();
		$html	.=	$this->getAnchors();

		if(isset($this->items) AND count($this->items) == 0){
			$html	.= '<div class="ifaq-no-result">'.JText::_('IFAQ_NO_RESULT_FOUND').'</div>';
		}

		if(!defined('IFAQ_CONTROLS_LOADED')
			AND $this->params->get('collapse-expand-all-controls',true)
			AND !JRequest::getVar('ifaqModule', false)
			AND !JRequest::getVar('print')
		){
			$html	.= '<br />';
			$html	.= $this->sliderObj->getControls();
			define('IFAQ_CONTROLS_LOADED',1);
		}elseif(!defined('IFAQ_CONTROLS_LOADED') AND $this->params->get('show_faq_print', 0)){
			$html	.= '<br />';
		}
		if(count($this->categories)>0){
			$html	.=	$this->getFaqCategories();
		}else{
			$questionCount	= JRequest::getInt('start',0)+1;
			if($this->category){
				$id	= $this->category->id;
			}else{
				$id	= 0;
			}
			$html	.=	$this->getFaqCategory($id,$questionCount);
		}

		$html		.=	$this->getLinks();
		$html		.=	$this->getFooter();

		$html		.= '</div>';

		if(JRequest::getVar('ifaqPlugin', 0)){
			JRequest::setVar('ifaqPlugin', 0);
		}

		return $html;
	}//function

	function getFaqCategories()
	{
		$html	= '';
		$j		= 0;
		$questionCount = JRequest::getInt('start',0)+1;


		foreach($this->categories[$this->parent->id] as $id => $category){
			if ( ($category->numitems) > 0 OR $this->params->get('show_empty_categories_cat') ){
				// compatibility with old links to moofaq
				$html	.= '<a  id="moofaqCat-'.$category->id.'"></a>';
				
				$html	.= '<h2  id="ifaqCat-'.$category->id.'"
								class="componentheading'.$this->params->get('pageclass_sfx').' ifaq-category-title">'
									.$category->title.'</h2>';
				$this->items	= $category->articles;

				$html	.=	$this->getDescription($category);
				if($category->numitems > 0){
					$html	.= $this->getFaqCategory($category->id,$questionCount,$j++);
				}
			}
		}
		return $html;
	}//function

	function getFaqCategory($id,&$questionCount)
	{

		$html	= 	'';
		$html	.= '<div class="blog'.$this->params->get('pageclass_sfx').'">';

			$html	.= $this->sliderObj->start( "ifaq-pane-".$id, $this->paneParams);
				$i = JRequest::getInt('start',0);
				//echo $this->print_r($this->items); exit;
				foreach ( $this->items as $article){

					$this->item =&$this->getItem($article,$i++);
					$faqtitle	= $this->item->title;
					if($this->params->get('add_line_numbers',JRequest::getInt('add_line_numbers',0))){
						$faqtitle = ($questionCount++). ' - '. $this->item->title;
					}


					$linkArticle	= JRoute::_(IfaqHelperRoute::getArticleRoute($this->item->slug, $this->item->catslug));
					$panelContent	= $this->loadItem();
					$html	.= $this->sliderObj->panel($faqtitle, 'article-id-'.$this->item->id, $panelContent, $linkArticle);


				}
			$html	.= $this->sliderObj->end();
		$html	.= '</div><br />';

		return $html;
	}//function

	function &getItem($item, $index = 0)
	{
		$mainframe = JFactory::getApplication();
		// Initialize some variables
		$user		= JFactory::getUser();
		$dispatcher	= JDispatcher::getInstance();
		$SiteName	= $mainframe->getCfg('sitename');


		// Get the page/component configuration and article parameters
		$item->params->merge($this->params);
		//echo ifaqHTMLHelper::print_r($item->params ); exit;
		if(isset($item->attribs)){
			$registry = new JRegistry;
			$registry->loadString($item->attribs);
			$aparams = $registry;
			// Merge article parameters into the page configuration
			$item->params->merge($aparams);
		}


		if($item->params->get('show_readmore') == 2){
			$item->text = $item->introtext.' '.$item->fulltext;
		}else{
			$item->text = $item->introtext;
		}

		$images  = json_decode($item->images);
		if (isset($images->image_intro) && !empty($images->image_intro)
			AND $item->params->get('show_image_effect',0)
		){
			$item->text = '<img '
							.($images->image_intro_caption
									? ' class="caption ifaq-img-effect" title="' .htmlspecialchars($images->image_intro_caption).'"'
									: ' class="ifaq-img-effect" ')
							.' src="'. htmlspecialchars($images->image_intro).'"'
							.' alt="'. htmlspecialchars($images->image_intro_alt).'" />'
							.$item->text;
		}


		if($item->params->get('enable_plugins')){
			// Process the content preparation plugins
			JPluginHelper::importPlugin('content');
			$results = $dispatcher->trigger('onContentPrepare', array ('com_content.article', &$item, &$item->params,0 ));
		}
		// Build the link and text of the readmore button
		if ($item->params->get('show_readmore') && $item->readmore){

			if ($item->params->get('access-view')){

				$item->readmore_link = JRoute::_(IfaqHelperRoute::getArticleRoute($item->slug, $item->catid));
			}else{
				$menu = JFactory::getApplication()->getMenu();
				$active = $menu->getActive();
				$itemId = $active->id;
				$link1 = JRoute::_('index.php?option=com_users&view=login&Itemid=' . $itemId);
				//$returnURL = JRoute::_(IfaqHelperRoute::getArticleRoute($this->item->slug));
				$item->readmore_link = new JURI($link1);
				$item->readmore_link->setVar('return', base64_encode(JURI::current()));
			}
		}

		$item->event = new stdClass();

		if($item->params->get('enable_plugins')){
			$item->event = new stdClass();
			$results = $dispatcher->trigger('onContentAfterTitle', array('com_content.article', &$item, &$item->params, 0));
			$item->event->afterDisplayTitle = trim(implode("\n", $results));

			$results = $dispatcher->trigger('onContentBeforeDisplay', array('com_content.article', &$item, &$item->params, 0));
			$item->event->beforeDisplayContent = trim(implode("\n", $results));

			$results = $dispatcher->trigger('onContentAfterDisplay', array('com_content.article', &$item, &$item->params, 0));
			$item->event->afterDisplayContent = trim(implode("\n", $results));
		}

		return $item;
	}

	function loadItem(){
		//$canEdit	= ($this->user->authorise('com_content', 'edit', 'content', 'all') || $this->user->authorise('com_content', 'edit', 'content', 'own'));
		$canEdit	= $this->item->params->get('access-edit');
		$html	= '';

		if ($this->item->state == 0){
			$html	.= '<div class="system-unpublished">';
		}

 
		if ( ($canEdit AND $this->params->get('frontend_canedit',1))
					|| $this->item->params->get('show_pdf_icon')
					|| $this->item->params->get('show_print_icon')
					|| $this->item->params->get('show_email_icon')
					|| $this->item->params->get('show_permalink_icon')
		){
			$html	.= '<div class="btn-group pull-right">';
			$html	.= '<a class="btn dropdown-toggle" data-toggle="dropdown" href="#"> <span class="icon-cog glyphicon glyphicon-cog"></span> <span class="caret"></span> </a>';
			// Note the actions class is deprecated. Use dropdown-menu instead.

			$html	.= '
					<ul class="dropdown-menu actions no-print">';
					if ($this->params->get('show_permalink_icon')){
						$html	.= '<li class="link-icon">'.JHTML::_('iFAQ.permalink',  $this->item, $this->params).'</li>';
					}
					if ($this->params->get('show_email_icon')){
						$html	.= '<li class="email-icon">'.JHTML::_('iFAQ.email',  $this->item, $this->params, $this->access).'</li>';
					}
					if ($this->params->get('show_pdf_icon')){
						$html	.= '<li class="pdf-icon">'.JHTML::_('iFAQ.pdf',  $this->item, $this->params, $this->access).'</li>';
					}
					if ($this->params->get('show_print_icon')){
						$html	.= '<li class="print-icon">'.JHTML::_('iFAQ.print_popup',  $this->item, $this->params, $this->access).'</li>';
					}

					if ($canEdit){
						$html	.= '<li class="edit-icon">'.JHTML::_('iFAQ.edit',  $this->item, $this->params, $this->access).'</li>';
					}
				$html	.= '</ul>
				</div>';
		}


		if (!$this->item->params->get('show_intro') AND $this->item->params->get('enable_plugins')){
			$html	.= $this->item->event->afterDisplayTitle;
		}
		if($this->item->params->get('enable_plugins')){
			$html	.= $this->item->event->beforeDisplayContent;
		}

		$html	.= '
				<div class="paneopen'.$this->item->params->get( 'pageclass_sfx' ).'">';

		$articleInfo = '';


		if (isset ($this->item->toc)){
			$html	.= $this->item->toc;
		}

		$html	.= $this->item->text;

		if ($this->item->params->get('show_readmore') && $this->item->readmore){

			$html	.= '<p class="readmore no-print">';
				$html	.= '<a  class="btn btn-default"  href="'.$this->item->readmore_link.'">  <span class="glyphicon glyphicon-chevron-right icon-chevron-right"> </span> ';
					if (!$this->item->params->get('access-view')){
						$html	.= JText::_('COM_IFAQ_REGISTER_TO_READ_MORE');
					}elseif ($readmore = $this->item->alternative_readmore){
						$html	.= $readmore;
						$html	.= JHTML::_('string.truncate', ($this->item->title), $this->item->params->get('readmore_limit'));
					}elseif ($this->item->params->get('show_readmore_title', 0) == 0){
						$html	.= JText::sprintf('COM_IFAQ_READ_MORE_TITLE');
					}else{
						$html	.= JText::_('COM_IFAQ_READ_MORE');
						$html	.= JHTML::_('string.truncate', ($this->item->title), $this->item->params->get('readmore_limit'));
					}
				$html	.= '</a>';
			$html	.= '</p>';

		}
		$html .='<div class="clearfix" ></div>';
		$html	.= '</div>';
		if ($this->item->state == 0){
			$html	.= '</div>';
		}
		if($this->item->params->get('enable_plugins')){
			$html	.= $this->item->event->afterDisplayContent;
		}

		return $html;
	}///function

	/**
	 * print_r()
	 * Does a var_export of the array and returns it between <pre> tags
	 *
	 * @param mixed $var any input you can think of
	 * @return string HTML
	 */
	function print_r($var)
	{
	    $input =var_export($var,true);
	    $input = preg_replace("! => \n\W+ array \(!Uims", " => Array ( ", $input);
	    $input = preg_replace("!array \(\W+\),!Uims", "Array ( ),", $input);
	    return("<pre>".str_replace('><?', '>', highlight_string('<'.'?'.$input, true))."</pre>");
	}

	public function getLinks() {
		if($this->params->get('show_links',JRequest::getVar('show_links'))){
			$html	= '<div class="ifaq-links">';
			$html	.= '	<ul>';
			    foreach(range('a', 'e') as $char) :// letters 'a' to 'e'
				    $link	= JRequest::getVar('link'.$char	,		$this->params->get('link'.$char));
				    $label	= JRequest::getVar('link'.$char.'_name',$this->params->get('link'.$char.'_name'));

				    if( ! $link) :
				        continue;
				    endif;

				    // Add 'http://' if not present
				    $link = (0 === strpos($link, 'http')) ? $link : 'http://'.$link;

				    // If no label is present, take the link
				    $label = ($label) ? $label : $link;
					$html	.= '<li class="ifaq-link-'.$char.'"  >';
						$html	.= '<a href="'.$link.'" '
							.($this->params->get('show_links') == '_blank' ? 'target="_blank"' : '').' >';
					    $html	.= $label;
					$html	.= '</a></li>';
			endforeach;
			$html	.= '</ul>';
			$html	.= '</div>';
			return $html;
		}
	}

	public function getFooter() {
		$html	= '';
		if (JPluginHelper::isEnabled('system', 'irecommendlink')){
			$html	.= '<div class="ifaq-irecommend-link">{irecommend}</div>';
		}
		if (isset($this->pagination)
			AND $this->view == 'category'
			AND ($this->params->def('show_pagination') == 1  || ($this->params->get('show_pagination') == 2))
			AND ($this->pagination->get('pages.total') > 1)
		){
			$html	.= '<div class="pagination">';
			if ($this->params->def('show_pagination_results', 1)){
				$html	.='<p class="counter">';
					$html	.= $this->pagination->getPagesCounter();
					$html	.= '</p>';

			}
			$html	.= $this->pagination->getPagesLinks();
			$html	.= '</div>';
		}
		if(JRequest::getVar('view')== 'search'){

			$url = htmlspecialchars($_SERVER['HTTP_REFERER']);
			if (  parse_url(JUri::root(), PHP_URL_HOST ) == parse_url($url, PHP_URL_HOST ) )
			{
				JFactory::getLanguage()->load('joomla', JPATH_ADMINISTRATOR);
				if( version_compare( $this->jVersion->getShortVersion(), '3.0', 'lt' )) {
					$arrow	= '&laquo; ';
				}else{
					$arrow	= '<i class="glyphicon glyphicon-chevron-left icon-chevron-left">  </i> ';
				}
				$html	.= '<a class="pagination-prev" href="'.$url.'" title="'.JText::_('JPREVIOUS').'">'
							.$arrow.JText::_('JTOOLBAR_BACK').'</a>';
			}

		}
		return $html;
	}

	public static function loadModulePosition($position, $style = 'none')
	{
		$html 		= '';
		$document	= JFactory::getDocument();
		$renderer	= $document->loadRenderer('module');
		$modules	= JModuleHelper::getModules($position);
		$params		= array('style' => $style);
		ob_start();

		foreach ($modules as $module)
		{
			echo $renderer->render($module, $params);
		}

		$html 	= ob_get_clean();
		return $html;
	}

}//class
