<?php
/**
 * @package		iFAQ
 * @copyright	Copyright (C) 2006 - 2014 Ideal Custom software development. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 */

// no direct access
defined('_JEXEC') or die;

jimport('joomla.application.component.helper');
jimport('joomla.application.categories');

/**
 * Content Component Route Helper
 *
 * @static
 * @package		iFAQ
 * @since 1.5
 */
abstract class IfaqHelperRoute
{
	protected static $lookup;

	/**
	 * @param	int	The route of the content item
	 */
	public static function getArticleRoute($id, $catid = 0)
	{
		$needles = array(
			'article'  => array((int) $id)
		);
		$app = JFactory::getApplication();
		$params = ($app->getParams('com_ifaq'));
		$option = $params->get('article_handler','com_ifaq');
		if($option == 'com_content'){
			require_once(JPATH_ROOT.'/components/com_content/helpers/route.php');
			$link = ContentHelperRoute::getArticleRoute($id, $catid);
		} else {
			//Create the link
			$link = 'index.php?option='.$option.'&amp;view=article&amp;id='. $id;
			if ((int)$catid > 1)
			{
				$categories = JCategories::getInstance('Content');
				$category = $categories->get((int)$catid);
				if($category)
				{
					$needles['category'] = array_reverse($category->getPath());
					$needles['categories'] = $needles['category'];
					$link .= '&amp;catid='.$catid;
				}
			}

			if ($item = self::_findItem($needles)) {
				$link .= '&amp;Itemid='.$item;
			}
			elseif ($item = self::_findItem()) {
				$link .= '&amp;Itemid='.$item;
			}
		}
		return $link;
	}
	public static function getCategoriesRoute($catid){
		if ($catid instanceof JCategoryNode)
		{
			$id = $catid->id;
			$category = $catid;
		}
		else
		{
			$id = (int) $catid;
			$category = JCategories::getInstance('Content')->get($id);
		}

		if($id < 1)
		{
			$link = '';
		}
		else
		{
			$needles = array(
				'category' => array($id)
			);

			if ($item = self::_findItem($needles))
			{
				$link = 'index.php?Itemid='.$item;
			}
			else
			{
				//Create the link
				$link = 'index.php?option=com_ifaq&amp;view=categories&amp;id='.$id;
				if($category)
				{
					$catids = array_reverse($category->getPath());
					$needles = array(
						'category' => $catids,
						'categories' => $catids
					);
					if ($item = self::_findItem($needles)) {
						$link .= '&amp;Itemid='.$item;
					}
					elseif ($item = self::_findItem()) {
						$link .= '&amp;Itemid='.$item;
					}
				}
			}
		}

		return $link;
	}

	public static function getCategoryRoute($catid)
	{
		if ($catid instanceof JCategoryNode)
		{
			$id = $catid->id;
			$category = $catid;
		}
		else
		{
			$id = (int) $catid;
			$category = JCategories::getInstance('Content')->get($id);
		}

		if($id < 1)
		{
			$link = '';
		}
		else
		{
			$needles = array(
				'category' => array($id)
			);

			if ($item = self::_findItem($needles))
			{
				$link = 'index.php?Itemid='.$item;
			}
			else
			{
				//Create the link
				$link = 'index.php?option=com_ifaq&amp;view=category&amp;id='.$id;
				if($category)
				{
					$catids = array_reverse($category->getPath());
					$needles = array(
						'category' => $catids,
						'categories' => $catids
					);
					if ($item = self::_findItem($needles)) {
						$link .= '&amp;Itemid='.$item;
					}
					elseif ($item = self::_findItem()) {
						$link .= '&amp;Itemid='.$item;
					}
				}
			}
		}

		return $link;
	}

	public static function getFormRoute($id)
	{
		//Create the link
		if ($id) {
			$link = 'index.php?option=com_ifaq&amp;task=article.edit&amp;a_id='. $id;
		} else {
			$link = 'index.php?option=com_ifaq&amp;task=article.edit&amp;a_id=0';
		}

		return $link;
	}

	protected static function _findItem($needles = null)
	{
		$app		= JFactory::getApplication();
		$menus		= $app->getMenu('site');

		// Prepare the reverse lookup array.
		if (self::$lookup === null)
		{
			self::$lookup = array();

			$component	= JComponentHelper::getComponent('com_ifaq');
			$items		= $menus->getItems('component_id', $component->id);
			$items	= (is_array($items) ? $items: array());
			foreach ($items as $item)
			{
				if (isset($item->query) && isset($item->query['view']))
				{
					$view = $item->query['view'];
					if (!isset(self::$lookup[$view])) {
						self::$lookup[$view] = array();
					}
					if (isset($item->query['id'])) {
						self::$lookup[$view][$item->query['id']] = $item->id;
					}
				}
			}
		}

		if ($needles)
		{
			foreach ($needles as $view => $ids)
			{
				if (isset(self::$lookup[$view]))
				{
					foreach($ids as $id)
					{
						if (isset(self::$lookup[$view][(int)$id])) {
							return self::$lookup[$view][(int)$id];
						}
					}
				}
			}
		}
		else
		{
			$active = $menus->getActive();
			if ($active && $active->component == 'com_ifaq') {
				return $active->id;
			}
		}

		return null;
	}
}
