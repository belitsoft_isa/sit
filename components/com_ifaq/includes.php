<?php
/**
 * @package		iFAQ
 * @copyright	Copyright (C) 2006 - 2014 Ideal Custom software development. All rights reserved.
 * @license		GNU/GPL, see LICENSE.php
 */

// no direct access
defined('_JEXEC') or die('Restricted access');

// Require the com_ifaq helper libraries
require_once(JPATH_ROOT.'/components/com_ifaq/controller.php');
require_once(JPATH_ROOT.'/components/com_ifaq/helpers/query.php');
require_once(JPATH_ROOT.'/components/com_ifaq/helpers/route.php');
require_once(JPATH_ROOT.'/components/com_ifaq/helpers/faq.php');
require_once(JPATH_ROOT.'/components/com_ifaq/helpers/slider.php');
require_once(JPATH_ROOT.'/components/com_ifaq/helpers/icon.php');
