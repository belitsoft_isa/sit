<?php
/**
 * @package		iFAQ
 * @copyright	Copyright (C) 2006 - 2014 Ideal Custom software development. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 */

// Check to ensure this file is included in Joomla!
defined('_JEXEC') or die;

jimport('joomla.application.component.view');

/**
 * HTML View class for the Content component
 *
 * @package		iFAQ
 * @since 1.5
 */
class IfaqViewSearch extends JViewLegacy
{
	protected $state;
	protected $items;

	function display($tpl = null)
	{
		$app	= JFactory::getApplication();
		$user	= JFactory::getUser();

		// Check for layout override only if this is not the active menu item
		$active	= $app->getMenu()->getActive();
		if (isset($active->query['layout'])) {
			// We need to set the layout from the query in case this is an alternative menu item (with an alternative layout)
			$this->setLayout($active->query['layout']);
		}
		
		
		$model = new IfaqModelSearch();
		// Get some data from the models
		$state		= $model->getState();
		if(isset($state->params) AND is_object($state->params)){
			$params	= &$state->params;
		}else{
			$params = JComponentHelper::getParams('com_ifaq'); 
		}
		if (isset($active->params) AND is_object($active->params)){
			$params->merge($active->params);
		}
		
		
		//setting variables
		if(!JRequest::getVar('q')){
			JRequest::setVar('q',$params->get('search',''));
		}
		if(!JRequest::getVar('exclude-categories')){
			JRequest::setVar('exclude-categories',$params->get('exclude-categories'));
		}
		if(!JRequest::getVar('search_type')){
			JRequest::setVar('search_type',		$params->get('search_type','any'));
		}
		if(!JRequest::getVar('searchphrase')){
			JRequest::setVar('searchphrase',		$params->get('searchphrase','all'));
		}
		
		$items		= $model->getItems();
		//echo '<pre>'; print_r($items); exit;
		// Check for errors.
		if (count($errors = $this->get('Errors'))) {
			JError::raiseError(500, implode("\n", $errors));
			return false;
		}
		// Compute the article slugs and prepare introtext (runs content plugins).
		for ($i = 0, $n = count($items); $i < $n; $i++)
		{
			$item = &$items[$i];
			$item->slug = $item->alias ? ($item->id . ':' . $item->alias) : $item->id;

			// No link for ROOT category
			if ($item->parent_alias == 'root') {
				$item->parent_slug = null;
			}

			$item->event = new stdClass();

			$dispatcher = JDispatcher::getInstance();

			$item->introtext = JHtml::_('content.prepare', $item->introtext);

			$results = $dispatcher->trigger('onContentAfterTitle', array('com_ifaq.article', &$item, &$item->params, 0));
			$item->event->afterDisplayTitle = trim(implode("\n", $results));

			$results = $dispatcher->trigger('onContentBeforeDisplay', array('com_ifaq.article', &$item, &$item->params, 0));
			$item->event->beforeDisplayContent = trim(implode("\n", $results));

			$results = $dispatcher->trigger('onContentAfterDisplay', array('com_ifaq.article', &$item, &$item->params, 0));
			$item->event->afterDisplayContent = trim(implode("\n", $results));
		}
		

		//Escape strings for HTML output
		$this->pageclass_sfx = htmlspecialchars($params->get('pageclass_sfx'));

		$this->assignRef('state', $state);
		$this->assignRef('items', $items);
		$this->assignRef('params', $params);
		$this->assignRef('user', $user);
		
		$this->_prepareDocument();
		JRequest::setVar('ifaq_description',$this->params->get('description', null));
		
		$ifaq	= new ifaqHTMLHelper($this->params, 'search');
		$ifaq->set('id',0);
		$ifaq->set('items',		$items);
	
		if(JRequest::getVar('ifaqPlugin', false)){
			return $ifaq->render();
		}else{
			echo $ifaq->render();
		}
		
		//parent::display($tpl);
	}

	/**
	 * Prepares the document
	 */
	function _prepareDocument()
	{
		$app		= JFactory::getApplication();
		$doc		= JFactory::getDocument();
		$menus		= $app->getMenu();
		$pathway	= $app->getPathway();
		$title		= null;

		// Because the application sets a default page title,
		// we need to get it from the menu item itself
		$menu = $menus->getActive();

		if ($menu) {
			if(!$this->params->get('page_title')){
				$this->params->set('page_title', $menu->title);
			}
			$this->params->set('page_heading', $this->params->get('page_title', $menu->title));
		}
		else {
			$this->params->def('page_heading', JText::_('JGLOBAL_ARTICLES'));
		}

		$id = (int) @$menu->query['id'];

		
		$title = $this->params->get('page_title', '');

		if (empty($title)) {
			$title = $app->getCfg('sitename');
		}
		elseif ($app->getCfg('sitename_pagetitles', 0)) {
			$title = JText::sprintf('JPAGETITLE', $app->getCfg('sitename'), $title);
		}
		$doc	= JFactory::getDocument();
		$doc->setTitle($title);

		if ($this->params->get('menu-meta_keywords')){
			$doc->setMetadata('keywords', $this->params->get('menu-meta_keywords'));
		}

		if($this->params->get('menu-meta_description')){
			$this->document->setDescription($this->params->get('menu-meta_description'));
		}
		
		if ($this->params->get('robots')){
			$this->document->setMetadata('robots', $this->params->get('robots'));
		}

		// Add feed links
		if ($this->params->get('show_feed_link', 1)) {
			$link = '&format=feed&limitstart=';
			$attribs = array('type' => 'application/rss+xml', 'title' => 'RSS 2.0');
			$doc->addHeadLink(JRoute::_($link . '&type=rss'), 'alternate', 'rel', $attribs);
			$attribs = array('type' => 'application/atom+xml', 'title' => 'Atom 1.0');
			$doc->addHeadLink(JRoute::_($link . '&type=atom'), 'alternate', 'rel', $attribs);
		}
	}
}