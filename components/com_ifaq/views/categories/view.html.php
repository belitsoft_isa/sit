<?php
/**
 * @copyright	Copyright (C) 2006 - 2014 Ideal Custom software development. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access
defined('_JEXEC') or die;

jimport('joomla.application.component.view');

/**
 * Content categories view.
 *
 * @package		iFAQ
 * @since 1.5
 */
class IfaqViewCategories extends JViewLegacy
{
	protected $state = null;
	protected $item = null;
	protected $items = null;

	/**
	 * Display the view
	 *
	 * @return	mixed	False on error, null otherwise.
	 */
	function display($tpl = null)
	{
		$model = new IfaqModelCategories();
		// Initialise variables
		$state		= $model->getState();
		//Categories
		$categories	= $model->getCategories();
		// Articles
		$articles	= $model->getArticles();
		$parent		= $model->getParent();
//echo '<pre>'; print_r($categories); exit;
		// Check for errors.
		if (count($errors = $model->get('Errors'))) {
			JError::raiseWarning(500, implode("\n", $errors));
			return false;
		}
//		echo  ifaqHTMLHelper::print_r($categories); exit;
		if ($categories === false)
		{
			JError::raiseError(404, JText::_('COM_IFAQ_ERROR_CATEGORY_NOT_FOUND'));
			return false;
		}

		if ($parent == false)
		{
			JError::raiseError(404, JText::_('COM_IFAQ_ERROR_PARENT_CATEGORY_NOT_FOUND'));
			return false;
		}

		$params = &$state->params;

		$categories = array($parent->id => $categories);

		//Escape strings for HTML output
		$this->pageclass_sfx = htmlspecialchars($params->get('pageclass_sfx'));

		$this->assign('maxLevelcat',	$params->get('maxLevelcat', -1));
		$this->assignRef('params',		$params);
		$this->assignRef('parent',		$parent);
		$this->assignRef('items',		$categories);

		$ifaq	= new ifaqHTMLHelper($params, 'categories');
		$ifaq->set('id',$this->parent->id);
		$ifaq->set('categories',	$categories);
		$ifaq->set('parent',	$parent);
		$ifaq->set('items',	$articles);
		//echo  ifaqHTMLHelper::print_r($params); exit;

		$this->_prepareDocument();

		if(JRequest::getVar('ifaqPlugin', false)){
			return $ifaq->render();
		}else{
			echo $ifaq->render();
		}

		//parent::display($tpl);
	}

	/**
	 * Prepares the document
	 */
	protected function _prepareDocument()
	{
		$app	= JFactory::getApplication();
		$doc	= JFactory::getDocument();
		$menus	= $app->getMenu();
		$title	= null;

		// Because the application sets a default page title,
		// we need to get it from the menu item itself
		$menu = $menus->getActive();

		if ($menu)
		{
			$title = ($this->params->get('page_title',	$menu->title));
		} else {
			$title = ($this->params->get('page_heading',$this->params->get('page_title', '')));
		}
		if (empty($title)) {
			$title = $app->getCfg('sitename');
		}
		elseif ($app->getCfg('sitename_pagetitles', 0) == 1) {
			$title = JText::sprintf('JPAGETITLE', $app->getCfg('sitename'), $title);
		}
		elseif ($app->getCfg('sitename_pagetitles', 0) == 2) {
			$title = JText::sprintf('JPAGETITLE', $title, $app->getCfg('sitename'));
		}
		$doc->setTitle($title);

		if ($this->params->get('menu-meta_keywords')){
			$doc->setMetadata('keywords', $this->params->get('menu-meta_keywords'));
		}

		if($this->params->get('menu-meta_description')){
			$this->document->setDescription($this->params->get('menu-meta_description'));
		}

		if ($this->params->get('robots')){
			$this->document->setMetadata('robots', $this->params->get('robots'));
		}
	}
}
