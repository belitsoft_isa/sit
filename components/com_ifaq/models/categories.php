<?php
/**
 * @copyright	Copyright (C) 2006 - 2014 Ideal Custom software development. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 */

// Check to ensure this file is included in Joomla!
defined('_JEXEC') or die;

jimport('joomla.application.component.model');

/**
 * This models supports retrieving lists of article categories.
 *
 * @package		iFAQ
 * @since		1.6
 */
class IfaqModelCategories extends JModelLegacy
{
	/**
	 * Model context string.
	 *
	 * @var		string
	 */
	public $_context = 'com_ifaq.categories';

	/**
	 * The category context (allows other extensions to derived from this model).
	 *
	 * @var		string
	 */
	protected $_extension = 'com_ifaq';

	private $_parent = null;

	private $_items = null;

	protected $_articles = array();

	/**
	 * Method to auto-populate the model state.
	 *
	 * Note. Calling getState in this method will result in recursion.
	 *
	 * @since	1.6
	 */
	protected function populateState()
	{
		$app = JFactory::getApplication();
		$this->setState('filter.extension', $this->_extension);

		// Get the parent id if defined.
		$parentId = JRequest::getInt('ifaq_id',JRequest::getInt('id',0));
		$this->setState('filter.parentId', $parentId);

	//	$params = $app->getParams();
		$params = JComponentHelper::getParams('com_ifaq');

		$menuParams = new JRegistry;

		if ($menu = $app->getMenu()->getActive() AND $app->input->getCmd('option') == 'com_ifaq') {
			$menuParams->loadString($menu->params);
			$params->merge($menuParams);
		}


		$this->setState('params', $params);


		$this->setState('filter.published',	1);
		$this->setState('filter.access',	true);
	}

	/**
	 * Method to get a store id based on model configuration state.
	 *
	 * This is necessary because the model is used by the component and
	 * different modules that might need different sets of data or different
	 * ordering requirements.
	 *
	 * @param	string		$id	A prefix for the store id.
	 *
	 * @return	string		A store id.
	 */
	protected function getStoreId($id = '')
	{
		// Compile the store id.
		$id	.= ':'.$this->getState('filter.extension');
		$id	.= ':'.$this->getState('filter.published');
		$id	.= ':'.$this->getState('filter.access');
		$id	.= ':'.$this->getState('filter.parentId');

		return parent::getStoreId($id);
	}

	/**
	 * Redefine the function an add some properties to make the styling more easy
	 *
	 * @param	bool	$recursive	True if you want to return children recursively.
	 *
	 * @return	mixed	An array of data items on success, false on failure.
	 * @since	1.6
	 */
	public function getCategories($recursive = false)
	{
		if (!count($this->_items)) {
			$app = JFactory::getApplication();
			$menu = $app->getMenu();
			$active = $menu->getActive();
			$params = new JRegistry();

			if ($active) {
				$params->loadString($active->params);
			}

			$options = array();
			$options['countItems'] = $params->get('show_cat_num_articles_cat', 1) || !$params->get('show_empty_categories_cat', 0);
			$categories = JCategories::getInstance('Content', $options);
			$this->_parent = $categories->get($this->getState('filter.parentId', 'root'));

			if (is_object($this->_parent)) {
				$this->_items = $this->_parent->getChildren($recursive);
			}
			else {
				$this->_items = false;
			}
		}


		foreach($this->_items as $id => $item){
//			echo ifaqHTMLHelper::print_r((int)$item->id); exit;
			$this->_items[$id]->articles	= $this->getItems((int)$item->id);
			//echo ifaqHTMLHelper::print_r($this->_items[$id]->articles); exit;
		}

		return $this->_items;
	}


	/**
	 * Get the articles in the category
	 *
	 * @return	mixed	An array of articles or false if an error occurs.
	 * @since	1.5
	 */
	function getItems($category)
	{
		$params = $this->getState()->get('params');
		$input	= JFactory::getApplication()->input;
		// set limit for query. If list, use parameter. If blog, add blog parameters for limit.
		//$limit = $this->getState('list.limit');
		$app = JFactory::getApplication();

		if (!in_array($category, $this->_articles) ) {
			$model = JModelLegacy::getInstance('Articles', 'IfaqModel', array('ignore_request' => true));
			$model->setState('params', JFactory::getApplication()->getParams());
			$model->setState('filter.category_id', $category);
			$model->setState('filter.published', $this->getState('filter.published'));
			$model->setState('filter.access', $this->getState('filter.access'));
			$model->setState('filter.language',$app->getLanguageFilter());

			$model->setState('filter.article_id', $input->getString('filter_article_id',$params->get('filter_article_id','')));
			$model->setState('filter.article_id.include', $input->getString('filter_article_id_include',$params->get('filter_article_id_include',1)));
				
			//$model->setState('filter.language', $this->getState('filter.language'));
			$model->setState('list.ordering', $this->_buildContentOrderBy());
			$model->setState('list.start', 0);
			$model->setState('list.limit', 9999);
			$model->setState('list.direction', $this->getState('list.direction'));
			$model->setState('list.filter', $this->getState('list.filter'));
			// filter.subcategories indicates whether to include articles from subcategories in the list or blog
			$model->setState('filter.subcategories', $this->getState('filter.subcategories'));
			$model->setState('filter.max_category_levels', $this->setState('filter.max_category_levels'));
			$model->setState('list.links', $this->getState('list.links'));


			$articles	= $model->getItems();

			//echo ifaqHTMLHelper::print_r($articles); exit;
			if (count($articles)) {
				$this->_articles[$category] = $articles;

				if ($this->_articles[$category] === false) {
					$this->setError($model->getError());
				}
				return $articles;
			}
		}
	}

	public function getArticles() {
	//	echo ifaqHTMLHelper::print_r($this->_articles); exit;
		return $this->_articles;
	}

	public function getParent()
	{
		if (!is_object($this->_parent)) {
			$this->getItems();
		}

		return $this->_parent;
	}
	/**
	 * Build the orderby for the query
	 * Copied from category view
	 *
	 * @return	string	$orderby portion of query
	 * @since	1.5
	 */
	protected function _buildContentOrderBy()
	{
		$app	= JFactory::getApplication('site');
		$params	= $this->state->params;
		$itemid	= JRequest::getInt('ifaq_id',JRequest::getInt('id',0)) . ':' . JRequest::getInt('Itemid', 0);
		$filter_order 		= $app->getUserStateFromRequest('com_ifaq.category.list.' . $itemid . '.filter_order', 'filter_order', '', 'string');
		$filter_order_Dir	= $app->getUserStateFromRequest('com_ifaq.category.list.' . $itemid . '.filter_order_Dir', 'filter_order_Dir', '', 'cmd');
		$orderby = ' ';

		if ($filter_order && $filter_order_Dir) {
			$orderby .= $filter_order . ' ' . $filter_order_Dir . ', ';
		}

		$articleOrderby		= $params->get('orderby_sec', 'order');
		$articleOrderDate	= $params->get('order_date');
		$categoryOrderby	= $params->def('orderby_pri', '');
		$secondary			= IfaqHelperQuery::orderbySecondary($articleOrderby, $articleOrderDate) . ', ';
		$primary			= IfaqHelperQuery::orderbyPrimary($categoryOrderby);

		$orderby .= $primary . ' ' . $secondary . ' a.created ';

		return $orderby;
	}
}