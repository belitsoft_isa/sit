<?php
/**
 * @package		iFAQ
 * @copyright	Copyright (C) 2006 - 2014 Ideal Custom software development. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 */

// no direct access
defined('_JEXEC') or die;

$app	= JFactory::getApplication();
$input	= $app->input;

$allowedFormat = array('html', 'csv', 'xml', 'csv', 'vcf');
// Make it ignore other formats
if($input->getCmd('format') AND !in_array($input->getCmd('format'), $allowedFormat)){
	$input->set('format','html');
}
$allowedTMPL = array('component','raw');
if($input->getCmd('tmpl') AND !in_array($input->getCmd('tmpl'),$allowedTMPL)){
	$input->set('tmpl', 'component');
}

$lang	= JFactory::getLanguage();
$lang->load('com_content');

if( !class_exists('JControllerLegacy') ) {
	require_once (JPATH_ROOT.'/administrator/components/com_ifaq/legacy/controller.php');
	require_once (JPATH_ROOT.'/administrator/components/com_ifaq/legacy/view.php');
	require_once (JPATH_ROOT.'/administrator/components/com_ifaq/legacy/model.php');
}

// Include dependancies
jimport('joomla.application.component.controller');
require_once JPATH_COMPONENT.'/includes.php';


$controller = JControllerLegacy::getInstance('Ifaq');
$controller->execute(JRequest::getCmd('task'));
$controller->redirect();
