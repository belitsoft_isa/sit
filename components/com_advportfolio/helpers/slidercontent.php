<?php
/**
 * @copyright	Copyright (c) 2013 Skyline Technology Ltd (http://extstore.com). All rights reserved.
 * @license		http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 */

// No direct access.
defined('_JEXEC') or die;
class AdvPortfolioHelperSliderContent {
    
    public static function repareContent($content)
    {
        $expandText = "more +";
        $collapseText = "less -";
        
        $document =& JFactory::getDocument();
        
        $beginTagName = "article_slider_begin";
        $endTagName = "article_slider_end";

        $testIndex = strpos($content, "{" . $beginTagName, $index);
        
        if (!$testIndex) 
            return $content;
        
        // Adding styles and javascript.
        
        $document->addStyleSheet( "/media/com_advportfolio/css/article_slider_css.css");
        $document->addScript("/media/com_advportfolio/js/article_slider_jquery.js");
        $document->addScriptDeclaration("
            
            var epjQuery = jQuery.noConflict();
            
            function InitializeSliders()
            {
                epjQuery('.jq_article_slider_container .jq_article_slider_content').hide();
                
                var expandedContents = epjQuery('.expanded .jq_article_slider_content');
                
                expandedContents.show('150');
                
                var expandedRightLabels = epjQuery('.expanded .jq_article_slider_title .jq_article_slider_title_right_label');
                
                expandedRightLabels.html('" . $collapseText. "');
                
                var collapsedRightLabels = epjQuery('.collapsed .jq_article_slider_title .jq_article_slider_title_right_label');
                
                collapsedRightLabels.html('" . $expandText. "');
                
                epjQuery('.jq_article_slider_container .jq_article_slider_title').click(function()
                {
                    var contentElement = epjQuery(this).next();
                    var rightLabel = epjQuery(this).children('.jq_article_slider_title_right_label');
                    
                    if (contentElement.is('.jq_article_slider_content'))
                    {
                        if (contentElement.is(':visible'))
                        {
                            contentElement.slideUp('150');
                            rightLabel.html('" . $expandText. "');
                        }
                        else
                        {
                            contentElement.slideDown('150');
                            rightLabel.html('" . $collapseText. "');
                        }
                        
                        return false;
                    }
                });
            }
            
            epjQuery(document).ready(function() { InitializeSliders(); });
            
            ");
        
        // Replacing tags.
        
        $newText = "";
        
        $index = 0;
        $articleLength = strlen($content);
        
        while ($index < $articleLength)
        {
            $beginTagFirstIndex = strpos($content, "{" . $beginTagName, $index);
            $endTagFirstIndex = strpos($content, "{" . $endTagName, $index);
 
            if (!$beginTagFirstIndex || !$endTagFirstIndex) break;
            
            $beginTagLastIndex = strpos($content, "}", $beginTagFirstIndex);
            $endTagLastIndex = strpos($content, "}", $endTagFirstIndex);
            
            if (!$beginTagLastIndex || !$endTagLastIndex) break;
            
            $beginTagText = substr($content, $beginTagFirstIndex, $beginTagLastIndex - $beginTagFirstIndex + 1);
            
            $title = AdvPortfolioHelperSliderContent::getParameterValue($beginTagText, "title", "SLIDER SECTION");
            $expanded = AdvPortfolioHelperSliderContent::getParameterValue($beginTagText, "expanded", "0");
            
            $openingHtml = AdvPortfolioHelperSliderContent::getOpeningHtml($title, $expanded);
            $closingHtml = AdvPortfolioHelperSliderContent::getClosingHtml();

            $newText .= substr($content, $index, $beginTagFirstIndex - $index);
            $newText .= $openingHtml;
            $newText .= substr($content, $beginTagLastIndex + 1, $endTagFirstIndex - $beginTagLastIndex - 1);
            $newText .= $closingHtml;
            
            $index = $endTagLastIndex + 1;
        }
        
        if ($index < $articleLength)
        {
            $newText .=  substr($content, $index);
        }
        
        $content = $newText;
        
        
        return $content;
    }

    public static function getParameterValue($tagText, $paramName, $defaultValue)
    {
        $tagStart = $paramName . "=\"";
        $tagEnd = "\"";
        
        $i1 = strpos($tagText, $tagStart, 0);
        
        if ($i1) $i1 += strlen($tagStart);
        else return $defaultValue;
        
        $i2 = strpos($tagText, $tagEnd, $i1 + 1);
        
        if (!$i2) return $defaultValue;
        
        $length = $i2 - $i1;
        
        if ($length <= 0) return $defaultValue;
        
        $value = substr($tagText, $i1, $length);

        $value = strip_tags($value);
        
        return $value;
    }
    

    public static function getOpeningHtml($title, $expanded)
    {
        $s = '<div class="jq_article_slider_container' . ($expanded == "1" ? ' expanded' : ' collapsed') . '">' .
                '<div class="jq_article_slider_title" >' .
                    '<div class="jq_article_slider_title_text" style="float:left;">' . $title . '</div>' .
                    '<div class="jq_article_slider_title_right_label" style="float:right;">' . "" . '</div>' .
                '</div>' .
                '<div class="jq_article_slider_content">';
        
        return $s;
    }
    
    public static function getClosingHtml()
    {
        $s = '</div>' .
            '</div>';
        
        return $s;
    }


}
