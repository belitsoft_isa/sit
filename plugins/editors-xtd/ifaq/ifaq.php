<?php
/**
 * @package		iFAQ
 * @copyright	Copyright (C) 2006 - 2014 IdealExtensions.com, Inc. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 */
// no direct access
defined('_JEXEC') or die;

jimport('joomla.plugin.plugin');

/**
 * Editor Article buton
 *
 * @package Editors-xtd
 * @since 1.5
 */
class plgButtonIfaq extends JPlugin
{
	/**
	 * Constructor
	 *
	 * @access	  protected
	 * @param	   object  $subject The object to observe
	 * @param	   array   $config  An array that holds the plugin configuration
	 * @since	   1.5
	 */
	public function __construct(& $subject, $config)
	{
		parent::__construct($subject, $config);
		$lang	= JFactory::getLanguage();
		$lang->load("com_ifaq",JPATH_ADMINISTRATOR);
		$lang->load('com_ifaq');
		$lang->load('com_ifaq',JPATH_ROOT.'/components/com_ifaq');
		$lang->load('plg_editors-xtd_ifaq',JPATH_ROOT.'/plugins/editors-xtd/ifaq');
	}


	/**
	 * Display the button
	 *
	 * @return array A four element array of (article_id, article_title, category_id, object)
	 */
	function onDisplay($name)
	{
		if(
			(is_array($this->params->get('avoid_components'))
				AND in_array(JRequest::getVar('option'),$this->params->get('avoid_components',array())))
			OR (!is_array($this->params->get('avoid_components'))
				AND JRequest::getVar('option') == 'com_contactenhanced')
		){
			return '';
		}
		/*
		 * Javascript to insert the link
		 * View element calls jSelectArticle when an article is clicked
		 * jSelectArticle creates the link tag, sends it to the editor,
		 * and closes the select frame.
		 */
		$js = "
		function jSelectiFAQ(tag) {
			jInsertEditorText(tag, '".$name."');
			SqueezeBox.close();
		}
";

		$js .= "
		jQuery(document).ready(function($){ $('i.icon-ifaq').addClass('glyphicon glyphicon-question-sign icon-question-sign text-error');});
";
		$doc = JFactory::getDocument();
		$doc->addScriptDeclaration($js);


		JHTML::_('behavior.modal');

		/*
		 * Use the built-in element view to select the article.
		 * Currently uses blank class.
		 */
		$link = 'index.php?option=com_ifaq&amp;view=config&amp;layout=modal&amp;tmpl=component';

		$button = new JObject();
		$button->set('modal', true);
		$button->set('link', $link);
		$button->class = 'btn btn-default';
		$button->set('text', JText::_('PLG_EDITORSXTD_IFAQ_BUTTON_ADD'));
		$button->set('name', 'ifaq');
		$button->set('options', "{handler: 'iframe', size: {x: 770, y: 400}}");

		return $button;
	}
}
