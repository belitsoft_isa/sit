<?php
/**
 * @package	iFAQ
 * @subpackage Base
 * @author	 Douglas Machado {@link http://idealextensions.com}
 * @author	 Created on 28-Mar-2015
 * @license	GNU/GPL
 */

//-- No direct access
defined('_JEXEC') || die('=;)');

jimport('joomla.plugin.plugin');

/**
 * Example Content Plugin.
 *
 * @package	iFAQ
 * @subpackage	Plugin
 */
class plgContentIFaq extends JPlugin
{
	/**
	 * Plugin that loads module positions within content
	 *
	 * @param	string	The context of the content being passed to the plugin.
	 * @param	object	The article object.  Note $article->text is also available
	 * @param	object	The article params
	 * @param	int		The 'page' number
	 */
	public function onContentPrepare($context, &$article, &$params, $page = 0)
	{
		// simple performance check to determine whether bot should process further
		if ( strpos( $article->text, 'ifaq' ) === false ) {
			return true;
		}


		// Get plugin info
		$plugin = JPluginHelper::getPlugin('content', 'ifaq');

		// define the regular expression for the bot
		$regex = "#{ifaq(.*?)}#s";

		$registry		= new JRegistry();
		$registry->loadString($plugin->params);
		$pluginParams	= $registry;

		// check whether plugin has been unpublished
		if ( !$pluginParams->get( 'enabled', 1 ) ) {
			$article->text = preg_replace( $regex, '', $row->text );
			return true;
		}


		// find all instances of plugin and put in $matches
		preg_match_all( $regex, $article->text, $matches );

		// Number of plugins
	 	$count = count( $matches[0] );

	 	// plugin only processes if there are any instances of the plugin in the text
	 	if ( $count ) {
			// Get plugin parameters
		 	$style	= $pluginParams->def( 'style', -2 );

	 		$this->plgContentProcessIfaq( $article, $matches, $count, $regex, $pluginParams );
		}

		return;
	}//function

	function plgContentProcessIfaq( &$row, &$matches, $count, $regex, &$botParams ){
		$config		=JFactory::getConfig();

		$lang =JFactory::getLanguage();
		$lang->load('com_ifaq');
		$lang->load('com_ifaq',JPATH_SITE.'/components/com_ifaq');
		$lang->load('plg_content_ifaq',dirname(__FILE__));

		$app = JFactory::getApplication();

		JRequest::setVar('article_id', (isset($row->id) ? $row->id : ''));
		for ( $i=0; $i < $count; $i++ ){
			if (@$matches[1][$i]) {
				$inline_params = $matches[1][$i];

				$app->input->set('ifaqPlugin', 1);

				// get view
				$view_matches = array();
				preg_match( "#view=\|(.*?)\|#s", $inline_params, $view_matches );
				if (isset($view_matches[1])){
					$view = trim( strtolower($view_matches[1]) );
				}else{
					$view = 'search';
				}

				// get id
				$id_matches = array();
				preg_match( "#id=\|(.*?)\|#s", $inline_params, $id_matches );
				if (isset($id_matches[1])) 				JRequest::setVar('ifaq_id', trim($id_matches[1]));
				
				// get article ids
				$article_id_matches = array();
				preg_match( "#articles=\|(.*?)\|#s", $inline_params, $article_id_matches );
				if (isset($article_id_matches[1])) 		JRequest::setVar('filter_article_id', trim($article_id_matches[1]));

				// get filter article type
				$article_filter_matches = array();
				preg_match( "#articles_filter=\|(.*?)\|#s", $inline_params, $article_filter_matches );
				if (isset($article_filter_matches[1])){
					if ($article_filter_matches[1] == 'exclude'){
						JRequest::setVar('filter_article_id_include', 0);
					}else{
						JRequest::setVar('filter_article_id_include', 1);
					}
				}
				
				// get title
				$title_matches = array();
				preg_match( "#title=\|(.*?)\|#s", $inline_params, $title_matches );
				if (isset($title_matches[1]))			JRequest::setVar('ifaq_title', trim($title_matches[1]));

				// get showtitle
				$title_matches = array();
				preg_match( "#showtitle=\|(.*?)\|#s", $inline_params, $showtitle_matches );
				if (isset($showtitle_matches[1]))		JRequest::setVar('ifaq_displayTitle', trim($showtitle_matches[1]));

				// get search
				$search_matches = array();
				preg_match( "#search=\|(.*?)\|#s", $inline_params, $search_matches );
				if (isset($search_matches[1]))			JRequest::setVar('ifaq_search', trim($search_matches[1]));

				// get searchphrase
				$searchphrase_matches = array();
				preg_match( "#searchphrase=\|(.*?)\|#s", $inline_params, $searchphrase_matches );
				if (isset($searchphrase_matches[1]))	JRequest::setVar('ifaq_searchphrase', trim($searchphrase_matches[1]));

				// get search_type
				$searchtype_matches = array();
				preg_match( "#search_type=\|(.*?)\|#s", $inline_params, $searchtype_matches );
				if (isset($searchtype_matches[1]))		JRequest::setVar('ifaq_search_type', trim($searchtype_matches[1]));

				// get text
				$description_matches = array();
				preg_match( "#description=\|(.*?)\|#s", $inline_params, $description_matches );
				if (isset($description_matches[1]))		JRequest::setVar('ifaq_description', trim($description_matches[1]));

				// get showdesc
				$showdesc_matches = array();
				preg_match( "#showdesc=\|(.*?)\|#s", $inline_params, $showdesc_matches );
				if (isset($showdesc_matches[1]))		JRequest::setVar('ifaq_displayTitle', trim($showdesc_matches[1]));

				// get showdesc
				$showdesc_matches = array();
				preg_match( "#showdesc=\|(.*?)\|#s", $inline_params, $showdesc_matches );
				if (isset($showdesc_matches[1]))		JRequest::setVar('ifaq_displayTitle', trim($showdesc_matches[1]));
				
				// set add_line_numbers
				$add_line_numbers_matches = array();
				preg_match( "#add_line_numbers=\|(.*?)\|#s", $inline_params, $add_line_numbers_matches );
				if (isset($add_line_numbers_matches[1]))		JRequest::setVar('add_line_numbers', trim($add_line_numbers_matches[1]));

				// Choose another template
				$template_matches = array();
				preg_match( "#template=\|(.*?)\|#s", $inline_params, $template_matches );
				if (isset($template_matches[1])){
					if (is_readable(JPATH_ROOT.'/components/com_ifaq/templates/'.trim($template_matches[1]).'/ifaq.css')) {
						$app->input->set('ifaq_plg_template', trim($template_matches[1]));
					}
				}

				// Choose another slider
				$slider_matches = array();
				preg_match( "#slider=\|(.*?)\|#s", $inline_params, $slider_matches );
				if (isset($slider_matches[1])){
					if (trim($slider_matches[1]) == 'custom'){
						JRequest::setVar('ifaq_plg_sliderType', 'LISliders');
					}elseif (trim($slider_matches[1]) == 'joomla'){
						JRequest::setVar('ifaq_plg_sliderType', 'DIVSliders');
					}
				}


				// Get links
				foreach(range('a', 'e') as $char){// letters 'a' to 'e'
					$link_matches = array();
					preg_match( "#link{$char}=\|(.*?)\|#s", $inline_params, $link_matches );
					if (isset($link_matches[1])){
						$link	= explode('::', $link_matches[1]);
						JRequest::setVar('link'.$char,			$link[0]);
						JRequest::setVar('link'.$char.'_name',	$link[1]);
						JRequest::setVar('show_links',			1);

					}

				}


			//Get model
				$model = JPATH_BASE.'/components/com_ifaq/models/'.$view.'.php';
				if(file_exists($model)){
					require_once($model);
					$modelClass	= 'IfaqModel'.ucfirst($view);
					$model		= new $modelClass();
				}elseif($config->get('config.error_reporting') == 6143 OR $config->get('config.debug')){
					$row->text = str_replace( $matches[0][$i], JText::_('Model does not exists'), $row->text );
					return '';
				}

			//Get view
				$viewPath = JPATH_BASE.'/components/com_ifaq/views/'.$view.'/view.html.php';
				if(file_exists($viewPath)){
					require_once(JPATH_BASE.'/components/com_ifaq/view.php');
					require_once(JPATH_BASE.'/components/com_ifaq/includes.php');
					require_once(JPATH_BASE.'/components/com_ifaq/models/'.$view.'.php');
					require_once(JPATH_BASE.'/components/com_ifaq/models/articles.php');
					require_once($viewPath);
					$viewClass	= 'IfaqView'.ucfirst($view);
					$view		= new $viewClass();

					$row->text = str_replace( $matches[0][$i], $view->display(), $row->text );

				}elseif($config->get('config.error_reporting') == 6143 OR $config->get('config.debug')){
					$row->text = str_replace( $matches[0][$i], JText::sprintf('View %s does not exists', $view), $row->text );
					return '';
				}

			}



		}
		$app->input->set('ifaqPlugin', 0);
	}//function

	/**
	 * Example after display title method
	 *
	 * Method is called by the view and the results are imploded and displayed in a placeholder
	 *
	 * @param 	object		The article object.  Note $article->text is also available
	 * @param 	object		The article params
	 * @param 	int			The 'page' number
	 * @return	string
	 */
	function onAfterDisplayTitle($article, &$params, $limitstart)
	{
		return '';
	}//function



	/**
	 * Example after display content method
	 *
	 * Method is called by the view and the results are imploded and displayed in a placeholder
	 *
	 * @param 	object		The article object.  Note $article->text is also available
	 * @param 	object		The article params
	 * @param 	int			The 'page' number
	 * @return	string
	 */
	function onAfterDisplayContent(&$article, &$params, $limitstart)
	{
		return '';
	}//function

	/**
	 * Example before save content method
	 *
	 * Method is called right before content is saved into the database.
	 * Article object is passed by reference, so any changes will be saved!
	 * NOTE:  Returning false will abort the save with an error.
	 * 	You can set the error by calling $article->setError($message)
	 *
	 * @param 	object		A JTableContent object
	 * @param 	boolean		If the content is just about to be created
	 * @return	boolean		If false, abort the save
	 */
	function onBeforeContentSave(&$article, $isNew)
	{
		return true;
	}//function

	/**
	 * Example after save content method
	 * Article is passed by reference, but after the save, so no changes will be saved.
	 * Method is called right after the content is saved
	 *
	 *
	 * @param 	object		A JTableContent object
	 * @param 	boolean		If the content is just about to be created
	 * @return	void
	 */
	function onAfterContentSave(&$article, $isNew)
	{
		return true;
	}//function
}//class
