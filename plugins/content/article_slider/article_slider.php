<?php
/**
* Article slider content plugin for Joomla 1.5
* @package ArticleSlider
* @author JoomPlace Team
* @copyright Copyright (C) JoomPlace, www.joomplace.com
* @license GNU/GPL http://www.gnu.org/copyleft/gpl.html
*/
defined("_JEXEC") or die("Restricted access");

jimport("joomla.plugin.plugin");

class plgContentArticle_Slider extends JPlugin
{
	protected $expandText;
	protected $collapseText;
	//----------------------------------------------------------------------------------------------------
	function onPrepareContent(&$article, &$params, $limitstart)
	{
		if (!isset($article->text)) return true;
		
		$this->expandText = "more +";
		$this->collapseText = "less -";
		
		$document =& JFactory::getDocument();
		
		$beginTagName = "article_slider_begin";
		$endTagName = "article_slider_end";
		
		$testIndex = strpos($article->text, "{" . $beginTagName, $index);
		
		if (!$testIndex) return true;
		
		// Adding styles and javascript.
		
		$document->addStyleSheet("plugins/content/article_slider_css.css");
		
		$document->addScript("plugins/content/article_slider_jquery.js");
		
		$document->addScriptDeclaration("
			
			var epjQuery = jQuery.noConflict();
			
			function InitializeSliders()
			{
				epjQuery('.jq_article_slider_container .jq_article_slider_content').hide();
				
				var expandedContents = epjQuery('.expanded .jq_article_slider_content');
				
				expandedContents.show('150');
				
				var expandedRightLabels = epjQuery('.expanded .jq_article_slider_title .jq_article_slider_title_right_label');
				
				expandedRightLabels.html('" . $this->collapseText . "');
				
				var collapsedRightLabels = epjQuery('.collapsed .jq_article_slider_title .jq_article_slider_title_right_label');
				
				collapsedRightLabels.html('" . $this->expandText . "');
				
				epjQuery('.jq_article_slider_container .jq_article_slider_title').click(function()
				{
					var contentElement = epjQuery(this).next();
					var rightLabel = epjQuery(this).children('.jq_article_slider_title_right_label');
					
					if (contentElement.is('.jq_article_slider_content'))
					{
						if (contentElement.is(':visible'))
						{
							contentElement.slideUp('150');
							rightLabel.html('" . $this->expandText . "');
						}
						else
						{
							contentElement.slideDown('150');
							rightLabel.html('" . $this->collapseText . "');
						}
						
						return false;
					}
				});
			}
			
			epjQuery(document).ready(function() { InitializeSliders(); });
			
			");
		
		// Replacing tags.
		
		$newText = "";
		
		$index = 0;
		$articleLength = strlen($article->text);
		
		while ($index < $articleLength)
		{
			$beginTagFirstIndex = strpos($article->text, "{" . $beginTagName, $index);
			$endTagFirstIndex = strpos($article->text, "{" . $endTagName, $index);
			
			//echo "<br/>\$beginTagFirstIndex: " . $beginTagFirstIndex;
			//echo "<br/>\$beginTagLastIndex: " . $beginTagLastIndex;
			
			if (!$beginTagFirstIndex || !$endTagFirstIndex) break;
			
			$beginTagLastIndex = strpos($article->text, "}", $beginTagFirstIndex);
			$endTagLastIndex = strpos($article->text, "}", $endTagFirstIndex);
			
			//echo "<br/>\$endTagFirstIndex: " . $endTagFirstIndex;
			//echo "<br/>\$endTagLastIndex: " . $endTagLastIndex;
			
			if (!$beginTagLastIndex || !$endTagLastIndex) break;
			
			$beginTagText = substr($article->text, $beginTagFirstIndex, $beginTagLastIndex - $beginTagFirstIndex + 1);
			
			$title = $this->getParameterValue($beginTagText, "title", "SLIDER SECTION");
			$expanded = $this->getParameterValue($beginTagText, "expanded", "0");
			
			$openingHtml = $this->getOpeningHtml($title, $expanded);
			$closingHtml = $this->getClosingHtml();
			
			//echo "<br/>\$beginTagText: '" . $beginTagText . "'";
			//echo "<br/>\$title: '" . $title . "'";
			//echo "<br/>\$expanded: '" . $expanded . "'";
			//echo "<br/>";
			
			$newText .= substr($article->text, $index, $beginTagFirstIndex - $index);
			$newText .= $openingHtml;
			$newText .= substr($article->text, $beginTagLastIndex + 1, $endTagFirstIndex - $beginTagLastIndex - 1);
			$newText .= $closingHtml;
			
			$index = $endTagLastIndex + 1;
		}
		
		if ($index < $articleLength)
		{
			$newText .=  substr($article->text, $index);
		}
		
		$article->text = $newText;
	}
	//----------------------------------------------------------------------------------------------------
	function getParameterValue($tagText, $paramName, $defaultValue)
	{
		$tagStart = $paramName . "=\"";
		$tagEnd = "\"";
		
		$i1 = strpos($tagText, $tagStart, 0);
		
		if ($i1) $i1 += strlen($tagStart);
		else return $defaultValue;
		
		$i2 = strpos($tagText, $tagEnd, $i1 + 1);
		
		if (!$i2) return $defaultValue;
		
		$length = $i2 - $i1;
		
		if ($length <= 0) return $defaultValue;
		
		$value = substr($tagText, $i1, $length);
		
		return $value;
	}
	//----------------------------------------------------------------------------------------------------
	function getOpeningHtml($title, $expanded)
	{
		$s = '<div class="jq_article_slider_container' . ($expanded == "1" ? ' expanded' : ' collapsed') . '">' .
				'<div class="jq_article_slider_title" >' .
					'<div class="jq_article_slider_title_text" style="float:left;">' . $title . '</div>' .
					'<div class="jq_article_slider_title_right_label" style="float:right;">' . "" . '</div>' .
				'</div>' .
				'<div class="jq_article_slider_content">';
		
		return $s;
	}
	
	function getClosingHtml()
	{
		$s = '</div>' .
			'</div>';
		
		return $s;
	}
}